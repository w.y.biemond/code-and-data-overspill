#here: make a regime diagram where we vary the spatial properties and keep UT and Q constant. 

#use the initialisation
#standard packages
import numpy as np                      #standard operations       
import os as os                          #making folders etc
import time                              #measure time of operation

#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water

#import the model
from modti_fork_fin import MC04_Gal_split #time-independent

# =============================================================================
# settings which are equal for all runs
# =============================================================================
N=10
Lsc=1000

sri = 0
u_w = 0
d_w = 1
sf1,sf2,sf3 = 0,0,0
'''
# =============================================================================
# Yangtze
# =============================================================================

#folders for saving
#here Q and Ut are constants
Q = 4000
soc = 25 
n_sea2,n_sea3 = 0.0005 , 0



#geometry 
#main channel
Ln1 = np.array([210000,39000,800,200])
b01 = 6000
bs1 = np.array([b01,b01,b01,b01])
dxn1 = np.array([1000,500,200,10]) 
#upper branch
Ln2 = np.array([200,800,60000,25000])
b02,binf2 = 3000,3500
bs2 = np.array([b02*np.exp(0.2/395.72),b02*np.exp(1/395.72),binf2,binf2*np.exp(10)])
dxn2 = np.array([10,100,200,100]) 
#lower branch
Ln3 = np.array([200,800,53000,25000])
b03,binf3 = 3000 , 30000
bs3 = np.array([b03*np.exp(0.2/23.45),b03*np.exp(1/23.45),binf3,binf3*np.exp(10)])
dxn3 = np.array([10,100,200,100]) 
#depth
#H=(9,11,7)

#varying parameters
#Ut1_list = np.linspace(0.65,1.30,11)
#Ut2_list = np.linspace(0.75,2.00,11)
#Ut3_list = np.linspace(0.58,1.70,11)
#Q_list = np.linspace(1000,10000,11)


'''
# =============================================================================
# Modaomen
# =============================================================================
#here Q and Ut are constants
Q = 2000.
soc = 35 

n_sea2,n_sea3 = 0 , 0.002
#geometry 
#main channel
Ln1 = np.array([110000,39000,800,200])
b01 = 2000
bs1 = np.array([b01,b01,b01,b01])
dxn1 = np.array([500,250,100,10]) 
#upper branch
Ln2 = np.array([200,800,11500,25000])
b02 = 2000
bs2 = np.array([b02,b02,b02,b02*np.exp(10)])
dxn2 = np.array([10,100,100,100]) 
#lower branch
Ln3 = np.array([200,800,10000,6000,25000])
b03,b13 = 500,3500
bs3 = np.array([b03,b03,b03, b13, b13*np.exp(10)])
dxn3 = np.array([10,100,100,100,100]) 
#depth
#H=(8,7,5)

'''
# =============================================================================
# Build all four scenarios here
# =============================================================================

#scenario Yangtze CHANNEL 2 
fold = 'Yangtze_geoNP_fin/'
loc = '/Users/biemo004/Documents/UU phd Saltisolutions/Files paper fork/model_runs/'+fold
#os.mkdir(loc)    

#varying parameters
inp = np.loadtxt(loc+'input.txt')
H_list = inp[:,0]
b_list = inp[:,1]
Ut1_list = inp[:,4]
Ut2_list = inp[:,2]
Ut3_list = inp[:,3]

for run in range(len(inp)):
    #adjust geometry to the scenario
    #depth
    H = (9,H_list[run],7)
    #width
    b_here = b_list[run]
    Lb_here = 61000/(np.log(b_here/b02)) #soms delen door 0 maar whatever
    bs2 = np.array([b02*np.exp(200/Lb_here),b02*np.exp(1000/Lb_here),b_here,b_here*np.exp(10)])


#scenario Yangtze CHANNEL 3 
fold = 'Yangtze_geoSP_fin/'
loc = '/Users/biemo004/Documents/UU phd Saltisolutions/Files paper fork/model_runs/'+fold
#os.mkdir(loc)    

#varying parameters
inp = np.loadtxt(loc+'input.txt')
H_list = inp[:,0]
b_list = inp[:,1]
Ut1_list = inp[:,4]
Ut2_list = inp[:,2]
Ut3_list = inp[:,3]


for run in range(len(inp)):
    #adjust geometry to the scenario
    #depth
    H = (9,11,H_list[run])
    #width
    b_here = b_list[run]
    Lb_here = 54000/(np.log(b_here/b03)) #soms delen door 0 maar whatever
    bs3 = np.array([b03*np.exp(200/Lb_here),b03*np.exp(1000/Lb_here),b_here,b_here*np.exp(10)])

#scenario Modaomen CHANNEL 2 
fold = 'Modao_geoMC_Q2000/'
loc = '/Users/biemo004/Documents/UU phd Saltisolutions/Files paper fork/model_runs/'+fold
#os.mkdir(loc)    

#varying parameters
inp = np.loadtxt(loc+'input.txt')
H_list = inp[:,0]
b_list = inp[:,1]
Ut1_list = inp[:,4]
Ut2_list = inp[:,3]
Ut3_list = inp[:,2]

for run in range(len(inp)):
    #adjust geometry to the scenario
    #depth
    H = (8,H_list[run],5)
    #width
    b_here = b_list[run]
    Lb_here = 12500/(np.log(b_here/b02)) #soms delen door 0 maar whatever
    bs2 = np.array([b02*np.exp(200/Lb_here),b02*np.exp(1000/Lb_here),b_here,b_here*np.exp(10)])
    '''
#scenario Modaomen CHANNEL 3 
fold = 'Modao_geoHC_Q2000/'
loc = '/Users/biemo004/Documents/UU phd Saltisolutions/Files paper fork/model_runs/'+fold
#os.mkdir(loc)    

#varying parameters
inp = np.loadtxt(loc+'input.txt')
H_list = inp[:,0]
b_list = inp[:,1]
Ut1_list = inp[:,4]
Ut2_list = inp[:,3]
Ut3_list = inp[:,2]

for run in range(len(inp)):
    #adjust geometry to the scenario
    #depth
    H = (8,7,H_list[run])
    #width
    b_here = b_list[run]
    Lb_here = 17000/(np.log(b_here/b02)) #soms delen door 0 maar whatever
    bs3 = np.array([b03,b03,b03,b_here,b_here*np.exp(10)])
    #'''
  
    Ut1,Ut2,Ut3 = Ut1_list[run] , Ut2_list[run] , Ut3_list[run]

    #Rest of the party
    def runmod():
        ini = np.zeros(int(np.sum(Ln1/dxn1+1)+np.sum(Ln2/dxn2+1)+np.sum(Ln3/dxn3+1))*(N+1)+3)
        ini[-3:] =[Q,Q/2,Q/2]
        out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [3,6,7],ini)
        print('initialisation completed')
        out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
        #check if there was convergence
        if out[0] == 'NOCON':
            #if there was no convergence, try to go in steps of Ut to this values
            Ut1_temp,Ut2_temp,Ut3_temp = np.linspace(0.75,Ut1,5) , np.linspace(0.75,Ut2,5) , np.linspace(0.75,Ut3,5)
            out = MC04_Gal_split(((Ut1_temp[0],Ut2_temp[0],Ut3_temp[0]), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [3,6,7],ini)
            print('initialisation completed')
            out = MC04_Gal_split(((Ut1_temp[0],Ut2_temp[0],Ut3_temp[0]), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
            for i in range(1,len(Ut1_temp)):
                out = MC04_Gal_split(((Ut1_temp[i],Ut2_temp[i],Ut3_temp[i]), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
        #check if there is convergence now
        if out[0] == 'NOCON':
            #try to go in steps of Q to this value
            Q_temp = np.linspace(1000,Q,5) #this value of 1000 depends of course a lot on the channel geometry
            out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q_temp[0], u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [3,6,7],ini)
            print('initialisation completed')
            out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q_temp[0], u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
            for i in range(1,len(Q_temp)):
                out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q_temp[i], u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
        #check if there is convergence now
        if out[0] == 'NOCON' : print('Still no convergence. better check grid spacing and stuff ')
        
        return out
    
    tijd = time.time()
    out2 = runmod()
    print('Doing the simulation takes  '+ str(time.time()-tijd)+' seconds')
    
    #save
    os.mkdir(loc+'/'+str('run')+str(run))
    #prepare files to be saved
    inp = np.array([N,Lsc,Ut1,Ut2,Ut3,soc,sri,u_w,d_w,Q,n_sea2,n_sea3])
    #geometry is complicated
    sav_geo1 = np.concatenate([[len(Ln1)],Ln1,[b01],bs1,dxn1])
    sav_geo2 = np.concatenate([[len(Ln2)],Ln2,[b02],bs2,dxn2])
    sav_geo3 = np.concatenate([[len(Ln3)],Ln3,[b03],bs3,dxn3])
    if len(Ln1) != len(Ln2) or len(Ln1) != len(Ln3):
        maxlen = np.max([len(Ln1),len(Ln2),len(Ln3)])
        if len(Ln1)<maxlen: sav_geo1 = np.concatenate([sav_geo1,np.nan+np.zeros((maxlen-len(Ln1))*3)])       
        if len(Ln2)<maxlen: sav_geo2 = np.concatenate([sav_geo2,np.nan+np.zeros((maxlen-len(Ln2))*3)])
        if len(Ln3)<maxlen: sav_geo3 = np.concatenate([sav_geo3,np.nan+np.zeros((maxlen-len(Ln3))*3)])
        if 3       <maxlen: sav_H    = np.concatenate([sav_geo3,np.nan+np.zeros((maxlen-len(Ln3))*3)])
    sav_H= np.concatenate([np.array(H),np.nan+np.zeros(len(sav_geo1)-3)]) #add depth
    geom = np.array([sav_geo1,sav_geo2,sav_geo3,sav_H]) 
    
    np.savetxt(loc+'/run'+str(run)+'/output.txt',out2, fmt='%s')
    np.savetxt(loc+'/run'+str(run)+'/input.txt',inp, fmt='%s')
    np.savetxt(loc+'/run'+str(run)+'/geometry.txt',geom, fmt='%s')
    