#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  3 16:58:11 2022

@author: biemo004
"""
import numpy as np
import scipy.integrate as integrate
g,Be,soc,cv,Sc,ch2 = 9.81,7.6e-4,35,7.28e-5,2.2,250
r= 1/2#(r=0=no slip, r=1/2=FWP slip)
g1,g2,g3,g4,g5 = -1 + (1.5+3*r) / (1+ 3 *r) ,  -3 / (2+6*r), (1+4*r) / (1+3*r) * (9+18*r) - 8 - 24*r , -9 * (1+4*r) / (1+3*r) , -8
p2p4 = integrate.quad(lambda z: (g3+g4*z**2+g5*z**3)*(-g3/6 -g4/60+g5/120 + g3/2*z**2 + g4/12 *z**4 +g5/20 *z**5)   , -1, 0) [0]
p1p3 = integrate.quad(lambda z: (g1 + g2*z**2)*(-g1/6 -g2/60 + g1/2*z**2 +g2/12*z**4)   , -1, 0) [0]
p2p3 = integrate.quad(lambda z: (g3+g4*z**2+g5*z**3)*(-g1/6 -g2/60 + g1/2*z**2 +g2/12*z**4)  , -1, 0) [0]
p1p4 = integrate.quad(lambda z: (g1 + g2*z**2)*(-g3/6 -g4/60+g5/120 + g3/2*z**2 + g4/12 *z**4 +g5/20 *z**5)   , -1, 0) [0]

#define solver qubic equations
def q_solv(a,b,c,d): #finds only the real solution, I think that is sufficient for now. equivalent to np.roots
    #define variables
    e=1/a * (c - b**2/(3*a))
    f=1/a * (d + 2*b**3/(27*a**2) - b*c/(3*a))
    w1 = -0.5*f + 0.5*np.sqrt(f**2 + 4/27*e**3 + 0j)
    w2 = -0.5*f - 0.5*np.sqrt(f**2 + 4/27*e**3 + 0j)
    z1,z2 = w1**(1/3),w2**(1/3) #we do not get all the solutions here 
    y1,y2 = z1-e/(3*z1) , z2-e/(3*z2)
    x1,x2 = y1-b/(3*a) , y2-b/(3*a)
    
    #check
    if z1 ==0. or z2==0. : print('error: qsolve does not work', x1,x2)
    #select the real solution
    return np.real(x1) if np.imag(x1)==0 else np.real(x2)


def pred_T(geg2,geg3):
    #load
    Q2,Ut2,H2,Le2,b02,bi2,soc2 = geg2
    Q3,Ut3,H3,Le3,b03,bi3,soc3 = geg3
    if soc2!=soc3: print('ERROR!')
    soc=soc2
    #if Q2<0 or Q3<0: return None #framework does not work if there is negative Q
    #avoid straight channel problems
    if b02==bi2: bi2 = bi2+1e-6
    if b03==bi3: bi3 = bi3+1e-6
    
    #build mixing
    Kh2 = ch2*Ut2**2
    Kh3 = ch2*Ut3**2
    Av2 = cv*Ut2*H2
    Av3 = cv*Ut3*H3
    Kv2 = Av2/Sc
    Kv3 = Av3/Sc
    #build length scales
    LD2 = Kh2/(Q2/(H2*b02))
    LDs2 = Kh2/(Q2/(H2*bi2)) #actually not true, Kh2 is different in the open ocean
    Lb2 = Le2/np.log(bi2/b02)
    Lbs2 = 2500#25000/10
    
    LD3 = Kh3/(Q3/(H3*b03))
    LDs3 = Kh3/(Q3/(H3*bi3)) #actually not true, Kh3 is different in the open ocean
    Lb3 = Le3/np.log(bi3/b03)
    Lbs3 = 2500#25000/10
    
    #calculate \nu in a MC04 way
    dsdx2 = q_solv((-p2p4*np.sqrt(g*Be*H2*soc)/(48**2*Q2/(H2*bi2)*Av2**2*Kv2)) * (H2**2*np.sqrt(g*Be*H2*soc))**3 ,
                   (-p1p4-p2p3)/(48*Kv2*Av2) * (g*Be*H2*soc) * H2**4 + (g*Be*H2*soc)*H2**4/(Kv2*Av2)*(g3/144 + g4/720 - g5/1152) , 
                   -p1p3*H2**2/Kv2*Q2/(H2*bi2) + Kh2*bi2*H2/Q2 + Q2/(H2*bi2)*H2**2/Kv2*(g1/3+g2/15) ,
                   -1)
    dsdx3 = q_solv((-p2p4*np.sqrt(g*Be*H3*soc)/(48**2*Q3/(H3*bi3)*Av3**2*Kv3)) * (H3**2*np.sqrt(g*Be*H3*soc))**3 ,
                   (-p1p4-p2p3)/(48*Kv3*Av3) * (g*Be*H3*soc) * H3**4 + (g*Be*H3*soc)*H3**4/(Kv3*Av3)*(g3/144 + g4/720 - g5/1152) , 
                   -p1p3*H3**2/Kv3*Q3/(H3*bi3) + Kh3*bi3*H3/Q3 + Q3/(H3*bi3)*H3**2/Kv3*(g1/3+g2/15) ,
                   -1)
    nu2 = (-p2p4*np.sqrt(g*Be*H2*soc)/(48**2*Q2/(H2*bi2)*Av2**2*Kv2)) * (H2**2*np.sqrt(g*Be*H2*soc))**3 * dsdx2**3 \
        + (-p1p4-p2p3)/(48*Kv2*Av2) * (g*Be*H2*soc) * H2**4 * dsdx2**2  \
            + (-p1p3*H2**2/Kv2*Q2/(H2*bi2) + Kh2*bi2*H2/Q2) * dsdx2
    nu3 = (-p2p4*np.sqrt(g*Be*H3*soc)/(48**2*Q3/(H3*bi3)*Av3**2*Kv3)) * (H3**2*np.sqrt(g*Be*H3*soc))**3 * dsdx3**3 \
        + (-p1p4-p2p3)/(48*Kv3*Av3) * (g*Be*H3*soc) * H3**4 * dsdx3**2  \
            + (-p1p3*H3**2/Kv3*Q3/(H3*bi3) + Kh3*bi3*H3/Q3) * dsdx3       
            
    #calculate coefficients in overspill equation
    nu2 = 1#np.exp(-Lbs2/LDs2)
    nu3 = 1#np.exp(-Lbs3/LDs3)
    mu2 = nu2*np.exp(Lb2/LD2*(np.exp(-Le2/Lb2)-1))
    mu3 = nu3*np.exp(Lb3/LD3*(np.exp(-Le3/Lb3)-1))
    ga2 = -2*Lb2 * (- Q2/(b02*H2) * (Kv2*Av2**2*48**2)/(g**2*Be**2*H2**8*p2p4*soc**2))**(1/3) *(np.exp(-1/3*Le2/Lb2)-1)
    ga3 = -2*Lb3 * (- Q3/(b03*H3) * (Kv3*Av3**2*48**2)/(g**2*Be**2*H3**8*p2p4*soc**2))**(1/3) *(np.exp(-1/3*Le3/Lb3)-1)
    
    #calculate salinity with T=0
    s2D = mu2 
    s3D = mu3
    s2E = (nu2**(2/3) - ga2)**(3/2) if nu2**(2/3) > ga2 else 0
    s3E = (nu3**(2/3) - ga3)**(3/2) if nu3**(2/3) > ga3 else 0
      
    dom2 = 'E' if s2E>s2D else 'D'
    dom3 = 'E' if s3E>s3D else 'D'
    #calcualte overspiol for different cases
    if dom2 =='D' and dom3=='D': #situation 1
        situ=1
        fact = ((1-mu2)/Q2 + (1-mu3)/Q3)**-1
        T2 = (mu3-mu2) * ((1-mu2)/Q2 + (1-mu3)/Q3)**-1
    elif dom2 =='D' and dom3 == 'E': #sitaution 2a
        situ=2.0
        fact = ((1-nu3**(2/3)*(nu3**(2/3)-ga3)**(1/2))/Q3 + (1-mu2)/Q2)**-1
        T2 = ((nu3**(2/3)-ga3)**(3/2) - mu2) * ((1-nu3**(2/3)*(nu3**(2/3)-ga3)**(1/2))/Q3 + (1-mu2)/Q2)**-1
    elif dom2 =='E' and dom3 == 'D': #sitaution 2b
        situ=2.5
        fact = ((1-nu2**(2/3)*(nu2**(2/3)-ga2)**(1/2))/Q2 + (1-mu3)/Q3)**-1
        T2 = (mu3 - (nu2**(2/3)-ga2)**(3/2) ) * ((1-nu2**(2/3)*(nu2**(2/3)-ga2)**(1/2))/Q2 + (1-mu3)/Q3)**-1
    elif dom2 =='E' and dom3=='E': #situation 3
        situ=3
        fact = ((1-nu2**(2/3)*(nu2**(2/3)-ga2)**(1/2))/Q2 + (1-nu3**(2/3)*(nu3**(2/3)-ga3)**(1/2))/Q3)**-1
        T2 = ((nu3**(2/3)-ga3)**(3/2) - (nu2**(2/3)-ga2)**(3/2)) * ((1-nu2**(2/3)*(nu2**(2/3)-ga2)**(1/2))/Q2 + (1-nu3**(2/3)*(nu3**(2/3)-ga3)**(1/2))/Q3)**-1
    else: print('ERROR')
    
    return T2*soc , situ , soc*np.max([s2D,s2E]), soc*np.max([s3D,s3E]) , fact

#print(pred_T([2143,1.40,11,61000,3000,3500,25],[1857,1.97,7,54000,3000,30000,25]))
#print(pred_T([977,1.05,7,12500,2000,2000,35],[23,0.75,5,17000,500,3500,35]))

print(pred_T([2121,1.40,11,61000,3000,3500,25],[1879,1.97,7,54000,3000,30000,25]))
print(pred_T([1903,1.05,7,12500,2000,2000,35],[97,0.75,5,17000,500,3500,35]))