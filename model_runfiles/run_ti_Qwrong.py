#here: make a regime diagram where we vary the spatial properties and keep UT and Q constant. 

#use the initialisation
#standard packages
import numpy as np                      #standard operations       
import os as os                          #making folders etc
import time                              #measure time of operation

#import the model
from modti_fork_Qwrong import MC04_Gal_split#time-independent

# =============================================================================
# settings which are equal for all runs
# =============================================================================
N=10
Lsc=1000


sri = 0
u_w = 0
d_w = 1
sf1,sf2,sf3 = None,None,None

# =============================================================================
# Yangtze
# =============================================================================

#folders for saving
fold = 'Yangtze_Qwrong_fin/'
loc = '/Users/biemo004/Documents/UU phd Saltisolutions/Files paper fork/model_runs/'+fold
#os.mkdir(loc)    
soc = 25 


#depth
H=(9,11,7)
n_sea2, n_sea3 =  0.0005,0.

#geometry 
#main channel
Ln1 = np.array([210000,39000,800,200])
b01 = 6000
bs1 = np.array([b01,b01,b01,b01])
dxn1 = np.array([2100,500,200,10]) 

#upper branch
Ln2 = np.array([200,800,60000,25000])
b02,binf2 = 3000,3500
bs2 = np.array([b02*np.exp(0.2/470),b02*np.exp(1/470),binf2,binf2*np.exp(10)])
#dxn2 = np.array([10,100,600,500]) 
dxn2 = np.array([10,100,200,100])
#lower branch
Ln3 = np.array([200,800,53000,25000])
b03,binf3 = 3000 , 30000
bs3 = np.array([b03*np.exp(0.2/21),b03*np.exp(1/21),binf3,binf3*np.exp(10)])
#dxn3 = np.array([10,100,500,500]) 
dxn3 = np.array([10,100,200,100])


#varying parameters
inp = np.loadtxt(loc+'input.txt')
#Ut1_list = (inp[:,2]+inp[:,3]+inp[:,4])/3
#Ut2_list = (inp[:,2]+inp[:,3]+inp[:,4])/3
#Ut3_list = (inp[:,2]+inp[:,3]+inp[:,4])/3
Ut1_list = inp[:,4]
Ut2_list = inp[:,2]
Ut3_list = inp[:,3]
Q_list = inp[:,1]


for run in range(45,50):#len(Q_list)):
    Q = Q_list[run]
    Ut1,Ut2,Ut3 = Ut1_list[run] , Ut2_list[run] , Ut3_list[run]
    def runmod():
        ini = np.zeros(int(np.sum(Ln1/dxn1+1)+np.sum(Ln2/dxn2+1)+np.sum(Ln3/dxn3+1))*(N+1)+3)
        ini[-3:] =[Q,Q/2,Q/2]
        out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [3,6,7],ini)
        print('initialisation completed')
        out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
        #check if there was convergence
        if out[0] == 'NOCON':
            #if there was no convergence, try to go in steps of Ut to this values
            Ut1_temp,Ut2_temp,Ut3_temp = np.linspace(1.5,Ut1,5) , np.linspace(1.5,Ut2,5) , np.linspace(1.5,Ut3,5)
            out = MC04_Gal_split(((Ut1_temp[0],Ut2_temp[0],Ut3_temp[0]), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [3,6,7],ini)
            print('initialisation completed')
            out = MC04_Gal_split(((Ut1_temp[0],Ut2_temp[0],Ut3_temp[0]), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
            for i in range(1,len(Ut1_temp)):
                out = MC04_Gal_split(((Ut1_temp[i],Ut2_temp[i],Ut3_temp[i]), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
        #check if there is convergence now
        if out[0] == 'NOCON':
            #try to go in steps of Q to this value
            Q_temp = np.linspace(1000,Q,5) #this value of 1000 depends of course a lot on the channel geometry
            out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q_temp[0], u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [3,6,7],ini)
            print('initialisation completed')
            out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q_temp[0], u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
            for i in range(1,len(Q_temp)):
                out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q_temp[i], u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
        #check if there is convergence now
        if out[0] == 'NOCON' : print('Still no convergence. better check grid spacing and stuff ')
        
        return out
    
    tijd = time.time()
    out2 = runmod()
    print('Doing the simulation takes  '+ str(time.time()-tijd)+' seconds')

    #save
    os.mkdir(loc+'/'+str('run')+str(run))
    #prepare files to be saved
    inp = np.array([N,Lsc,Ut1,Ut2,Ut3,soc,sri,u_w,d_w,Q,n_sea2,n_sea3])
    #geometry is complicated
    sav_geo1 = np.concatenate([[len(Ln1)],Ln1,[b01],bs1,dxn1])
    sav_geo2 = np.concatenate([[len(Ln2)],Ln2,[b02],bs2,dxn2])
    sav_geo3 = np.concatenate([[len(Ln3)],Ln3,[b03],bs3,dxn3])
    if len(Ln1) != len(Ln2) or len(Ln1) != len(Ln3):
        maxlen = np.max([len(Ln1),len(Ln2),len(Ln3)])
        if len(Ln1)<maxlen: sav_geo1 = np.concatenate([sav_geo1,np.nan+np.zeros((maxlen-len(Ln1))*3)])       
        if len(Ln2)<maxlen: sav_geo2 = np.concatenate([sav_geo2,np.nan+np.zeros((maxlen-len(Ln2))*3)])
        if len(Ln3)<maxlen: sav_geo3 = np.concatenate([sav_geo3,np.nan+np.zeros((maxlen-len(Ln3))*3)])
        if 3       <maxlen: sav_H    = np.concatenate([sav_geo3,np.nan+np.zeros((maxlen-len(Ln3))*3)])
    sav_H= np.concatenate([np.array(H),np.nan+np.zeros(len(sav_geo1)-3)]) #add depth
    geom = np.array([sav_geo1,sav_geo2,sav_geo3,sav_H]) 
    
    np.savetxt(loc+'/run'+str(run)+'/output.txt',out2, fmt='%s')
    np.savetxt(loc+'/run'+str(run)+'/input.txt',inp, fmt='%s')
    np.savetxt(loc+'/run'+str(run)+'/geometry.txt',geom, fmt='%s')
