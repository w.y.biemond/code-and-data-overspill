#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 15:39:13 2022

@author: biemo004
"""


#analysis of time-independent runs of fork system
#here: Ut,Q constant, L,b,h vaired
#standard packages
import numpy as np                      #standard operations       
import matplotlib.pyplot as plt         #plot figures
import pandas as pd                     #import datasets
from scipy import optimize   , interpolate 
import scipy as sp          #do fits
import scipy.io                         #import matlab datasets

import matplotlib.animation as ani       #make animations
from matplotlib.collections import LineCollection
import matplotlib.patheffects as pe
#import cv2                               #import images as matrices
import time                              #measure time of operation
import os as os                          #making folders etc

#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water
ch2=250



def load_reg(fold):
    loc = '/Users/biemo004/Documents/UU phd Saltisolutions/Output/fork_system/'+fold
    loc = '/Users/biemo004/Documents/UU phd Saltisolutions/Files paper fork/model_runs/'+fold

    Nruns = 21*21
    out_plot=np.zeros((Nruns,26))
    out_ls = np.zeros((Nruns,2,7))
    flux_save = []
    
    for run in range(Nruns):
    
        N,Lsc,Ut1,Ut2,Ut3,soc,sri,u_w,d_w,Q,n_sea2,n_sea3= np.loadtxt(loc+'/run'+str(run)+'/input.txt')
        sss = np.loadtxt(loc+'/run'+str(run)+'/output.txt')
        #rebuild geometry - somewhat more complicated
        chan1, chan2, chan3, H = np.loadtxt(loc+'/run'+str(run)+'/geometry.txt')
        Ln1,b01,bs1,dxn1 = chan1[1:1+int(chan1[0])],chan1[1+int(chan1[0])],chan1[2+int(chan1[0]):2+2*int(chan1[0])],chan1[2+2*int(chan1[0]):2+3*int(chan1[0])]
        Ln2,b02,bs2,dxn2 = chan2[1:1+int(chan2[0])],chan2[1+int(chan2[0])],chan2[2+int(chan2[0]):2+2*int(chan2[0])],chan2[2+2*int(chan2[0]):2+3*int(chan2[0])]
        Ln3,b03,bs3,dxn3 = chan3[1:1+int(chan3[0])],chan3[1+int(chan3[0])],chan3[2+int(chan3[0]):2+2*int(chan3[0])],chan3[2+2*int(chan3[0]):2+3*int(chan3[0])]
        H1,H2,H3 = H[:3]
        Q1,Q2,Q3 = sss[-3:]
        
        #build some usefull information
        nxn1 = np.array(Ln1/dxn1+1,dtype=int)
        nxn2 = np.array(Ln2/dxn2+1,dtype=int)
        nxn3 = np.array(Ln3/dxn3+1,dtype=int)
        di1 = np.zeros(len(Ln1)+1,dtype=int) #starting indices of the domains
        di2 = np.zeros(len(Ln2)+1,dtype=int) #starting indices of the domains
        di3 = np.zeros(len(Ln3)+1,dtype=int) #starting indices of the domains
        for i in range(1,len(Ln1)):   di1[i] = np.sum(nxn1[:i])
        di1[-1] = np.sum(nxn1)
        for i in range(1,len(Ln2)):   di2[i] = np.sum(nxn2[:i])
        di2[-1] = np.sum(nxn2)
        for i in range(1,len(Ln3)):   di3[i] = np.sum(nxn3[:i])
        di3[-1] = np.sum(nxn3)
        
        #build parameters: convergence length
        bn1, bn2, bn3 = np.zeros(len(Ln1)), np.zeros(len(Ln2)), np.zeros(len(Ln3))
        bn1[0] = 9e99 if bs1[0] == b01 else Ln1[0]/np.log(bs1[0]/b01)
        bn2[0] = 9e99 if bs2[0] == b02 else Ln2[0]/np.log(bs2[0]/b02)
        bn3[0] = 9e99 if bs3[0] == b03 else Ln3[0]/np.log(bs3[0]/b03)
        for i in range(1,len(Ln1)): bn1[i] = 9e99 if bs1[i] == bs1[i-1] else Ln1[i]/np.log(bs1[i]/bs1[i-1])
        for i in range(1,len(Ln2)): bn2[i] = 9e99 if bs2[i] == bs2[i-1] else Ln2[i]/np.log(bs2[i]/bs2[i-1])
        for i in range(1,len(Ln3)): bn3[i] = 9e99 if bs3[i] == bs3[i-1] else Ln3[i]/np.log(bs3[i]/bs3[i-1])
        #build width
        b1, bex1 = np.zeros(np.sum(nxn1)),np.zeros(np.sum(nxn1))
        b2, bex2 = np.zeros(np.sum(nxn2)),np.zeros(np.sum(nxn2))
        b3, bex3 = np.zeros(np.sum(nxn3)),np.zeros(np.sum(nxn3))
        b1[0:nxn1[0]] = b01 * np.exp(bn1[0]**(-1)*(np.linspace(-Ln1[0],0,nxn1[0])+Ln1[0]))
        b2[0:nxn2[0]] = b02 * np.exp(bn2[0]**(-1)*(np.linspace(-Ln2[0],0,nxn2[0])+Ln2[0]))
        b3[0:nxn3[0]] = b03 * np.exp(bn3[0]**(-1)*(np.linspace(-Ln3[0],0,nxn3[0])+Ln3[0]))
        bex1[0:nxn1[0]] = [bn1[0]]*nxn1[0]
        bex2[0:nxn2[0]] = [bn2[0]]*nxn2[0]
        bex3[0:nxn3[0]] = [bn3[0]]*nxn3[0]
        
        for i in range(1,len(nxn1)): bex1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = [bn1[i]]*nxn1[i]
        for i in range(1,len(nxn2)): bex2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = [bn2[i]]*nxn2[i]
        for i in range(1,len(nxn3)): bex3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = [bn3[i]]*nxn3[i]
        for i in range(1,len(nxn1)): b1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = b1[np.sum(nxn1[:i])-1] * np.exp(bn1[i]**(-1)*(np.linspace(-Ln1[i],0,nxn1[i])+Ln1[i]))
        for i in range(1,len(nxn2)): b2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = b2[np.sum(nxn2[:i])-1] * np.exp(bn2[i]**(-1)*(np.linspace(-Ln2[i],0,nxn2[i])+Ln2[i]))
        for i in range(1,len(nxn3)): b3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = b3[np.sum(nxn3[:i])-1] * np.exp(bn3[i]**(-1)*(np.linspace(-Ln3[i],0,nxn3[i])+Ln3[i]))
        
        N=int(N)
        M=N+1 
        
        #plotting parameters
        #vertical
        nz=51 #vertical step - only for plot
        pz1 =np.zeros((di1[-1],nz))
        for i in range(di1[-1]): pz1[i] = np.linspace(-H1,0,nz)
        pz2 =np.zeros((di2[-1],nz))
        for i in range(di2[-1]): pz2[i] = np.linspace(-H2,0,nz)
        pz3 =np.zeros((di3[-1],nz))
        for i in range(di3[-1]): pz3[i] = np.linspace(-H3,0,nz)
        
        #horizontal
        px1 = np.zeros((np.sum(nxn1),nz))
        px1[0:nxn1[0]] = -np.linspace(np.sum(Ln1[0:]), np.sum(Ln1[0+1:]), nxn1[0]).repeat(nz).reshape((di1[1],nz)) #here i can use the di list
        for i in range(1,len(nxn1)): px1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = -np.linspace(np.sum(Ln1[i:]), np.sum(Ln1[i+1:]), nxn1[i]).repeat(nz).reshape((nxn1[i],nz))
        px2 = np.zeros((np.sum(nxn2),nz))
        px2[0:nxn2[0]] = -np.linspace(np.sum(Ln2[0:]), np.sum(Ln2[0+1:]), nxn2[0]).repeat(nz).reshape((di2[1],nz)) #here i can use the di list
        for i in range(1,len(nxn2)): px2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = -np.linspace(np.sum(Ln2[i:]), np.sum(Ln2[i+1:]), nxn2[i]).repeat(nz).reshape((nxn2[i],nz))
        px3 = np.zeros((np.sum(nxn3),nz))
        px3[0:nxn3[0]] = -np.linspace(np.sum(Ln3[0:]), np.sum(Ln3[0+1:]), nxn3[0]).repeat(nz).reshape((di3[1],nz)) #here i can use the di list
        for i in range(1,len(nxn3)): px3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = -np.linspace(np.sum(Ln3[i:]), np.sum(Ln3[i+1:]), nxn3[i]).repeat(nz).reshape((nxn3[i],nz))
        
        px1, px2, px3 = px1+np.sum(Ln1) , px2+np.sum(Ln1)+np.sum(Ln2) , px3+np.sum(Ln1)+np.sum(Ln3)
        px1,px2,px3=px1/1000 , px2/1000, px3/1000
        
        
        
        #salt calculated
        sb1 = np.reshape(sss[:di1[-1]*M],(di1[-1],M))[:,0] *soc
        sn1 = np.reshape(sss[:di1[-1]*M],(di1[-1],M))[:,1:] *soc
        sb2 = np.reshape(sss[di1[-1]*M:(di1[-1]+di2[-1])*M],(di2[-1],M))[:,0] *soc
        sn2 = np.reshape(sss[di1[-1]*M:(di1[-1]+di2[-1])*M],(di2[-1],M))[:,1:] *soc
        sb3 = np.reshape(sss[(di1[-1]+di2[-1])*M:-3],(di3[-1],M))[:,0] *soc
        sn3 = np.reshape(sss[(di1[-1]+di2[-1])*M:-3],(di3[-1],M))[:,1:] *soc
        
        sigma = np.linspace(-1,0,nz)
        sp1 = np.array([np.sum([sn1[i,n-1]*np.cos(np.pi*n*sigma) for n in range(1,M)],0) for i in range(di1[-1])])
        sp2 = np.array([np.sum([sn2[i,n-1]*np.cos(np.pi*n*sigma) for n in range(1,M)],0) for i in range(di2[-1])])
        sp3 = np.array([np.sum([sn3[i,n-1]*np.cos(np.pi*n*sigma) for n in range(1,M)],0) for i in range(di3[-1])])
        
        #stratification
        strat1 = (sp1[:,0]-sp1[:,-1])/soc#/sb1
        strat2 = (sp2[:,0]-sp2[:,-1])/soc#/sb2
        strat3 = (sp3[:,0]-sp3[:,-1])/soc#/sb3
        
        s1 = sp1+sb1[:,np.newaxis]
        s2 = sp2+sb2[:,np.newaxis]
        s3 = sp3+sb3[:,np.newaxis]
        
        # =============================================================================
        #     #salt flux 
        # =============================================================================
        #addtional parameters
        dx1,dx2,dx3 = np.zeros(np.sum(nxn1)),np.zeros(np.sum(nxn2)),np.zeros(np.sum(nxn3))
        dx1[0:nxn1[0]],dx2[0:nxn2[0]],dx3[0:nxn3[0]] = dxn1[0],dxn2[0],dxn3[0]
        for i in range(1,len(nxn1)): dx1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = dxn1[i]
        for i in range(1,len(nxn2)): dx2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = dxn2[i]
        for i in range(1,len(nxn3)): dx3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = dxn3[i]
        
        #river discharge distribution - lets assume our equation is correct.
        
        #build parameters
        u_bar1, u_bar2, u_bar3 = Q1/(H1*b1), Q2/(H2*b2), Q3/(H3*b3)
        alf1, alf2, alf3 = g*Be*H1**3/(48*cv*Ut1*H1) , g*Be*H2**3/(48*cv*Ut2*H2) , g*Be*H3**3/(48*cv*Ut3*H3)
        Av1, Av2, Av3 = cv*Ut1*H1+np.zeros(len(b1)), cv*Ut2*H2+np.zeros(len(b2)), cv*Ut3*H3+np.zeros(len(b3))
        Kv1, Kv2, Kv3 = Av1/Sc, Av2/Sc, Av3/Sc
        Kh1, Kh2, Kh3 = ch2*Ut1**2+np.zeros(di1[-1]), ch2*Ut2**2+np.zeros(di2[-1]), ch2*Ut3**2+np.zeros(di3[-1])
        Kh2[di2[-2]:], Kh3[di3[-2]:]= ch2*Ut2**2*b2[di2[-2]:]/b2[di2[-2]], ch2*Ut3**2*b3[di3[-2]:]/b3[di3[-2]]
        #Kh1, Kh2, Kh3= ch*Ut*b1, ch*Ut*b2, ch*Ut*b3
        sf1,sf2,sf3 = 2*cv*Ut1 , 2*cv*Ut2 , 2*cv*Ut3
        
        #lists of ks and ns and pis
        kkp = np.linspace(1,N,N)*np.pi #k*pi
        nnp = np.linspace(1,N,N)*np.pi #n*pi
        pkn = np.array([kkp]*N) + np.transpose([nnp]*N) #pi*(n+k)
        pnk = np.array([nnp]*N) - np.transpose([kkp]*N) #pi*(n-k)
        np.fill_diagonal(pkn,None),np.fill_diagonal(pnk,None)
        
        #coefficients
        #new partial slip coefficients
        #'''
        rr1, rr2, rr3 = Av1/(sf1*H1) , Av2/(sf2*H2) ,Av3/(sf3*H3)
        g11, g12, g13 = -1 + (1.5+3*rr1) / (1+ 3 *rr1), -1 + (1.5+3*rr2) / (1+ 3 *rr2), -1 + (1.5+3*rr3) / (1+ 3 *rr3)
        g21, g22, g23 =  -3 / (2+6*rr1) , -3 / (2+6*rr2), -3 / (2+6*rr3)
        g31, g32, g33 = (1+4*rr1) / (1+3*rr1) * (9+18*rr1) - 8 - 24*rr1, (1+4*rr2) / (1+3*rr2) * (9+18*rr2) - 8 - 24*rr2, (1+4*rr3) / (1+3*rr3) * (9+18*rr3) - 8 - 24*rr3
        g41, g42, g43 = -9 * (1+4*rr1) / (1+3*rr1), -9 * (1+4*rr2) / (1+3*rr2), -9 * (1+4*rr3) / (1+3*rr3)
        g51, g52, g53 = - 8 , -8, -8
        g61, g62, g63 = 4+4*rr1 -12*(0.5+rr1)**2/(1+3*rr1), 4+4*rr2 -12*(0.5+rr2)**2/(1+3*rr2), 4+4*rr3 -12*(0.5+rr3)**2/(1+3*rr3)
        g71, g72, g73 = 4 , 4, 4
        g81, g82, g83 = (3+6*rr1) / (1+3*rr1) , (3+6*rr2) / (1+3*rr2), (3+6*rr3) / (1+3*rr3)
        #'''
        
        #channel 1
        sb1_x = np.concatenate([[0],(sb1[2:]-sb1[:-2])/(2*dx1[1:-1]),[0]])     #derivative of sb
        
        #method 2: calculate flux directly from Fourier modes. Works better. 
        F1_1 = b1*H1*u_bar1*sb1
        F2_1 = b1*H1*np.sum(sn1*(2*u_bar1[:,np.newaxis]*g21[:,np.newaxis]*np.cos(nnp)/nnp**2 \
                              + alf1*sb1_x[:,np.newaxis]*(2*g41[:,np.newaxis]*np.cos(nnp)/nnp**2 + g51*((6*np.cos(nnp)-6)/nnp**4 - 3*np.cos(nnp)/nnp**2))),1)
        F3_1 = b1*H1*- Kh1*sb1_x
        #remove edge points
        F1_1[[0,-1]],F2_1[[0,-1]],F3_1[[0,-1]] = None , None , None
        F1_1[di1[1:-1]],F2_1[di1[1:-1]],F3_1[di1[1:-1]] = None, None, None
        F1_1[di1[1:-1]-1],F2_1[di1[1:-1]-1],F3_1[di1[1:-1]-1] = None, None, None    
        
        #channel 2
        sb2_x = np.concatenate([[0],(sb2[2:]-sb2[:-2])/(2*dx2[1:-1]),[0]])     #derivative of sb
        #b2_x =  np.concatenate([[0],(b2[2:]-b2[:-2])/(2*dx2[1:-1]),[0]]) #beter analytisch
        
        #method 2: calculate flux directly from Fourier modes. Works better. 
        F1_2 = b2*H2*u_bar2*sb2
        F2_2 = b2*H2*np.sum(sn2*(2*u_bar2[:,np.newaxis]*g22[:,np.newaxis]*np.cos(nnp)/nnp**2 \
                              + alf2*sb2_x[:,np.newaxis]*(2*g42[:,np.newaxis]*np.cos(nnp)/nnp**2 + g52*((6*np.cos(nnp)-6)/nnp**4 - 3*np.cos(nnp)/nnp**2))),1)
        F3_2 = b2*H2*- Kh2*sb2_x #- H2*Kh2*sb2*b2_x
        #remove boundary points
        F1_2[[0,-1]],F2_2[[0,-1]],F3_2[[0,-1]] = None , None , None
        F1_2[di2[1:-1]],F2_2[di2[1:-1]],F3_2[di2[1:-1]] = None, None, None
        F1_2[di2[1:-1]-1],F2_2[di2[1:-1]-1],F3_2[di2[1:-1]-1] = None, None, None
        
        #channel 3
        sb3_x = np.concatenate([[0],(sb3[2:]-sb3[:-2])/(2*dx3[1:-1]),[0]])     #derivative of sb
        #b3_x =  np.concatenate([[0],(b3[2:] - b3[:-2])/(2*dx3[1:-1]),[0]])  
         
        #method 2: calculate flux directly from Fourier modes. Works better. 
        F1_3 = b3*H3*u_bar3*sb3
        F2_3 = b3*H3*np.sum(sn3*(2*u_bar3[:,np.newaxis]*g23[:,np.newaxis]*np.cos(nnp)/nnp**2 \
                           + alf3*sb3_x[:,np.newaxis]*(2*g43[:,np.newaxis]*np.cos(nnp)/nnp**2 + g53*((6*np.cos(nnp)-6)/nnp**4 - 3*np.cos(nnp)/nnp**2))),1)
        F3_3 = b3*H3*- Kh3*sb3_x #- H3*Kh3*sb3*b3_x
        
        F1_3[[0,-1]],F2_3[[0,-1]],F3_3[[0,-1]] = None , None , None
        F1_3[di3[1:-1]],F2_3[di3[1:-1]],F3_3[di3[1:-1]] = None, None, None
        F1_3[di3[1:-1]-1],F2_3[di3[1:-1]-1],F3_3[di3[1:-1]-1] = None, None, None
        
        
        flux1,flux2,flux3 = (F1_1+F2_1+F3_1) , (F1_2+F2_2+F3_2) , (F1_3+F2_3+F3_3) 
        #some checks if the code works
        if sri !=0: print('WARNING: flux calculations might be nonsense becasue there is a net salt transport through the river')
        if len(flux1)!=np.sum(flux1==None) and len(F1_1)!=np.sum(F1_1==None):
            if (100*np.nanmax(np.abs(flux1))/F1_1[np.nanargmax(np.abs(flux1))]) > 1: print('WARNING: errors in flux are relatively high: '
                                                                                       , 100*np.nanmax(np.abs(flux1))/F1_1[np.nanargmax(np.abs(flux1))]) 
        if (100*(flux2[10]+flux3[10])/(0.5*(np.abs(F1_2[10])+np.abs(F1_3[10]))))>1 : print('WARNING: errors in flux are relatively high: '
                                                                                           , 100*(flux2[10]+flux3[10])/(0.5*(np.abs(F1_2[10])+np.abs(F1_3[10])))) 
        
        #calculate parameters
        if np.max(s1)>2 and np.min(s1)>-0.5:
            X2 = (px1[-1,0]-px1[:,0][np.where(s1[:,0]>2)[0][0]])
        else: 
            X2 = None
        s_chan2 = np.mean((b2[:,np.newaxis]*s2)[:di2[-2]])/np.mean(b2[:di2[-2]])
        s_chan3 = np.mean((b3[:,np.newaxis]*s3)[:di3[-2]])/np.mean(b3[:di3[-2]])
        flux23 = flux2[10]
        width_ratio = (b02/b03)
        length_ratio = H2/H3#np.sum(Ln2[:-1])/np.sum(Ln3[:-1])
        dd_current1 = alf1*sb1_x[-2]
        dd_current2 = alf2*sb2_x[1]
        dd_current3 = alf3*sb3_x[1]
                
        flux23_norm = flux2[10]/F1_2[10]
        out_plot[run] = [X2,s_chan2,s_chan3, flux23, flux23_norm,strat1[-1], width_ratio,length_ratio, Ut1, Q, 
                         bs2[-2],bs3[-2],H[1],H[2],np.sum(Ln2[:-1]),np.sum(Ln3[:-1]),Q1,Q2,Q3,np.mean(s1[-1]), dd_current1,dd_current2,dd_current3,Ut1,Ut2,Ut3]
        #save the horizontal flux 
        flux_save.append([np.array([F1_1,F2_1,F3_1,flux1]) , np.array([F1_2,F2_2,F3_2,flux2]) , np.array([F1_3,F2_3,F3_3,flux3])])
        
        #build relevant numbers for length scale
        out_ls[run] = np.array([[Q2,Ut2,H2,np.sum(Ln2[:-1]),b02,bs2[-2],soc], [Q3,Ut3,H3,np.sum(Ln3[:-1]),b03,bs3[-2],soc]])

    #remove runs which did not converge -  can also be done with masked arrays 
    out_plot[np.where(np.abs(out_plot[:,1])>soc+2)[0],:5] = None
    
    F1j_list,F2j_list,F3j_list =[] , [] , []
    for i in range(Nruns):
        F11now = flux_save[i][0][0][-2]
        F12now = flux_save[i][1][0][1]
        F13now = flux_save[i][2][0][1]
        F21now = flux_save[i][0][1][-2]
        F22now = flux_save[i][1][1][1]
        F23now = flux_save[i][2][1][1]
        F31now = flux_save[i][0][2][-2]
        F32now = flux_save[i][1][2][1]
        F33now = flux_save[i][2][2][1]
        F1j_list.append([F11now,F12now,F13now])
        F2j_list.append([F21now,F22now,F23now])
        F3j_list.append([F31now,F32now,F33now])

    F1j_list ,F2j_list ,F3j_list = np.array(F1j_list),np.array(F2j_list),np.array(F3j_list)
    flux_junc = (F1j_list ,F2j_list ,F3j_list)
    
    #return relevant nubmers
    return out_plot,flux_save,flux_junc,out_ls

#run the party
#dat = (load_reg('real_est/Yangtze47')) , load_reg('real_est/Yangtze48') , load_reg('real_est/Modao47') , (load_reg('real_est/Modao48') )
dat = (load_reg('Yangtze_geoNP_fin')) , load_reg('Yangtze_geoSP_fin') , load_reg('Modao_geoMC_Q2000') , (load_reg('Modao_geoHC_Q2000') )

Nruns=21*21
flux_try2 = np.zeros((len(dat),3,Nruns))
flux_try3 = np.zeros((len(dat),3,Nruns))
for i in range(len(dat)):
    for j in range(3):
        for k in range(Nruns): 
            flux_try2[i,j,k] = dat[i][1][k][1][j][1]
            flux_try3[i,j,k] = dat[i][1][k][2][j][1]
branch2_MW = [2000,7]
branch3_MW = [3500,5]
branch2_YE = [3500,11]
branch3_YE = [30000,7]
            
#%%calculate length scales
import scipy.integrate as integrate
g,Be,soc,cv,Sc,ch2 = 9.81,7.6e-4,35,7.28e-5,2.2,250
r= 1/2#(r=0=no slip, r=1/2=FWP slip)
g1,g2,g3,g4,g5 = -1 + (1.5+3*r) / (1+ 3 *r) ,  -3 / (2+6*r), (1+4*r) / (1+3*r) * (9+18*r) - 8 - 24*r , -9 * (1+4*r) / (1+3*r) , -8
p2p4 = integrate.quad(lambda z: (g3+g4*z**2+g5*z**3)*(-g3/6 -g4/60+g5/120 + g3/2*z**2 + g4/12 *z**4 +g5/20 *z**5)   , -1, 0) [0]
p1p3 = integrate.quad(lambda z: (g1 + g2*z**2)*(-g1/6 -g2/60 + g1/2*z**2 +g2/12*z**4)   , -1, 0) [0]
p2p3 = integrate.quad(lambda z: (g3+g4*z**2+g5*z**3)*(-g1/6 -g2/60 + g1/2*z**2 +g2/12*z**4)  , -1, 0) [0]
p1p4 = integrate.quad(lambda z: (g1 + g2*z**2)*(-g3/6 -g4/60+g5/120 + g3/2*z**2 + g4/12 *z**4 +g5/20 *z**5)   , -1, 0) [0]

#not sure if straight channel gives problems

#define solver qubic equations
def q_solv(a,b,c,d): #finds only the real solution, I think that is sufficient for now. equivalent to np.roots
    #define variables
    e=1/a * (c - b**2/(3*a))
    f=1/a * (d + 2*b**3/(27*a**2) - b*c/(3*a))
    w1 = -0.5*f + 0.5*np.sqrt(f**2 + 4/27*e**3 + 0j)
    w2 = -0.5*f - 0.5*np.sqrt(f**2 + 4/27*e**3 + 0j)
    z1,z2 = w1**(1/3),w2**(1/3) #we do not get all the solutions here 
    y1,y2 = z1-e/(3*z1) , z2-e/(3*z2)
    x1,x2 = y1-b/(3*a) , y2-b/(3*a)
    
    #check
    if z1 ==0. or z2==0. : print('error: qsolve does not work', x1,x2)
    #select the real solution
    return np.real(x1) if np.imag(x1)==0 else np.real(x2)

def pred_T(geg2,geg3):
    #load
    Q2,Ut2,H2,Le2,b02,bi2,soc2 = geg2
    Q3,Ut3,H3,Le3,b03,bi3,soc3 = geg3
    if soc2!=soc3: print('ERROR!')
    soc=soc2
    if Q2<0: 
        T2Q = -Q2*soc
        situ2 = 1
    elif  Q3<0: 
        T2Q = Q3*soc
        situ3 = 0
    else: T2Q=np.nan  
    
    #avoid straight channel problems
    if b02==bi2: bi2 = bi2+1e-6
    if b03==bi3: bi3 = bi3+1e-6
    
    #build mixing
    Kh2 = ch2*Ut2**2
    Kh3 = ch2*Ut3**2
    Av2 = cv*Ut2*H2
    Av3 = cv*Ut3*H3
    Kv2 = Av2/Sc
    Kv3 = Av3/Sc
    #build length scales
    LD2 = Kh2/(Q2/(H2*b02))
    LDs2 = Kh2/(Q2/(H2*bi2)) #actually not true, Kh2 is different in the open ocean
    Lb2 = Le2/np.log(bi2/b02)
    Lbs2 = 2500#25000/10
    
    LD3 = Kh3/(Q3/(H3*b03))
    LDs3 = Kh3/(Q3/(H3*bi3)) #actually not true, Kh3 is different in the open ocean
    Lb3 = Le3/np.log(bi3/b03)
    Lbs3 = 2500#25000/10
    
    #calculate \nu in a MC04 way
    dsdx2 = q_solv((-p2p4*np.sqrt(g*Be*H2*soc)/(48**2*Q2/(H2*bi2)*Av2**2*Kv2)) * (H2**2*np.sqrt(g*Be*H2*soc))**3 ,
                   (-p1p4-p2p3)/(48*Kv2*Av2) * (g*Be*H2*soc) * H2**4 + (g*Be*H2*soc)*H2**4/(Kv2*Av2)*(g3/144 + g4/720 - g5/1152) , 
                   -p1p3*H2**2/Kv2*Q2/(H2*bi2) + Kh2*bi2*H2/Q2 + Q2/(H2*bi2)*H2**2/Kv2*(g1/3+g2/15) ,
                   -1)
    dsdx3 = q_solv((-p2p4*np.sqrt(g*Be*H3*soc)/(48**2*Q3/(H3*bi3)*Av3**2*Kv3)) * (H3**2*np.sqrt(g*Be*H3*soc))**3 ,
                   (-p1p4-p2p3)/(48*Kv3*Av3) * (g*Be*H3*soc) * H3**4 + (g*Be*H3*soc)*H3**4/(Kv3*Av3)*(g3/144 + g4/720 - g5/1152) , 
                   -p1p3*H3**2/Kv3*Q3/(H3*bi3) + Kh3*bi3*H3/Q3 + Q3/(H3*bi3)*H3**2/Kv3*(g1/3+g2/15) ,
                   -1)
    nu2 = (-p2p4*np.sqrt(g*Be*H2*soc)/(48**2*Q2/(H2*bi2)*Av2**2*Kv2)) * (H2**2*np.sqrt(g*Be*H2*soc))**3 * dsdx2**3 \
        + (-p1p4-p2p3)/(48*Kv2*Av2) * (g*Be*H2*soc) * H2**4 * dsdx2**2  \
            + (-p1p3*H2**2/Kv2*Q2/(H2*bi2) + Kh2*bi2*H2/Q2) * dsdx2
    nu3 = (-p2p4*np.sqrt(g*Be*H3*soc)/(48**2*Q3/(H3*bi3)*Av3**2*Kv3)) * (H3**2*np.sqrt(g*Be*H3*soc))**3 * dsdx3**3 \
        + (-p1p4-p2p3)/(48*Kv3*Av3) * (g*Be*H3*soc) * H3**4 * dsdx3**2  \
            + (-p1p3*H3**2/Kv3*Q3/(H3*bi3) + Kh3*bi3*H3/Q3) * dsdx3       
            
    #calculate coefficients in overspill equation
    nu2 = 1#np.exp(-Lbs2/LDs2)
    nu3 = 1#np.exp(-Lbs3/LDs3)
    mu2 = nu2*np.exp(Lb2/LD2*(np.exp(-Le2/Lb2)-1))
    mu3 = nu3*np.exp(Lb3/LD3*(np.exp(-Le3/Lb3)-1))
    ga2 = -2*Lb2 * (- Q2/(b02*H2) * (Kv2*Av2**2*48**2)/(g**2*Be**2*H2**8*p2p4*soc**2))**(1/3) *(np.exp(-1/3*Le2/Lb2)-1)
    ga3 = -2*Lb3 * (- Q3/(b03*H3) * (Kv3*Av3**2*48**2)/(g**2*Be**2*H3**8*p2p4*soc**2))**(1/3) *(np.exp(-1/3*Le3/Lb3)-1)
    
    #calculate salinity with T=0
    s2D = mu2 
    s3D = mu3
    s2E = (nu2**(2/3) - ga2)**(3/2) if nu2**(2/3) > ga2 else 0
    s3E = (nu3**(2/3) - ga3)**(3/2) if nu3**(2/3) > ga3 else 0
      
    dom2 = 'E' if s2E>s2D else 'D'
    dom3 = 'E' if s3E>s3D else 'D'
    #calcualte overspiol for different cases
    if dom2 =='D' and dom3=='D': #situation 1
        situ2,situ3 = 3,2
        fact = ((1-mu2)/Q2 + (1-mu3)/Q3)**-1
        T2 = (mu3-mu2) * ((1-mu2)/Q2 + (1-mu3)/Q3)**-1
    elif dom2 =='D' and dom3 == 'E': #sitaution 2a
        situ2,situ3 = 3, 1
        fact = ((1-nu3**(2/3)*(nu3**(2/3)-ga3)**(1/2))/Q3 + (1-mu2)/Q2)**-1
        T2 = ((nu3**(2/3)-ga3)**(3/2) - mu2) * ((1-nu3**(2/3)*(nu3**(2/3)-ga3)**(1/2))/Q3 + (1-mu2)/Q2)**-1
    elif dom2 =='E' and dom3 == 'D': #sitaution 2b
        if 'situ2' not in locals():
            situ2=2
        if 'situ3' not in locals():
            situ3=2
        fact = ((1-nu2**(2/3)*(nu2**(2/3)-ga2)**(1/2))/Q2 + (1-mu3)/Q3)**-1
        T2 = (mu3 - (nu2**(2/3)-ga2)**(3/2) ) * ((1-nu2**(2/3)*(nu2**(2/3)-ga2)**(1/2))/Q2 + (1-mu3)/Q3)**-1
    elif dom2 =='E' and dom3=='E': #situation 3
        situ2,situ3 = 2,1
        fact = ((1-nu2**(2/3)*(nu2**(2/3)-ga2)**(1/2))/Q2 + (1-nu3**(2/3)*(nu3**(2/3)-ga3)**(1/2))/Q3)**-1
        T2 = ((nu3**(2/3)-ga3)**(3/2) - (nu2**(2/3)-ga2)**(3/2)) * ((1-nu2**(2/3)*(nu2**(2/3)-ga2)**(1/2))/Q2 + (1-nu3**(2/3)*(nu3**(2/3)-ga3)**(1/2))/Q3)**-1
    else: print('ERROR')
    
    T2 = np.nanmax([T2,T2Q])
    
    #convert situ to the old ones

    if situ2 +situ3 *3 == 5: situ_old  = 4
    elif situ2 +situ3 *3 == 6: situ_old  = 2
    elif situ2 +situ3 *3 == 8: situ_old  = 3
    elif situ2 +situ3 *3 == 9: situ_old  = 1
    else:  situ_old  = None
    
    return T2*soc , situ2+situ3*3 , soc*np.max([s2D,s2E]), soc*np.max([s3D,s3E]) , fact ,situ_old

pT_NP, pT_SP, pT_MC, pT_HC = np.zeros((21*21,6)),np.zeros((21*21,6)),np.zeros((21*21,6)),np.zeros((21*21,6))
for i in range(21*21):
    pT_NP[i] = pred_T(dat[0][3][i][0],dat[0][3][i][1])
    pT_SP[i] = pred_T(dat[1][3][i][0],dat[1][3][i][1])
    pT_MC[i] = pred_T(dat[2][3][i][0],dat[2][3][i][1])
    pT_HC[i] = pred_T(dat[3][3][i][0],dat[3][3][i][1])
#

#%% fig S3
fig,ax = plt.subplots(1,4,figsize=(11,4))

a00 = ax[0].contourf(dat[0][0][:,10].reshape((21,21)),dat[0][0][:,12].reshape((21,21)),
                                 pT_NP[:,0].reshape((21,21)),cmap='Blues',levels = np.linspace(0,6500,14) )#\
a01 = ax[1].contourf(dat[1][0][:,11].reshape((21,21)),dat[1][0][:,13].reshape((21,21)),
                                pT_SP[:,0].reshape((21,21)),cmap='Blues',levels = np.linspace(-1000,8000,10) )#\
a02 = ax[2].contourf(dat[2][0][:,10].reshape((21,21)),dat[2][0][:,12].reshape((21,21)),
                                 pT_MC[:,0].reshape((21,21)),cmap='Blues',levels = np.linspace(-1000,5500,14) )#\
a03 = ax[3].contourf(dat[3][0][:,11].reshape((21,21)),dat[3][0][:,13].reshape((21,21)),
                                pT_HC[:,0].reshape((21,21)),cmap='Blues',levels = np.linspace(-5000,50000,12) )#\

ax[2].contour(dat[2][0][:,10].reshape((21,21)),dat[2][0][:,12].reshape((21,21)),
                                dat[2][0][:,-8].reshape((21,21)),colors='black',levels = [0] )#\
ax[2].text(2000,10,r'$Q_{HC}<0$',c='white')

ax[0].set_ylabel('$H$ [m]',fontsize=15)
ax[0].set_xlabel('$b_d$ [m]',fontsize=15),ax[1].set_xlabel('$b_d$ [m]',fontsize=15),ax[2].set_xlabel('$b_d$ [m]',fontsize=15),ax[3].set_xlabel('$b_d$ [m]',fontsize=15)

ax[0].text(2100,16.1,'(a) NP',c='white',fontsize=15) 
ax[1].text(6900,11.8,'(b) SP',c='black',fontsize=15)
ax[2].text(1090,11.8,'(c) MC',c='white',fontsize=15) 
ax[3].text(700,9.3,'(d) HC',c='white',fontsize=15) 

#ax[0,0].tick_params(axis='both', which='major', labelsize=14) 
#ax[1,0].tick_params(axis='both', which='major', labelsize=14) 
#ax[0,1].tick_params(axis='both', which='major', labelsize=14) 
#ax[1,1].tick_params(axis='both', which='major', labelsize=14) 

branch2_MW = [2000,7]
branch3_MW = [3500,5]
branch2_YE = [3500,11]
branch3_YE = [30000,7]
ax[0].scatter(*branch2_YE,c='black',marker='s')
ax[1].scatter(*branch3_YE,c='black',marker='s')
ax[2].scatter(*branch2_MW,c='black',marker='s')
ax[3].scatter(*branch3_MW,c='black',marker='s')

for i in range(4): ax[i].set_facecolor('black')
#ax[1,2].text(1800,10,'$Q_{HC}<0$',c='white',fontsize=15)

cb00 = fig.colorbar(a00, ax=ax[0],orientation='horizontal',pad=0.18)
cb00.set_label(label='Overspill [kg s$^{-1}$]',fontsize=15)#label='Overspill [?]',fontsize=15)
cb00._get_ticker_locator_formatter()[0].set_params(nbins=5)
cb00.update_ticks()
cb01 = fig.colorbar(a01, ax=ax[1],orientation='horizontal',pad=0.18)
cb01.set_label(label='Overspill [kg s$^{-1}$]',fontsize=15)#label='Overspill [?]',fontsize=15)
cb01._get_ticker_locator_formatter()[0].set_params(nbins=6)
cb01.update_ticks()
cb02 = fig.colorbar(a02, ax=ax[2],orientation='horizontal',pad=0.18)
cb02.set_label(label='Overspill [kg s$^{-1}$]',fontsize=15)#label='Overspill [?]',fontsize=15)
cb02._get_ticker_locator_formatter()[0].set_params(nbins=6)
cb02.update_ticks()
cb03 = fig.colorbar(a03, ax=ax[3],orientation='horizontal',pad=0.18)
cb03.set_label(label='Overspill [kg s$^{-1}$]',fontsize=15)#label='Overspill [?]',fontsize=15)
cb03._get_ticker_locator_formatter()[0].set_params(nbins=5)
cb03.update_ticks()


plt.tight_layout()
#plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper fork channel/figs_def/figS3.jpg',dpi=300,bbox_inches='tight')
plt.show()





