#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 15:39:13 2022

@author: biemo004
"""


#analysis of time-independent runs of fork system
#here: Ut,Q constant, L,b,h vaired
#standard packages
import numpy as np                      #standard operations       
import matplotlib.pyplot as plt         #plot figures
import pandas as pd                     #import datasets
from scipy import optimize   , interpolate 
import scipy as sp          #do fits
import scipy.io                         #import matlab datasets

import matplotlib.animation as ani       #make animations
from matplotlib.collections import LineCollection
import matplotlib.patheffects as pe
#import cv2                               #import images as matrices
import time                              #measure time of operation
import os as os                          #making folders etc

#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water
ch2=250
# reorder the output
#reor1 = np.tile([5,4,3,2,1,0,6,7,8,9,10],10)
#reor2 = reor1+np.repeat(np.arange(10),11)*11

#choose here folder
fold = 'Yangtze_Qwrong_fin/'
Nruns = 21*21

#calculate salinity
#load the files 
loc = '/Users/biemo004/Documents/UU phd Saltisolutions/Files paper fork/model_runs/'+fold
out_plot=np.zeros((Nruns,26))
out_ls = np.zeros((Nruns,2,5))

flux_save = []
for run in range(Nruns):

    N,Lsc,Ut1,Ut2,Ut3,soc,sri,u_w,d_w,Q,n_sea2,n_sea3= np.loadtxt(loc+'run'+str(run)+'/input.txt')
    #rebuild geometry - somewhat more complicated
    chan1, chan2, chan3, H = np.loadtxt(loc+'/run'+str(run)+'/geometry.txt')
    Ln1,b01,bs1,dxn1 = chan1[1:1+int(chan1[0])],chan1[1+int(chan1[0])],chan1[2+int(chan1[0]):2+2*int(chan1[0])],chan1[2+2*int(chan1[0]):2+3*int(chan1[0])]
    Ln2,b02,bs2,dxn2 = chan2[1:1+int(chan2[0])],chan2[1+int(chan2[0])],chan2[2+int(chan2[0]):2+2*int(chan2[0])],chan2[2+2*int(chan2[0]):2+3*int(chan2[0])]
    Ln3,b03,bs3,dxn3 = chan3[1:1+int(chan3[0])],chan3[1+int(chan3[0])],chan3[2+int(chan3[0]):2+2*int(chan3[0])],chan3[2+2*int(chan3[0]):2+3*int(chan3[0])]
    H1,H2,H3 = H[:3]

    #build some usefull information
    nxn1 = np.array(Ln1/dxn1+1,dtype=int)
    nxn2 = np.array(Ln2/dxn2+1,dtype=int)
    nxn3 = np.array(Ln3/dxn3+1,dtype=int)
    di1 = np.zeros(len(Ln1)+1,dtype=int) #starting indices of the domains
    di2 = np.zeros(len(Ln2)+1,dtype=int) #starting indices of the domains
    di3 = np.zeros(len(Ln3)+1,dtype=int) #starting indices of the domains
    for i in range(1,len(Ln1)):   di1[i] = np.sum(nxn1[:i])
    di1[-1] = np.sum(nxn1)
    for i in range(1,len(Ln2)):   di2[i] = np.sum(nxn2[:i])
    di2[-1] = np.sum(nxn2)
    for i in range(1,len(Ln3)):   di3[i] = np.sum(nxn3[:i])
    di3[-1] = np.sum(nxn3)
    
    #build parameters: convergence length
    bn1, bn2, bn3 = np.zeros(len(Ln1)), np.zeros(len(Ln2)), np.zeros(len(Ln3))
    bn1[0] = 9e99 if bs1[0] == b01 else Ln1[0]/np.log(bs1[0]/b01)
    bn2[0] = 9e99 if bs2[0] == b02 else Ln2[0]/np.log(bs2[0]/b02)
    bn3[0] = 9e99 if bs3[0] == b03 else Ln3[0]/np.log(bs3[0]/b03)
    for i in range(1,len(Ln1)): bn1[i] = 9e99 if bs1[i] == bs1[i-1] else Ln1[i]/np.log(bs1[i]/bs1[i-1])
    for i in range(1,len(Ln2)): bn2[i] = 9e99 if bs2[i] == bs2[i-1] else Ln2[i]/np.log(bs2[i]/bs2[i-1])
    for i in range(1,len(Ln3)): bn3[i] = 9e99 if bs3[i] == bs3[i-1] else Ln3[i]/np.log(bs3[i]/bs3[i-1])
    #build width
    b1, bex1 = np.zeros(np.sum(nxn1)),np.zeros(np.sum(nxn1))
    b2, bex2 = np.zeros(np.sum(nxn2)),np.zeros(np.sum(nxn2))
    b3, bex3 = np.zeros(np.sum(nxn3)),np.zeros(np.sum(nxn3))
    b1[0:nxn1[0]] = b01 * np.exp(bn1[0]**(-1)*(np.linspace(-Ln1[0],0,nxn1[0])+Ln1[0]))
    b2[0:nxn2[0]] = b02 * np.exp(bn2[0]**(-1)*(np.linspace(-Ln2[0],0,nxn2[0])+Ln2[0]))
    b3[0:nxn3[0]] = b03 * np.exp(bn3[0]**(-1)*(np.linspace(-Ln3[0],0,nxn3[0])+Ln3[0]))
    bex1[0:nxn1[0]] = [bn1[0]]*nxn1[0]
    bex2[0:nxn2[0]] = [bn2[0]]*nxn2[0]
    bex3[0:nxn3[0]] = [bn3[0]]*nxn3[0]
    
    for i in range(1,len(nxn1)): bex1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = [bn1[i]]*nxn1[i]
    for i in range(1,len(nxn2)): bex2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = [bn2[i]]*nxn2[i]
    for i in range(1,len(nxn3)): bex3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = [bn3[i]]*nxn3[i]
    for i in range(1,len(nxn1)): b1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = b1[np.sum(nxn1[:i])-1] * np.exp(bn1[i]**(-1)*(np.linspace(-Ln1[i],0,nxn1[i])+Ln1[i]))
    for i in range(1,len(nxn2)): b2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = b2[np.sum(nxn2[:i])-1] * np.exp(bn2[i]**(-1)*(np.linspace(-Ln2[i],0,nxn2[i])+Ln2[i]))
    for i in range(1,len(nxn3)): b3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = b3[np.sum(nxn3[:i])-1] * np.exp(bn3[i]**(-1)*(np.linspace(-Ln3[i],0,nxn3[i])+Ln3[i]))
    
    N=int(N)
    M=N+1 
    
    #plotting parameters
    #vertical
    nz=51 #vertical step - only for plot
    pz1 =np.zeros((di1[-1],nz))
    for i in range(di1[-1]): pz1[i] = np.linspace(-H1,0,nz)
    pz2 =np.zeros((di2[-1],nz))
    for i in range(di2[-1]): pz2[i] = np.linspace(-H2,0,nz)
    pz3 =np.zeros((di3[-1],nz))
    for i in range(di3[-1]): pz3[i] = np.linspace(-H3,0,nz)
    
    #horizontal
    px1 = np.zeros((np.sum(nxn1),nz))
    px1[0:nxn1[0]] = -np.linspace(np.sum(Ln1[0:]), np.sum(Ln1[0+1:]), nxn1[0]).repeat(nz).reshape((di1[1],nz)) #here i can use the di list
    for i in range(1,len(nxn1)): px1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = -np.linspace(np.sum(Ln1[i:]), np.sum(Ln1[i+1:]), nxn1[i]).repeat(nz).reshape((nxn1[i],nz))
    px2 = np.zeros((np.sum(nxn2),nz))
    px2[0:nxn2[0]] = -np.linspace(np.sum(Ln2[0:]), np.sum(Ln2[0+1:]), nxn2[0]).repeat(nz).reshape((di2[1],nz)) #here i can use the di list
    for i in range(1,len(nxn2)): px2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = -np.linspace(np.sum(Ln2[i:]), np.sum(Ln2[i+1:]), nxn2[i]).repeat(nz).reshape((nxn2[i],nz))
    px3 = np.zeros((np.sum(nxn3),nz))
    px3[0:nxn3[0]] = -np.linspace(np.sum(Ln3[0:]), np.sum(Ln3[0+1:]), nxn3[0]).repeat(nz).reshape((di3[1],nz)) #here i can use the di list
    for i in range(1,len(nxn3)): px3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = -np.linspace(np.sum(Ln3[i:]), np.sum(Ln3[i+1:]), nxn3[i]).repeat(nz).reshape((nxn3[i],nz))
    
    px1, px2, px3 = px1+np.sum(Ln1) , px2+np.sum(Ln1)+np.sum(Ln2) , px3+np.sum(Ln1)+np.sum(Ln3)
    px1,px2,px3=px1/1000 , px2/1000, px3/1000
    
    #load output
    with open(loc+'/run'+str(run)+'/output.txt') as f:
        lines = f.readlines()
        if lines[0] =='NOCON\n': 
            out_plot[run] = [np.nan,np.nan,np.nan, np.nan, np.nan,np.nan, np.nan,np.nan, Ut1, Q, 
                             bs2[-2],bs3[-2],H[1],H[2],np.sum(Ln2[:-1]),np.sum(Ln3[:-1]),Q,np.nan,np.nan,np.nan,np.nan,np.nan,np.nan,Ut1,Ut2,Ut3]
            #save the horizontal flux 
            flux_save.append([[np.nan,np.nan,np.nan,np.nan] , [np.nan,np.nan,np.nan,np.nan] , [np.nan,np.nan,np.nan,np.nan]])
            
            out_ls[run] = np.array([[np.mean(b2[:di2[-2]]),H2,None,Ut2,35], [np.mean(b3[:di3[-2]]),H3,None,Ut3,35]])
        else:
            sss = np.loadtxt(loc+'/run'+str(run)+'/output.txt')
            #load river discharge
            Q1,Q2,Q3 = sss[-3:]
            
            #salt calculated
            sb1 = np.reshape(sss[:di1[-1]*M],(di1[-1],M))[:,0] *soc
            sn1 = np.reshape(sss[:di1[-1]*M],(di1[-1],M))[:,1:] *soc
            sb2 = np.reshape(sss[di1[-1]*M:(di1[-1]+di2[-1])*M],(di2[-1],M))[:,0] *soc
            sn2 = np.reshape(sss[di1[-1]*M:(di1[-1]+di2[-1])*M],(di2[-1],M))[:,1:] *soc
            sb3 = np.reshape(sss[(di1[-1]+di2[-1])*M:-3],(di3[-1],M))[:,0] *soc
            sn3 = np.reshape(sss[(di1[-1]+di2[-1])*M:-3],(di3[-1],M))[:,1:] *soc
            
            sigma = np.linspace(-1,0,nz)
            sp1 = np.array([np.sum([sn1[i,n-1]*np.cos(np.pi*n*sigma) for n in range(1,M)],0) for i in range(di1[-1])])
            sp2 = np.array([np.sum([sn2[i,n-1]*np.cos(np.pi*n*sigma) for n in range(1,M)],0) for i in range(di2[-1])])
            sp3 = np.array([np.sum([sn3[i,n-1]*np.cos(np.pi*n*sigma) for n in range(1,M)],0) for i in range(di3[-1])])
            
            #stratification
            strat1 = (sp1[:,0]-sp1[:,-1])/soc#/sb1
            strat2 = (sp2[:,0]-sp2[:,-1])/soc#/sb2
            strat3 = (sp3[:,0]-sp3[:,-1])/soc#/sb3
            
            s1 = sp1+sb1[:,np.newaxis]
            s2 = sp2+sb2[:,np.newaxis]
            s3 = sp3+sb3[:,np.newaxis]
            
            # =============================================================================
            #     #salt flux 
            # =============================================================================
            #addtional parameters
            dx1,dx2,dx3 = np.zeros(np.sum(nxn1)),np.zeros(np.sum(nxn2)),np.zeros(np.sum(nxn3))
            dx1[0:nxn1[0]],dx2[0:nxn2[0]],dx3[0:nxn3[0]] = dxn1[0],dxn2[0],dxn3[0]
            for i in range(1,len(nxn1)): dx1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = dxn1[i]
            for i in range(1,len(nxn2)): dx2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = dxn2[i]
            for i in range(1,len(nxn3)): dx3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = dxn3[i]
            
            #river discharge distribution - lets assume our equation is correct.
            
            #build parameters
            u_bar1, u_bar2, u_bar3 = Q1/(H1*b1), Q2/(H2*b2), Q3/(H3*b3)
            alf1, alf2, alf3 = g*Be*H1**3/(48*cv*Ut1*H1) , g*Be*H2**3/(48*cv*Ut2*H2) , g*Be*H3**3/(48*cv*Ut3*H3)
            Av1, Av2, Av3 = cv*Ut1*H1+np.zeros(len(b1)), cv*Ut2*H2+np.zeros(len(b2)), cv*Ut3*H3+np.zeros(len(b3))
            Kv1, Kv2, Kv3 = Av1/Sc, Av2/Sc, Av3/Sc
            Kh1, Kh2, Kh3 = ch2*Ut1**2+np.zeros(di1[-1]), ch2*Ut2**2+np.zeros(di2[-1]), ch2*Ut3**2+np.zeros(di3[-1])
            Kh2[di2[-2]:], Kh3[di3[-2]:]= ch2*Ut2**2*b2[di2[-2]:]/b2[di2[-2]], ch2*Ut3**2*b3[di3[-2]:]/b3[di3[-2]]
            #Kh1, Kh2, Kh3= ch*Ut1*b1, ch*Ut2*b2, ch*Ut3*b3
            sf1,sf2,sf3 = 2*cv*Ut1 , 2*cv*Ut2 , 2*cv*Ut3
            
            #lists of ks and ns and pis
            kkp = np.linspace(1,N,N)*np.pi #k*pi
            nnp = np.linspace(1,N,N)*np.pi #n*pi
            pkn = np.array([kkp]*N) + np.transpose([nnp]*N) #pi*(n+k)
            pnk = np.array([nnp]*N) - np.transpose([kkp]*N) #pi*(n-k)
            np.fill_diagonal(pkn,None),np.fill_diagonal(pnk,None)
            
            #coefficients
            #new partial slip coefficients
            #'''
            rr1, rr2, rr3 = Av1/(sf1*H1) , Av2/(sf2*H2) ,Av3/(sf3*H3)
            g11, g12, g13 = -1 + (1.5+3*rr1) / (1+ 3 *rr1), -1 + (1.5+3*rr2) / (1+ 3 *rr2), -1 + (1.5+3*rr3) / (1+ 3 *rr3)
            g21, g22, g23 =  -3 / (2+6*rr1) , -3 / (2+6*rr2), -3 / (2+6*rr3)
            g31, g32, g33 = (1+4*rr1) / (1+3*rr1) * (9+18*rr1) - 8 - 24*rr1, (1+4*rr2) / (1+3*rr2) * (9+18*rr2) - 8 - 24*rr2, (1+4*rr3) / (1+3*rr3) * (9+18*rr3) - 8 - 24*rr3
            g41, g42, g43 = -9 * (1+4*rr1) / (1+3*rr1), -9 * (1+4*rr2) / (1+3*rr2), -9 * (1+4*rr3) / (1+3*rr3)
            g51, g52, g53 = - 8 , -8, -8
            g61, g62, g63 = 4+4*rr1 -12*(0.5+rr1)**2/(1+3*rr1), 4+4*rr2 -12*(0.5+rr2)**2/(1+3*rr2), 4+4*rr3 -12*(0.5+rr3)**2/(1+3*rr3)
            g71, g72, g73 = 4 , 4, 4
            g81, g82, g83 = (3+6*rr1) / (1+3*rr1) , (3+6*rr2) / (1+3*rr2), (3+6*rr3) / (1+3*rr3)
            #'''
            
            #channel 1
            sb1_x = np.concatenate([[0],(sb1[2:]-sb1[:-2])/(2*dx1[1:-1]),[0]])     #derivative of sb
            
            #method 2: calculate flux directly from Fourier modes. Works better. 
            F1_1 = b1*H1*u_bar1*sb1
            F2_1 = b1*H1*np.sum(sn1*(2*u_bar1[:,np.newaxis]*g21[:,np.newaxis]*np.cos(nnp)/nnp**2 \
                                  + alf1*sb1_x[:,np.newaxis]*(2*g41[:,np.newaxis]*np.cos(nnp)/nnp**2 + g51*((6*np.cos(nnp)-6)/nnp**4 - 3*np.cos(nnp)/nnp**2))),1)
            F3_1 = b1*H1*- Kh1*sb1_x
            #remove edge points
            F1_1[[0,-1]],F2_1[[0,-1]],F3_1[[0,-1]] = None , None , None
            F1_1[di1[1:-1]],F2_1[di1[1:-1]],F3_1[di1[1:-1]] = None, None, None
            F1_1[di1[1:-1]-1],F2_1[di1[1:-1]-1],F3_1[di1[1:-1]-1] = None, None, None    
            
            #channel 2
            sb2_x = np.concatenate([[0],(sb2[2:]-sb2[:-2])/(2*dx2[1:-1]),[0]])     #derivative of sb
            #b2_x =  np.concatenate([[0],(b2[2:]-b2[:-2])/(2*dx2[1:-1]),[0]]) #beter analytisch
            
            #method 2: calculate flux directly from Fourier modes. Works better. 
            F1_2 = b2*H2*u_bar2*sb2
            F2_2 = b2*H2*np.sum(sn2*(2*u_bar2[:,np.newaxis]*g22[:,np.newaxis]*np.cos(nnp)/nnp**2 \
                                  + alf2*sb2_x[:,np.newaxis]*(2*g42[:,np.newaxis]*np.cos(nnp)/nnp**2 + g52*((6*np.cos(nnp)-6)/nnp**4 - 3*np.cos(nnp)/nnp**2))),1)
            F3_2 = b2*H2*- Kh2*sb2_x #- H2*Kh2*sb2*b2_x
            #remove boundary points
            F1_2[[0,-1]],F2_2[[0,-1]],F3_2[[0,-1]] = None , None , None
            F1_2[di2[1:-1]],F2_2[di2[1:-1]],F3_2[di2[1:-1]] = None, None, None
            F1_2[di2[1:-1]-1],F2_2[di2[1:-1]-1],F3_2[di2[1:-1]-1] = None, None, None
            
            #channel 3
            sb3_x = np.concatenate([[0],(sb3[2:]-sb3[:-2])/(2*dx3[1:-1]),[0]])     #derivative of sb
            #b3_x =  np.concatenate([[0],(b3[2:] - b3[:-2])/(2*dx3[1:-1]),[0]])  
             
            #method 2: calculate flux directly from Fourier modes. Works better. 
            F1_3 = b3*H3*u_bar3*sb3
            F2_3 = b3*H3*np.sum(sn3*(2*u_bar3[:,np.newaxis]*g23[:,np.newaxis]*np.cos(nnp)/nnp**2 \
                               + alf3*sb3_x[:,np.newaxis]*(2*g43[:,np.newaxis]*np.cos(nnp)/nnp**2 + g53*((6*np.cos(nnp)-6)/nnp**4 - 3*np.cos(nnp)/nnp**2))),1)
            F3_3 = b3*H3*- Kh3*sb3_x #- H3*Kh3*sb3*b3_x
            
            F1_3[[0,-1]],F2_3[[0,-1]],F3_3[[0,-1]] = None , None , None
            F1_3[di3[1:-1]],F2_3[di3[1:-1]],F3_3[di3[1:-1]] = None, None, None
            F1_3[di3[1:-1]-1],F2_3[di3[1:-1]-1],F3_3[di3[1:-1]-1] = None, None, None
            
            
            flux1,flux2,flux3 = (F1_1+F2_1+F3_1) , (F1_2+F2_2+F3_2) , (F1_3+F2_3+F3_3) 
            #some checks if the code works
            if sri !=0: print('WARNING: flux calculations might be nonsense becasue there is a net salt transport through the river')
            if len(flux1)!=np.sum(flux1==None) and len(F1_1)!=np.sum(F1_1==None):
                if (100*np.nanmax(np.abs(flux1))/F1_1[np.nanargmax(np.abs(flux1))]) > 1: print('WARNING: errors in flux are relatively high: '
                                                                                           , 100*np.nanmax(np.abs(flux1))/F1_1[np.nanargmax(np.abs(flux1))]) 
            if (100*(flux2[10]+flux3[10])/(0.5*(np.abs(F1_2[10])+np.abs(F1_3[10]))))>1 : print('WARNING: errors in flux are relatively high: '
                                                                                               , 100*(flux2[10]+flux3[10])/(0.5*(np.abs(F1_2[10])+np.abs(F1_3[10])))) 
            
            #calculate parameters
            if np.max(s1)>2 and np.min(s1)>-0.5:
                X2 = (px1[-1,0]-px1[:,0][np.where(s1[:,0]>2)[0][0]])
            else: 
                X2 = None
            s_chan2 = np.mean((b2[:,np.newaxis]*s2)[:di2[-2]])/np.mean(b2[:di2[-2]])
            s_chan3 = np.mean((b3[:,np.newaxis]*s3)[:di3[-2]])/np.mean(b3[:di3[-2]])
            flux23 = flux2[10]
            width_ratio = (b02/b03)
            length_ratio = H2/H3#np.sum(Ln2[:-1])/np.sum(Ln3[:-1])
            dd_current1 = alf1*sb1_x[-2]
            dd_current2 = alf2*sb2_x[1]
            dd_current3 = alf3*sb3_x[1]
                    
            flux23_norm = flux2[10]/F1_2[10]
            out_plot[run] = [X2,s_chan2,s_chan3, flux23, flux23_norm,strat1[-1], width_ratio,length_ratio, Ut1, Q, 
                             bs2[-2],bs3[-2],H[1],H[2],np.sum(Ln2[:-1]),np.sum(Ln3[:-1]),Q1,Q2,Q3,np.mean(s1[-1]), dd_current1,dd_current2,dd_current3,Ut1,Ut2,Ut3]
            #save the horizontal flux 
            flux_save.append([[F1_1,F2_1,F3_1,flux1] , [F1_2,F2_2,F3_2,flux2] , [F1_3,F2_3,F3_3,flux3]])
            
            out_ls[run] = np.array([[np.mean(b2[:di2[-2]]),H2,Q2,Ut2,35], [np.mean(b3[:di3[-2]]),H3,Q3,Ut3,35]])

    
#remove runs which did not converge -  can also be done with masked arrays 
out_plot[np.where(np.abs(out_plot[:,1])>soc+2)[0],:5] = None

#%%load party
inp = np.loadtxt(loc+'input.txt')
zeta = inp[:,0]*1.3

fig,ax = plt.subplots(1,1,figsize=(4,4))

#a0 = ax.contourf(zeta.reshape((11,11)),out_plot[:,9].reshape((11,11)),out_plot[:,-7].reshape((11,11)),cmap='Spectral_r')#,levels=np.linspace(0,6000,13))#,levels = [-500,-400,-300,-200,-100,0,500,1000,1500] )#\
a0 = ax.contourf(zeta.reshape((21,21)),out_plot[:,9].reshape((21,21)),(out_plot[:,-8]/out_plot[:,-10]).reshape((21,21)),cmap='Purples',levels=np.linspace(1,3.5,11))#,levels = [-500,-400,-300,-200,-100,0,500,1000,1500] )#\
#a0 = ax.contourf(zeta.reshape((11,11)),out_plot[:,9].reshape((11,11)),(out_plot[:,-8]/out_plot[:,-10]).reshape((11,11)),cmap='hot_r',levels=np.linspace(-0.4,0.15,12))#,levels=np.linspace(0,6000,13))#,levels = [-500,-400,-300,-200,-100,0,500,1000,1500] )#\
#a0 = ax.contourf(zeta.reshape((11,11)),out_plot[:,9].reshape((11,11)),(out_plot[:,-1]/out_plot[:,-2]).reshape((11,11)),cmap='PuOr')#,levels=np.linspace(0,6000,13))#,levels = [-500,-400,-300,-200,-100,0,500,1000,1500] )#\
#ax.contour(zeta.reshape((21,21)),out_plot[:,9].reshape((21,21)),out_plot[:,3].reshape((21,21)),colors='black',levels=[0],linewidths=3)#,levels=np.linspace(0,6000,13))#,levels = [-500,-400,-300,-200,-100,0,500,1000,1500] )#\

ax.set_facecolor('black')

ax.set_ylabel(r'River discharge $Q$ [m$^3$s$^{-1}$]',fontsize=14)
#ax.set_xlabel('Amplitude tidal current $U_T$ [ms$^{-1}$]',fontsize=20)
ax.set_xlabel('Tidal amplitude $\zeta$ [m]',fontsize=14)
ax.tick_params(axis='both', which='major', labelsize=11) 

cax = fig.add_axes([0.1, -0.1, 0.8, 0.05])
cb = fig.colorbar(a0, cax=cax,orientation='horizontal')
#cb.set_label(label='Salinity [g/kg]',fontsize=20)
cb.set_label(label=r'$Q_{SP}/Q_{SC}$ ',fontsize=14)#label='Overspill [?]',fontsize=20)
#cb.set_label(label='Discharge through HC',fontsize=20)
#cb.set_label(label='$U_T$ ratio',fontsize=20)
cb.ax.tick_params(labelsize=11) 
cb._get_ticker_locator_formatter()[0].set_params(nbins=6)
cb.update_ticks()

plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper fork channel/figs_def/figS5.jpg',bbox_inches='tight', dpi=300)

plt.show()





