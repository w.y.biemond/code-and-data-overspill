#code to run fork channels time-indepedent, without visualisation
#effect depth variaton on river discharge distribution is built in
#channels should be of equal depth

#runcell(0, '/Users/biemo004/Documents/UU phd Saltisolutions/Scripts/Python/Start bestand.py')
#standard packages
import numpy as np                      #standard operations       
import matplotlib.pyplot as plt         #plot figures
import pandas as pd                     #import datasets
from scipy import optimize   , interpolate 
import scipy as sp          #do fits
import scipy.io                         #import matlab datasets

import matplotlib.animation as ani       #make animations
from matplotlib.collections import LineCollection
import matplotlib.patheffects as pe
#import cv2                               #import images as matrices
import time                              #measure time of operation
import os as os                          #making folders etc

#values constants
g =9.81 #gravitation
Be=7.6e-4 #isohaline contractie
Sc=2.2 #Schmidt getal
cv=7.28e-5 #empirische constante
ch=0.035 #empirische constante
ch2= 250
CD = 0.001 #wind drag coefficient
r = 1.225/1000 #density air divided by density water

def MC04_Gal_split(inp_p, inp_chn1, inp_chn2, inp_chn3, term, init):
    #meaning input:
        #inp_p: physical parameters: (Ut, soc, Q, L, bs, H)
        #inp_n: numerical paramters: (dx, N, Lsc)
        #init: initial guess for solutions, in the right format - TO INCLUDE LATER
        #term: which terms of the equation are taken into account
        
    (Ut1,Ut2,Ut3), soc, sri, Q1, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3) = inp_p #1. , 35. , 150. , 100000. , [2000.,1e9] , 10.
    Ln1, b01, bs1, dxn1 = inp_chn1
    Ln2, b02, bs2, dxn2 = inp_chn2
    Ln3, b03, bs3, dxn3 = inp_chn3
    if np.any(Ln1%dxn1!=0): print( 'WARNING: L/dx is not an integer')#check numerical parameters
    if np.any(Ln2%dxn2!=0): print( 'WARNING: L/dx is not an integer')#check numerical parameters
    if np.any(Ln3%dxn3!=0): print( 'WARNING: L/dx is not an integer')#check numerical parameters
    if len(Ln1) != len(bs1) or len(Ln1)!=len(dxn1) : print('ERROR: number of domains is not correct')
    if len(Ln2) != len(bs2) or len(Ln2)!=len(dxn2) : print('ERROR: number of domains is not correct')    
    if len(Ln3) != len(bs3) or len(Ln3)!=len(dxn3) : print('ERROR: number of domains is not correct')
    if u_w!=0: print('WARNING: this code is not suited yet to model wind, first check all terms')
    
    if init[0] == 'NOCON': return ['NOCON']
    
    dln1 = dxn1/Lsc
    dln2 = dxn2/Lsc
    dln3 = dxn3/Lsc
    nxn1 = np.array(Ln1/dxn1+1,dtype=int)
    nxn2 = np.array(Ln2/dxn2+1,dtype=int)
    nxn3 = np.array(Ln3/dxn3+1,dtype=int)
    di1 = np.zeros(len(Ln1)+1,dtype=int) #starting indices of the domains
    di2 = np.zeros(len(Ln2)+1,dtype=int) #starting indices of the domains
    di3 = np.zeros(len(Ln3)+1,dtype=int) #starting indices of the domains
    for i in range(1,len(Ln1)):   di1[i] = np.sum(nxn1[:i])
    di1[-1] = np.sum(nxn1)
    for i in range(1,len(Ln2)):   di2[i] = np.sum(nxn2[:i])
    di2[-1] = np.sum(nxn2)
    for i in range(1,len(Ln3)):   di3[i] = np.sum(nxn3[:i])
    di3[-1] = np.sum(nxn3)
    
    dl1,dl2,dl3 = np.zeros(np.sum(nxn1)),np.zeros(np.sum(nxn2)),np.zeros(np.sum(nxn3))
    dl1[0:nxn1[0]],dl2[0:nxn2[0]],dl3[0:nxn3[0]] = dln1[0],dln2[0],dln3[0]
    for i in range(1,len(nxn1)): dl1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = dln1[i]
    for i in range(1,len(nxn2)): dl2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = dln2[i]
    for i in range(1,len(nxn3)): dl3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = dln3[i]
    
    M=N+1 
    
    # make H a list from here. H can not really vary in the channel network so making it lists 
    # is a bit overkill but lets do it anyway
    H1= H[0]+np.zeros(di1[-1])
    H2= H[1]+np.zeros(di2[-1])
    H3= H[2]+np.zeros(di3[-1])
    #del H
    
    #plotting parameters
    #build parameters: convergence length
    bn1, bn2, bn3 = np.zeros(len(Ln1)), np.zeros(len(Ln2)), np.zeros(len(Ln3))
    bn1[0] = 9e99 if bs1[0] == b01 else Ln1[0]/np.log(bs1[0]/b01)
    bn2[0] = 9e99 if bs2[0] == b02 else Ln2[0]/np.log(bs2[0]/b02)
    bn3[0] = 9e99 if bs3[0] == b03 else Ln3[0]/np.log(bs3[0]/b03)
    for i in range(1,len(Ln1)): bn1[i] = 9e99 if bs1[i] == bs1[i-1] else Ln1[i]/np.log(bs1[i]/bs1[i-1])
    for i in range(1,len(Ln2)): bn2[i] = 9e99 if bs2[i] == bs2[i-1] else Ln2[i]/np.log(bs2[i]/bs2[i-1])
    for i in range(1,len(Ln3)): bn3[i] = 9e99 if bs3[i] == bs3[i-1] else Ln3[i]/np.log(bs3[i]/bs3[i-1])
    #build width
    b1, bex1 = np.zeros(np.sum(nxn1)),np.zeros(np.sum(nxn1))
    b2, bex2 = np.zeros(np.sum(nxn2)),np.zeros(np.sum(nxn2))
    b3, bex3 = np.zeros(np.sum(nxn3)),np.zeros(np.sum(nxn3))
    b1[0:nxn1[0]] = b01 * np.exp(bn1[0]**(-1)*(np.linspace(-Ln1[0],0,nxn1[0])+Ln1[0]))
    b2[0:nxn2[0]] = b02 * np.exp(bn2[0]**(-1)*(np.linspace(-Ln2[0],0,nxn2[0])+Ln2[0]))
    b3[0:nxn3[0]] = b03 * np.exp(bn3[0]**(-1)*(np.linspace(-Ln3[0],0,nxn3[0])+Ln3[0]))
    bex1[0:nxn1[0]] = [bn1[0]]*nxn1[0]
    bex2[0:nxn2[0]] = [bn2[0]]*nxn2[0]
    bex3[0:nxn3[0]] = [bn3[0]]*nxn3[0]
    
    for i in range(1,len(nxn1)): bex1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = [bn1[i]]*nxn1[i]
    for i in range(1,len(nxn2)): bex2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = [bn2[i]]*nxn2[i]
    for i in range(1,len(nxn3)): bex3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = [bn3[i]]*nxn3[i]
    for i in range(1,len(nxn1)): b1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = b1[np.sum(nxn1[:i])-1] * np.exp(bn1[i]**(-1)*(np.linspace(-Ln1[i],0,nxn1[i])+Ln1[i]))
    for i in range(1,len(nxn2)): b2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = b2[np.sum(nxn2[:i])-1] * np.exp(bn2[i]**(-1)*(np.linspace(-Ln2[i],0,nxn2[i])+Ln2[i]))
    for i in range(1,len(nxn3)): b3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = b3[np.sum(nxn3[:i])-1] * np.exp(bn3[i]**(-1)*(np.linspace(-Ln3[i],0,nxn3[i])+Ln3[i]))
    
    #plotting parameters
    #vertical
    nz=51 #vertical step - only for plot
    pz1 =np.zeros((di1[-1],nz))
    for i in range(di1[-1]): pz1[i] = np.linspace(-H1[i],0,nz)
    pz2 =np.zeros((di2[-1],nz))
    for i in range(di2[-1]): pz2[i] = np.linspace(-H2[i],0,nz)
    pz3 =np.zeros((di3[-1],nz))
    for i in range(di3[-1]): pz3[i] = np.linspace(-H3[i],0,nz)

    #horizontal
    px1 = np.zeros((np.sum(nxn1),nz))
    px1[0:nxn1[0]] = -np.linspace(np.sum(Ln1[0:]), np.sum(Ln1[0+1:]), nxn1[0]).repeat(nz).reshape((di1[1],nz)) #here i can use the di list
    for i in range(1,len(nxn1)): px1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = -np.linspace(np.sum(Ln1[i:]), np.sum(Ln1[i+1:]), nxn1[i]).repeat(nz).reshape((nxn1[i],nz))
    px2 = np.zeros((np.sum(nxn2),nz))
    px2[0:nxn2[0]] = -np.linspace(np.sum(Ln2[0:]), np.sum(Ln2[0+1:]), nxn2[0]).repeat(nz).reshape((di2[1],nz)) #here i can use the di list
    for i in range(1,len(nxn2)): px2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = -np.linspace(np.sum(Ln2[i:]), np.sum(Ln2[i+1:]), nxn2[i]).repeat(nz).reshape((nxn2[i],nz))
    px3 = np.zeros((np.sum(nxn3),nz))
    px3[0:nxn3[0]] = -np.linspace(np.sum(Ln3[0:]), np.sum(Ln3[0+1:]), nxn3[0]).repeat(nz).reshape((di3[1],nz)) #here i can use the di list
    for i in range(1,len(nxn3)): px3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = -np.linspace(np.sum(Ln3[i:]), np.sum(Ln3[i+1:]), nxn3[i]).repeat(nz).reshape((nxn3[i],nz))

    px1, px2, px3 = px1+np.sum(Ln1) , px2+np.sum(Ln1)+np.sum(Ln2) , px3+np.sum(Ln1)+np.sum(Ln3)
    px1,px2,px3=px1/1000 , px2/1000, px3/1000
 

    #build parameters - adjusted
    #sf1,sf2,sf3 = 2*cv*1,2*cv*1,2*cv*1
    #Av1, Av2, Av3 = cv*7.5+np.zeros(di1[-1]), cv*7.5+np.zeros(di2[-1]), cv*7.5+np.zeros(di3[-1])  
    Kh1, Kh2, Kh3 = ch2*Ut1**2+np.zeros(di1[-1]), ch2*Ut2**2+np.zeros(di2[-1]), ch2*Ut3**2+np.zeros(di3[-1])
    Kh2[di2[-2]:], Kh3[di3[-2]:]= ch2*Ut2**2*b2[di2[-2]:]/b2[di2[-2]], ch2*Ut3**2*b3[di3[-2]:]/b3[di3[-2]]
    
    #old
    Av1, Av2, Av3 = cv*Ut1*H1, cv*Ut2*H2, cv*Ut3*H3
    sf1, sf2, sf3 = 2*cv*Ut1, 2*cv*Ut2, 2*cv*Ut3
    #Kh1, Kh2, Kh3 = ch*Ut1*b1, ch*Ut2*b2, ch*Ut3*b3

    bH_11, bH_12, bH_13 = 1/(H1*b1), 1/(H2*b2), 1/(H3*b3)
    Kv1, Kv2, Kv3 = Av1/Sc, Av2/Sc, Av3/Sc
    alf1, alf2, alf3 = g*Be*H1**3/(48*Av1), g*Be*H2**3/(48*Av2), g*Be*H3**3/(48*Av3)
    #print(Av2[0],Av3[0],Kv2[0],Kv3[0])

    #derivative of Kh
    Kh1_x = np.concatenate([[0],(Kh1[2:]-Kh1[:-2])/(2*dl1[1:-1]*Lsc),[0]])
    Kh2_x = np.concatenate([[0],(Kh2[2:]-Kh2[:-2])/(2*dl2[1:-1]*Lsc),[0]])
    Kh3_x = np.concatenate([[0],(Kh3[2:]-Kh3[:-2])/(2*dl3[1:-1]*Lsc),[0]])
    Kh1_x[[0,-1]] , Kh1_x[di1[1:-1]] , Kh1_x[di1[1:-1]-1] = None , None , None
    Kh2_x[[0,-1]] , Kh2_x[di2[1:-1]] , Kh2_x[di2[1:-1]-1] = None , None , None
    Kh3_x[[0,-1]] , Kh3_x[di3[1:-1]] , Kh3_x[di3[1:-1]-1] = None , None , None
    
    #lists of ks and ns and pis
    kkp = np.linspace(1,N,N)*np.pi #k*pi
    nnp = np.linspace(1,N,N)*np.pi #n*pi
    pkn = np.array([kkp]*N) + np.transpose([nnp]*N) #pi*(n+k)
    pnk = np.array([nnp]*N) - np.transpose([kkp]*N) #pi*(n-k)
    np.fill_diagonal(pkn,None),np.fill_diagonal(pnk,None)

    #coefficients
    #new partial slip coefficients
    #'''
    rr1, rr2, rr3 = Av1/(sf1*H1) , Av2/(sf2*H2) ,Av3/(sf3*H3)
    g11, g12, g13 = -1 + (1.5+3*rr1) / (1+ 3 *rr1), -1 + (1.5+3*rr2) / (1+ 3 *rr2), -1 + (1.5+3*rr3) / (1+ 3 *rr3)
    g21, g22, g23 =  -3 / (2+6*rr1) , -3 / (2+6*rr2), -3 / (2+6*rr3)
    g31, g32, g33 = (1+4*rr1) / (1+3*rr1) * (9+18*rr1) - 8 - 24*rr1, (1+4*rr2) / (1+3*rr2) * (9+18*rr2) - 8 - 24*rr2, (1+4*rr3) / (1+3*rr3) * (9+18*rr3) - 8 - 24*rr3
    g41, g42, g43 = -9 * (1+4*rr1) / (1+3*rr1), -9 * (1+4*rr2) / (1+3*rr2), -9 * (1+4*rr3) / (1+3*rr3)
    g51, g52, g53 = - 8 , -8, -8
    g61, g62, g63 = 4+4*rr1 -12*(0.5+rr1)**2/(1+3*rr1), 4+4*rr2 -12*(0.5+rr2)**2/(1+3*rr2), 4+4*rr3 -12*(0.5+rr3)**2/(1+3*rr3)
    g71, g72, g73 = 4 , 4, 4
    g81, g82, g83 = (3+6*rr1) / (1+3*rr1) , (3+6*rr2) / (1+3*rr2), (3+6*rr3) / (1+3*rr3)
    #'''

    #dimensions: x,k,n
    #k=n is not possible here. 
    C1a1 = bH_11/2 
    C1a2 = bH_12/2 
    C1a3 = bH_13/2 
    
    C3a1 = 2*bH_11[:,np.newaxis]*g21[:,np.newaxis]/(kkp**2)*np.cos(kkp) #+ (r*CD*H1[:,np.newaxis]*u_w**2*d_w)/(4*Av1[:,np.newaxis]) * (g71+(2*g81[:,np.newaxis]-g71)*np.cos(kkp))/(kkp**2)
    C3b1 = alf1[:,np.newaxis]*soc/Lsc * ( 2*g41[:,np.newaxis]/kkp**2 * np.cos(kkp) - g51/kkp**4 *(6-6*np.cos(kkp) +3*kkp**2*np.cos(kkp)) )
    C3a2 = 2*bH_12[:,np.newaxis]*g22[:,np.newaxis]/(kkp**2)*np.cos(kkp) #+ (r*CD*H2[:,np.newaxis]*u_w**2*d_w)/(4*Av2[:,np.newaxis]) * (g72+(2*g82[:,np.newaxis]-g72)*np.cos(kkp))/(kkp**2)
    C3b2 = alf2[:,np.newaxis]*soc/Lsc * ( 2*g42[:,np.newaxis]/kkp**2 * np.cos(kkp) - g52/kkp**4 *(6-6*np.cos(kkp) +3*kkp**2*np.cos(kkp)) )
    C3a3 = 2*bH_13[:,np.newaxis]*g23[:,np.newaxis]/(kkp**2)*np.cos(kkp) #+ (r*CD*H3[:,np.newaxis]*u_w**2*d_w)/(4*Av3[:,np.newaxis]) * (g73+(2*g83[:,np.newaxis]-g73)*np.cos(kkp))/(kkp**2)
    C3b3 = alf3[:,np.newaxis]*soc/Lsc * ( 2*g43[:,np.newaxis]/kkp**2 * np.cos(kkp) - g53/kkp**4 *(6-6*np.cos(kkp) +3*kkp**2*np.cos(kkp)) )

    C2a1 = bH_11[:,np.newaxis] * (g11[:,np.newaxis]/2 + g21[:,np.newaxis]/6 + g21[:,np.newaxis]/(4*kkp**2)) #+ (r*CD*H1[:,np.newaxis]*u_w**2*d_w)/(4*Av1[:,np.newaxis])*(g61[:,np.newaxis]/2-g71/4+g81[:,np.newaxis]*(1/6+1/(4*kkp**2)))
    C2b1 = alf1[:,np.newaxis]*soc/Lsc * (g31[:,np.newaxis]/2 + g41[:,np.newaxis]*(1/6 + 1/(4*kkp**2)) -g51*(1/8 + 3/(8*kkp**2)) )
    C2c1 = bH_11[:,np.newaxis,np.newaxis]*g21[:,np.newaxis,np.newaxis]* ( np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2 ) 
           #+ (r*CD*H1[:,np.newaxis,np.newaxis]*u_w**2*d_w)/(4*Av1[:,np.newaxis,np.newaxis]) * (g71/2*((1-np.cos(pkn))/pkn**2 + (1-np.cos(pnk))/pnk**2) + g81[:,np.newaxis,np.newaxis]*((np.cos(pkn))/pkn**2 + (np.cos(pnk))/pnk**2)) )
    C2d1 = soc/Lsc*alf1[:,np.newaxis,np.newaxis] * (g41[:,np.newaxis,np.newaxis]*(np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2) + g51* ((3*np.cos(pkn)-3)/pkn**4 + (3*np.cos(pnk)-3)/pnk**4 - 3/2*np.cos(pkn)/pkn**2 - 3/2*np.cos(pnk)/pnk**2) )
    
    C2a2 = bH_12[:,np.newaxis] * (g12[:,np.newaxis]/2 + g22[:,np.newaxis]/6 + g22[:,np.newaxis]/(4*kkp**2)) #+ (r*CD*H2[:,np.newaxis]*u_w**2*d_w)/(4*Av2[:,np.newaxis])*(g62[:,np.newaxis]/2-g72/4+g82[:,np.newaxis]*(1/6+1/(4*kkp**2)))
    C2b2 = alf2[:,np.newaxis]*soc/Lsc * (g32[:,np.newaxis]/2 + g42[:,np.newaxis]*(1/6 + 1/(4*kkp**2)) -g52*(1/8 + 3/(8*kkp**2)) )
    C2c2 = bH_12[:,np.newaxis,np.newaxis]*g22[:,np.newaxis,np.newaxis]* ( np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2 ) 
           #+ (r*CD*H2[:,np.newaxis,np.newaxis]*u_w**2*d_w)/(4*Av2[:,np.newaxis,np.newaxis]) * (g72/2*((1-np.cos(pkn))/pkn**2 + (1-np.cos(pnk))/pnk**2) + g82[:,np.newaxis,np.newaxis]*((np.cos(pkn))/pkn**2 + (np.cos(pnk))/pnk**2)) )
    C2d2 = soc/Lsc*alf2[:,np.newaxis,np.newaxis] * (g42[:,np.newaxis,np.newaxis]*(np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2) + g52* ((3*np.cos(pkn)-3)/pkn**4 + (3*np.cos(pnk)-3)/pnk**4 - 3/2*np.cos(pkn)/pkn**2 - 3/2*np.cos(pnk)/pnk**2) )
    
    C2a3 = bH_13[:,np.newaxis] * (g13[:,np.newaxis]/2 + g23[:,np.newaxis]/6 + g23[:,np.newaxis]/(4*kkp**2)) #+ (r*CD*H3[:,np.newaxis]*u_w**2*d_w)/(4*Av3[:,np.newaxis])*(g63[:,np.newaxis]/2-g73/4+g83[:,np.newaxis]*(1/6+1/(4*kkp**2)))
    C2b3 = alf3[:,np.newaxis]*soc/Lsc * (g33[:,np.newaxis]/2 + g43[:,np.newaxis]*(1/6 + 1/(4*kkp**2)) -g53*(1/8 + 3/(8*kkp**2)) )
    C2c3 = bH_13[:,np.newaxis,np.newaxis]*g23[:,np.newaxis,np.newaxis]* ( np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2 ) 
           #+ (r*CD*H3[:,np.newaxis,np.newaxis]*u_w**2*d_w)/(4*Av3[:,np.newaxis,np.newaxis]) * (g73/2*((1-np.cos(pkn))/pkn**2 + (1-np.cos(pnk))/pnk**2) + g83[:,np.newaxis,np.newaxis]*((np.cos(pkn))/pkn**2 + (np.cos(pnk))/pnk**2)) )
    C2d3 = soc/Lsc*alf3[:,np.newaxis,np.newaxis] * (g43[:,np.newaxis,np.newaxis]*(np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2) + g53* ((3*np.cos(pkn)-3)/pkn**4 + (3*np.cos(pnk)-3)/pnk**4 - 3/2*np.cos(pkn)/pkn**2 - 3/2*np.cos(pnk)/pnk**2) )

    C5a1 = alf1[:,np.newaxis]*soc/Lsc *kkp* ( -9*g51+6*g41[:,np.newaxis]+kkp**2*(-12*g31[:,np.newaxis]-4*g41[:,np.newaxis]+3*g51) ) / (48*kkp**3)
    C5b1 = alf1[:,np.newaxis]*soc*bex1[:,np.newaxis]**(-1)*kkp * ( -9*g51+6*g41[:,np.newaxis]+kkp**2*(-12*g31[:,np.newaxis]-4*g41[:,np.newaxis]+3*g51) ) / (48*kkp**3)    
    C5c1 = alf1[:,np.newaxis,np.newaxis]*soc/Lsc*nnp* ( (3*g51*np.cos(pkn)-3*g51)/pkn**5 + np.cos(pkn)/pkn**3 * (g41[:,np.newaxis,np.newaxis]-1.5*g51) + np.cos(pkn)/pkn * (g51/8 - g41[:,np.newaxis,np.newaxis]/6 - g31[:,np.newaxis,np.newaxis]/2) 
                            +(3*g51*np.cos(pnk)-3*g51)/pnk**5 + np.cos(pnk)/pnk**3 * (g41[:,np.newaxis,np.newaxis]-1.5*g51) + np.cos(pnk)/pnk * (g51/8 - g41[:,np.newaxis,np.newaxis]/6 - g31[:,np.newaxis,np.newaxis]/2))
    C5d1 = alf1[:,np.newaxis,np.newaxis]*soc*nnp*bex1[:,np.newaxis,np.newaxis]**(-1)*( (3*g51*np.cos(pkn)-3*g51)/pkn**5 + np.cos(pkn)/pkn**3 * (g41[:,np.newaxis,np.newaxis]-1.5*g51) 
                                + np.cos(pkn)/pkn * (g51/8 - g41[:,np.newaxis,np.newaxis]/6 - g31[:,np.newaxis,np.newaxis]/2)
                                +(3*g51*np.cos(pnk)-3*g51)/pnk**5 + np.cos(pnk)/pnk**3 * (g41[:,np.newaxis,np.newaxis]-1.5*g51) + np.cos(pnk)/pnk * (g51/8 - g41[:,np.newaxis,np.newaxis]/6 - g31[:,np.newaxis,np.newaxis]/2))
    C5e1 = Lsc*bex1[:,np.newaxis]**(-1) * (r*CD*H1[:,np.newaxis]*u_w**2*d_w)/(4*Av1[:,np.newaxis]) * (-g61[:,np.newaxis]/4 + g71/8 + g81[:,np.newaxis]*(3-2*kkp**2)/(24*kkp**2))
    C5f1 = (Lsc*bex1[:,np.newaxis,np.newaxis]**(-1) * (r*CD*H1[:,np.newaxis,np.newaxis]*u_w**2*d_w)/(4*Av1[:,np.newaxis,np.newaxis]) * nnp
              * ( ((g81[:,np.newaxis,np.newaxis]-g71/2)*np.cos(pkn)-g71/2)/(pkn**3) + ((g81[:,np.newaxis,np.newaxis]-g71/2)*np.cos(pnk)-g71/2)/(pnk**3) 
                 +(-g61[:,np.newaxis,np.newaxis]/2+g71/4-g81[:,np.newaxis,np.newaxis]/6)*np.cos(pkn)/pkn + (-g61[:,np.newaxis,np.newaxis]/2+g71/4-g81[:,np.newaxis,np.newaxis]/6)*np.cos(pnk)/pnk ) )
    
    C5a2 = alf2[:,np.newaxis]*soc/Lsc *kkp* ( -9*g52+6*g42[:,np.newaxis]+kkp**2*(-12*g32[:,np.newaxis]-4*g42[:,np.newaxis]+3*g52) ) / (48*kkp**3)
    C5b2 = alf2[:,np.newaxis]*soc*bex2[:,np.newaxis]**(-1)*kkp * ( -9*g52+6*g42[:,np.newaxis]+kkp**2*(-12*g32[:,np.newaxis]-4*g42[:,np.newaxis]+3*g52) ) / (48*kkp**3)    
    C5c2 = alf2[:,np.newaxis,np.newaxis]*soc/Lsc*nnp* ( (3*g52*np.cos(pkn)-3*g52)/pkn**5 + np.cos(pkn)/pkn**3 * (g42[:,np.newaxis,np.newaxis]-1.5*g52) + np.cos(pkn)/pkn * (g52/8 - g42[:,np.newaxis,np.newaxis]/6 - g32[:,np.newaxis,np.newaxis]/2) 
                            +(3*g52*np.cos(pnk)-3*g52)/pnk**5 + np.cos(pnk)/pnk**3 * (g42[:,np.newaxis,np.newaxis]-1.5*g52) + np.cos(pnk)/pnk * (g52/8 - g42[:,np.newaxis,np.newaxis]/6 - g32[:,np.newaxis,np.newaxis]/2))
    C5d2 = alf2[:,np.newaxis,np.newaxis]*soc*nnp*bex2[:,np.newaxis,np.newaxis]**(-1)*( (3*g52*np.cos(pkn)-3*g52)/pkn**5 + np.cos(pkn)/pkn**3 * (g42[:,np.newaxis,np.newaxis]-1.5*g52) 
                                + np.cos(pkn)/pkn * (g52/8 - g42[:,np.newaxis,np.newaxis]/6 - g32[:,np.newaxis,np.newaxis]/2)
                                +(3*g52*np.cos(pnk)-3*g52)/pnk**5 + np.cos(pnk)/pnk**3 * (g42[:,np.newaxis,np.newaxis]-1.5*g52) + np.cos(pnk)/pnk * (g52/8 - g42[:,np.newaxis,np.newaxis]/6 - g32[:,np.newaxis,np.newaxis]/2))
    C5e2 = Lsc*bex2[:,np.newaxis]**(-1) * (r*CD*H2[:,np.newaxis]*u_w**2*d_w)/(4*Av2[:,np.newaxis]) * (-g62[:,np.newaxis]/4 + g72/8 + g82[:,np.newaxis]*(3-2*kkp**2)/(24*kkp**2))
    C5f2 = (Lsc*bex2[:,np.newaxis,np.newaxis]**(-1) * (r*CD*H2[:,np.newaxis,np.newaxis]*u_w**2*d_w)/(4*Av2[:,np.newaxis,np.newaxis]) * nnp
              * ( ((g82[:,np.newaxis,np.newaxis]-g72/2)*np.cos(pkn)-g72/2)/(pkn**3) + ((g82[:,np.newaxis,np.newaxis]-g72/2)*np.cos(pnk)-g72/2)/(pnk**3) 
                 +(-g62[:,np.newaxis,np.newaxis]/2+g72/4-g82[:,np.newaxis,np.newaxis]/6)*np.cos(pkn)/pkn + (-g62[:,np.newaxis,np.newaxis]/2+g72/4-g82[:,np.newaxis,np.newaxis]/6)*np.cos(pnk)/pnk ) )
    
    C5a3 = alf3[:,np.newaxis]*soc/Lsc *kkp* ( -9*g53+6*g43[:,np.newaxis]+kkp**2*(-12*g33[:,np.newaxis]-4*g43[:,np.newaxis]+3*g53) ) / (48*kkp**3)
    C5b3 = alf3[:,np.newaxis]*soc*bex3[:,np.newaxis]**(-1)*kkp * ( -9*g53+6*g43[:,np.newaxis]+kkp**2*(-12*g33[:,np.newaxis]-4*g43[:,np.newaxis]+3*g53) ) / (48*kkp**3)    
    C5c3 = alf3[:,np.newaxis,np.newaxis]*soc/Lsc*nnp* ( (3*g53*np.cos(pkn)-3*g53)/pkn**5 + np.cos(pkn)/pkn**3 * (g43[:,np.newaxis,np.newaxis]-1.5*g53) + np.cos(pkn)/pkn * (g53/8 - g43[:,np.newaxis,np.newaxis]/6 - g33[:,np.newaxis,np.newaxis]/2) 
                            +(3*g53*np.cos(pnk)-3*g53)/pnk**5 + np.cos(pnk)/pnk**3 * (g43[:,np.newaxis,np.newaxis]-1.5*g53) + np.cos(pnk)/pnk * (g53/8 - g43[:,np.newaxis,np.newaxis]/6 - g33[:,np.newaxis,np.newaxis]/2))
    C5d3 = alf3[:,np.newaxis,np.newaxis]*soc*nnp*bex3[:,np.newaxis,np.newaxis]**(-1)*( (3*g53*np.cos(pkn)-3*g53)/pkn**5 + np.cos(pkn)/pkn**3 * (g43[:,np.newaxis,np.newaxis]-1.5*g53) 
                                + np.cos(pkn)/pkn * (g53/8 - g43[:,np.newaxis,np.newaxis]/6 - g33[:,np.newaxis,np.newaxis]/2)
                                +(3*g53*np.cos(pnk)-3*g53)/pnk**5 + np.cos(pnk)/pnk**3 * (g43[:,np.newaxis,np.newaxis]-1.5*g53) + np.cos(pnk)/pnk * (g53/8 - g43[:,np.newaxis,np.newaxis]/6 - g33[:,np.newaxis,np.newaxis]/2))
    C5e3 = Lsc*bex3[:,np.newaxis]**(-1) * (r*CD*H3[:,np.newaxis]*u_w**2*d_w)/(4*Av3[:,np.newaxis]) * (-g63[:,np.newaxis]/4 + g73/8 + g83[:,np.newaxis]*(3-2*kkp**2)/(24*kkp**2))
    C5f3 = (Lsc*bex3[:,np.newaxis,np.newaxis]**(-1) * (r*CD*H3[:,np.newaxis,np.newaxis]*u_w**2*d_w)/(4*Av3[:,np.newaxis,np.newaxis]) * nnp
              * ( ((g83[:,np.newaxis,np.newaxis]-g73/2)*np.cos(pkn)-g73/2)/(pkn**3) + ((g83[:,np.newaxis,np.newaxis]-g73/2)*np.cos(pnk)-g73/2)/(pnk**3) 
                 +(-g63[:,np.newaxis,np.newaxis]/2+g73/4-g83[:,np.newaxis,np.newaxis]/6)*np.cos(pkn)/pkn + (-g63[:,np.newaxis,np.newaxis]/2+g73/4-g83[:,np.newaxis,np.newaxis]/6)*np.cos(pnk)/pnk ) )

    C6a1 = Lsc*Kv1[:,np.newaxis]*kkp**2/(2*H1[:,np.newaxis]**2)
    C6a2 = Lsc*Kv2[:,np.newaxis]*kkp**2/(2*H2[:,np.newaxis]**2)
    C6a3 = Lsc*Kv3[:,np.newaxis]*kkp**2/(2*H3[:,np.newaxis]**2)
    
    C7a1 = -bex1**-1 * Kh1 / 2 - Kh1_x / 2
    C7b1 = -Kh1/(2*Lsc)
    C7a2 = -bex2**-1 * Kh2 / 2 - Kh2_x / 2
    C7b2 = -Kh2/(2*Lsc)
    C7a3 = -bex3**-1 * Kh3 / 2 - Kh3_x / 2
    C7b3 = -Kh3/(2*Lsc)

    C10a1 = -bex1**(-1)*Kh1 - Kh1_x
    C10b1 = -Kh1/Lsc
    C10c1 = bex1[:,np.newaxis]**(-1)*alf1[:,np.newaxis]*soc * ( 2*g41[:,np.newaxis]/nnp**2*np.cos(nnp) - g51/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
    C10d1 = alf1[:,np.newaxis]*soc/Lsc * ( 2*g41[:,np.newaxis]/nnp**2*np.cos(nnp) - g51/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
    C10e1 = (2*bH_11[:,np.newaxis]*g21[:,np.newaxis]) / nnp**2 * np.cos(nnp) #+ (r*CD*H1[:,np.newaxis]*u_w**2*d_w)/(4*Av1[:,np.newaxis]) * (g71+ (2*g81[:,np.newaxis]-g71)*np.cos(nnp))/(nnp**2)
    C10f1 = alf1[:,np.newaxis]*soc/Lsc * ( 2*g41[:,np.newaxis]/nnp**2*np.cos(nnp) - g51/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
    C10g1 = Lsc*bex1[:,np.newaxis]**(-1)*(r*CD*H1[:,np.newaxis]*u_w**2*d_w)/(4*Av1[:,np.newaxis]) * (g71+(2*g81[:,np.newaxis]-g71)*np.cos(nnp))/(nnp**2)
    C10h1 = bH_11
    
    C10a2 = -bex2**(-1)*Kh2 - Kh2_x
    C10b2 = -Kh2/Lsc
    C10c2 = bex2[:,np.newaxis]**(-1)*alf2[:,np.newaxis]*soc * ( 2*g42[:,np.newaxis]/nnp**2*np.cos(nnp) - g52/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
    C10d2 = alf2[:,np.newaxis]*soc/Lsc * ( 2*g42[:,np.newaxis]/nnp**2*np.cos(nnp) - g52/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
    C10e2 = (2*bH_12[:,np.newaxis]*g22[:,np.newaxis]) / nnp**2 * np.cos(nnp) #+ (r*CD*H2[:,np.newaxis]*u_w**2*d_w)/(4*Av2[:,np.newaxis]) * (g72+ (2*g82[:,np.newaxis]-g72)*np.cos(nnp))/(nnp**2)
    C10f2 = alf2[:,np.newaxis]*soc/Lsc * ( 2*g42[:,np.newaxis]/nnp**2*np.cos(nnp) - g52/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
    C10g2 = Lsc*bex2[:,np.newaxis]**(-1)*(r*CD*H2[:,np.newaxis]*u_w**2*d_w)/(4*Av2[:,np.newaxis]) * (g72+(2*g82[:,np.newaxis]-g72)*np.cos(nnp))/(nnp**2)
    C10h2 = bH_12
    
    C10a3 = -bex3**(-1)*Kh3 - Kh3_x
    C10b3 = -Kh3/Lsc
    C10c3 = bex3[:,np.newaxis]**(-1)*alf3[:,np.newaxis]*soc * ( 2*g43[:,np.newaxis]/nnp**2*np.cos(nnp) - g53/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
    C10d3 = alf3[:,np.newaxis]*soc/Lsc * ( 2*g43[:,np.newaxis]/nnp**2*np.cos(nnp) - g53/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
    C10e3 = (2*bH_13[:,np.newaxis]*g23[:,np.newaxis]) / nnp**2 * np.cos(nnp) #+ (r*CD*H3[:,np.newaxis]*u_w**2*d_w)/(4*Av3[:,np.newaxis]) * (g73+ (2*g83[:,np.newaxis]-g73)*np.cos(nnp))/(nnp**2)
    C10f3 = alf3[:,np.newaxis]*soc/Lsc * ( 2*g43[:,np.newaxis]/nnp**2*np.cos(nnp) - g53/nnp**4 * (6-6*np.cos(nnp)+3*nnp**2*np.cos(nnp)) )
    C10g3 = Lsc*bex3[:,np.newaxis]**(-1)*(r*CD*H3[:,np.newaxis]*u_w**2*d_w)/(4*Av3[:,np.newaxis]) * (g73+(2*g83[:,np.newaxis]-g73)*np.cos(nnp))/(nnp**2)
    C10h3 =  bH_13
    
    #for 'Huibs conditions'
    C12a1 = H1[-1]*b1[-1]*0.5*Kh1[-1]
    C12b1 = H1[-1]*b1[-1]*alf1[-1]*soc*kkp* ( g51*(kkp**2-3)/(16*kkp**3) + g41[-1]*(3-2*kkp**2)/(24*kkp**3) - g31[-1]/(4*kkp) )
    C12c1 = H1[-1]*b1[-1]*alf1[-1]*soc*nnp* ( g51/8*(np.cos(pkn)/pkn**5*(24-12*pkn**2+pkn**4) - 24/pkn**5 + np.cos(pnk)/pnk**5*(24-12*pnk**2+pnk**4) - 24/pnk**5 )
                                      +g41[-1]/6*((6-pkn**2)*np.cos(pkn)/pkn**3  + (6-pnk**2)*np.cos(pnk)/pnk**3 ) + g31[-1]/2*(np.cos(pkn)/pkn + np.cos(pnk)/pnk) )
    C12d1 = H1[-1]*b1[-1]*Lsc*bH_11[-1]*(0.5+0.5*g11[-1]+g21[-1]*(1/6+1/(4*kkp**2)))
    C12e1 = H1[-1]*b1[-1]*Lsc*bH_11[-1] * (np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2)
    C12f1 =  H1[-1]*b1[-1]*Lsc*bH_11[-1] * 2*g21[-1]*np.cos(kkp)/kkp**2 
    C12g1 = H1[-1]*b1[-1]*soc*alf1[-1] * (g41[-1]*2*np.cos(kkp)/kkp**2 + g51*((6*np.cos(kkp)-1)/kkp**4 - 3*np.cos(kkp)/kkp**2))

    C12a2 = H2[0]*b2[0]*0.5*Kh2[0]
    C12b2 = H2[0]*b2[0]*alf2[0]*soc*kkp* ( g52*(kkp**2-3)/(16*kkp**3) + g42[0]*(3-2*kkp**2)/(24*kkp**3) - g32[0]/(4*kkp) )
    C12c2 = H2[0]*b2[0]*alf2[0]*soc*nnp* ( g52/8*(np.cos(pkn)/pkn**5*(24-12*pkn**2+pkn**4) - 24/pkn**5 + np.cos(pnk)/pnk**5*(24-12*pnk**2+pnk**4) - 24/pnk**5 )
                                      +g42[0]/6*((6-pkn**2)*np.cos(pkn)/pkn**3  + (6-pnk**2)*np.cos(pnk)/pnk**3 ) + g32[0]/2*(np.cos(pkn)/pkn + np.cos(pnk)/pnk) )
    C12d2 =  H2[0]*b2[0]*Lsc*bH_12[0]*(0.5+0.5*g12[0]+g22[0]*(1/6+1/(4*kkp**2)))
    C12e2 = H2[0]*b2[0]*Lsc*bH_12[0] * (np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2)
    C12f2 =  H2[0]*b2[0]*Lsc*bH_12[0] * 2*g22[0]*np.cos(kkp)/kkp**2 
    C12g2 = H2[0]*b2[0]*soc*alf2[0] * (g42[0]*2*np.cos(kkp)/kkp**2 + g52*((6*np.cos(kkp)-1)/kkp**4 - 3*np.cos(kkp)/kkp**2))

    C12a3 = H3[0]*b3[0]*0.5*Kh3[0]
    C12b3 = H3[0]*b3[0]*alf3[0]*soc*kkp* ( g53*(kkp**2-3)/(16*kkp**3) + g43[0]*(3-2*kkp**2)/(24*kkp**3) - g33[0]/(4*kkp) )
    C12c3 = H3[0]*b3[0]*alf3[0]*soc*nnp* ( g53/8*(np.cos(pkn)/pkn**5*(24-12*pkn**2+pkn**4) - 24/pkn**5 + np.cos(pnk)/pnk**5*(24-12*pnk**2+pnk**4) - 24/pnk**5 )
                                      +g43[0]/6*((6-pkn**2)*np.cos(pkn)/pkn**3  + (6-pnk**2)*np.cos(pnk)/pnk**3 ) + g33[0]/2*(np.cos(pkn)/pkn + np.cos(pnk)/pnk) )
    
    C12d3 =  H3[0]*b3[0]*Lsc*bH_13[0]*(0.5+0.5*g13[0]+g23[0]*(1/6+1/(4*kkp**2)))
    C12e3 = H3[0]*b3[0]*Lsc*bH_13[0] * (np.cos(pkn)/pkn**2 + np.cos(pnk)/pnk**2)
    C12f3 =  H3[0]*b3[0]*Lsc*bH_13[0] * 2*g23[0]*np.cos(kkp)/kkp**2 
    C12g3 = H3[0]*b3[0]*soc*alf3[0] * (g43[0]*2*np.cos(kkp)/kkp**2 + g53*((6*np.cos(kkp)-1)/kkp**4 - 3*np.cos(kkp)/kkp**2))
    
    C12e1[np.where(np.isnan(C12c1))] = 0  
    C12e2[np.where(np.isnan(C12c2))] = 0  
    C12e3[np.where(np.isnan(C12c3))] = 0  
    
    
    #for depth-averaged salt flux
    C13a1 = H1[-1]*b1[-1]*bH_11[-1]
    C13b1 = H1[-1]*b1[-1]*2*bH_11[-1]*g21[-1]*np.cos(nnp)/nnp**2
    C13c1 = H1[-1]*b1[-1]*alf1[-1]*soc/Lsc * (2*g41[-1]*np.cos(nnp)/nnp**2 + g51*(6*np.cos(nnp)-6)/nnp**4 -g51*3*np.cos(nnp)/nnp**2 ) 
    C13d1 = H1[-1]*b1[-1]*- Kh1[-1]/Lsc
    
    C13a2 = H2[0]*b2[0]*bH_12[0]
    C13b2 = H2[0]*b2[0]*2*bH_12[0]*g22[0]*np.cos(nnp)/nnp**2
    C13c2 = H2[0]*b2[0]*alf2[0]*soc/Lsc * (2*g42[0]*np.cos(nnp)/nnp**2 + g52*(6*np.cos(nnp)-6)/nnp**4 -g52*3*np.cos(nnp)/nnp**2 ) 
    C13d2 = H2[0]*b2[0]*- Kh2[0]/Lsc

    C13a3 = H3[0]*b3[0]*bH_13[0]
    C13b3 = H3[0]*b3[0]*2*bH_13[0]*g23[0]*np.cos(nnp)/nnp**2
    C13c3 = H3[0]*b3[0]*alf3[0]*soc/Lsc * (2*g43[0]*np.cos(nnp)/nnp**2 + g53*(6*np.cos(nnp)-6)/nnp**4 -g53*3*np.cos(nnp)/nnp**2 ) 
    C13d3 = H3[0]*b3[0]*- Kh3[0]/Lsc
    
    #for river discharge distribution
    #first calculate the integrated width
    vol2,vol3 = np.zeros(len(Ln2)) , np.zeros(len(Ln3))
    vol2[0] = bex2[di2[0]]/b02*(1-np.exp(-Ln2[0]/bex2[di2[0]])) if bs2[0] !=b02 else Ln2[0]/b02
    for i in range(1,len(Ln2)): 
        vol2[i] = bex2[di2[i]]/bs2[i-1]*(1-np.exp(-Ln2[i]/bex2[di2[i]])) if bs2[i] != bs2[i-1] else Ln2[i]/bs2[i]
    vol3[0] = bex3[di3[0]]/b03*(1-np.exp(-Ln3[0]/bex3[di3[0]])) if bs3[0] !=b03 else Ln3[0]/b03
    for i in range(1,len(Ln3)): 
        vol3[i] = bex3[di3[i]]/bs3[i-1]*(1-np.exp(-Ln3[i]/bex3[di3[i]])) if bs3[i] != bs3[i-1] else Ln3[i]/bs3[i]
    vol2,vol3 = np.sum(vol2) , np.sum(vol3)
    #factors
    C14a2 = (1/8+rr2[0]/2)/(1/3+rr2[0]) * Be*H2[0]*soc*0 +n_sea2
    C14b2 =-(1/8+rr2[0]/2)/(1/3+rr2[0]) * Be*H2[0]*soc*0 
    C14c2 = 3/(1+3*rr2[0]) * (g*H2[0]**3)**-1 * vol2 * Av2[0]
    
    C14a3 = (1/8+rr3[0]/2)/(1/3+rr3[0]) * Be*H3[0]*soc*0 +n_sea3
    C14b3 =-(1/8+rr3[0]/2)/(1/3+rr3[0]) * Be*H3[0]*soc*0
    C14c3 = 3/(1+3*rr3[0]) * (g*H3[0]**3)**-1 * vol3 * Av3[0]

    
    #set the contribution to the sum to zero of k=n (the diagonal)
    C2c1[np.where(np.isnan(C2c1))] = 0 
    C2c2[np.where(np.isnan(C2c2))] = 0 
    C2c3[np.where(np.isnan(C2c3))] = 0 
    C2d1[np.where(np.isnan(C2d1))] = 0
    C2d2[np.where(np.isnan(C2d2))] = 0            
    C2d3[np.where(np.isnan(C2d3))] = 0

    C5c1[np.where(np.isnan(C5c1))] = 0
    C5c2[np.where(np.isnan(C5c2))] = 0
    C5c3[np.where(np.isnan(C5c3))] = 0
    C5d1[np.where(np.isnan(C5d1))] = 0
    C5d2[np.where(np.isnan(C5d2))] = 0
    C5d3[np.where(np.isnan(C5d3))] = 0
    C5f1[np.where(np.isnan(C5f1))] = 0
    C5f2[np.where(np.isnan(C5f2))] = 0
    C5f3[np.where(np.isnan(C5f3))] = 0

    C12c1[np.where(np.isnan(C12c1))] = 0  
    C12c2[np.where(np.isnan(C12c2))] = 0  
    C12c3[np.where(np.isnan(C12c3))] = 0  
    
    # =============================================================================
    #     #solution formulae
    # =============================================================================

    #inner boundaries in domains - points and derivatives are equal
    sol_in2 =  {'se': lambda S_1, S0: S_1 - S0,
                'dse': lambda S_3,S_2,S_1,S0,S1,S2,dx1, dx2: (3*S_1-4*S_2+S_3)/(2*dx1) - (-3*S0+4*S1-S2)/(2*dx2)
               }
    
    #branching point 
    
    #end of domain 1##################################################
    #depth-averaged: total flux is equal 
    sol_branch_sb1 = lambda S01,S_11,S_21,S_31, S02,S12,S22,S32, S03,S13,S23,S33, QQ1, QQ2, QQ3: C13a1*QQ1*S01[0] + np.sum(S01[1:]*(C13b1*QQ1+C13c1*(S_21[0]-4*S_11[0]+3*S01[0])/(2*dl1[-1]) )) + C13d1*(S_21[0]-4*S_11[0]+3*S01[0])/(2*dl1[-1]) \
        - ( C13a2*QQ2*S02[0] + np.sum(S02[1:]*(C13b2*QQ2+C13c2*(-3*S02[0]+4*S12[0]-S22[0])/(2*dl2[0]) )) + C13d2*(-3*S02[0]+4*S12[0]-S22[0])/(2*dl2[0]) ) \
        - ( C13a3*QQ3*S03[0] + np.sum(S03[1:]*(C13b3*QQ3+C13c3*(-3*S03[0]+4*S13[0]-S23[0])/(2*dl3[0]) )) + C13d3*(-3*S03[0]+4*S13[0]-S23[0])/(2*dl3[0]) )

    #Huibs idea, all three domains                                                     
    sol_branch_sn1 = lambda S01,S_11,S_21,S_31, S02,S12,S22,S32, S03,S13,S23,S33, QQ1,QQ2,QQ3, z: (
         C12a1*(S_21[z+1]-4*S_11[z+1]+3*S01[z+1])/(2*dl1[-1]) - C12a2*(-3*S02[z+1]+4*S12[z+1]-S22[z+1])/(2*dl2[0]) - C12a3*(-3*S03[z+1]+4*S13[z+1]-S23[z+1])/(2*dl3[0]) 
         + C12b1[z]*S01[z+1]*(S_21[0]-4*S_11[0]+3*S01[0])/(2*dl1[-1]) - C12b2[z]*S02[z+1]*(-3*S02[0]+4*S12[0]-S22[0])/(2*dl2[0]) - C12b3[z]*S03[z+1]*(-3*S03[0]+4*S13[0]-S23[0])/(2*dl3[0]) 
         + np.sum([C12c1[z,n]*S01[n+1] for n in range(N)])*(S_21[0]-4*S_11[0]+3*S01[0])/(2*dl1[-1]) - np.sum([C12c2[z,n]*S02[n+1] for n in range(N)])*(-3*S02[0]+4*S12[0]-S22[0])/(2*dl2[0]) 
            - np.sum([C12c3[z,n]*S03[n+1] for n in range(N)])*(-3*S03[0]+4*S13[0]-S23[0])/(2*dl3[0]) 
         + C12d1[z]*S01[z+1]*QQ1 - C12d2[z]*S02[z+1]*QQ2 - C12d3[z]*S03[z+1]*QQ3
         + np.sum([C12e1[z,n]*S01[n+1] for n in range(N)])*QQ1 - np.sum([C12e2[z,n]*S02[n+1] for n in range(N)])*QQ2 - np.sum([C12e3[z,n]*S03[n+1] for n in range(N)])*QQ3+S01[0]
         + C12f1[z] * QQ1 * S01[0]  - C12f2[z] * QQ2 * S02[0]  - C12f3[z] * QQ3 * S03[0]
         + C12g1[z] * S01[0] * (S_21[0]-4*S_11[0]+3*S01[0])/(2*dl1[-1]) - C12g2[z] * S02[0] * (-3*S02[0]+4*S12[0]-S22[0])/(2*dl2[0]) - C12g3[z] * S03[0] * (-3*S03[0]+4*S13[0]-S23[0])/(2*dl3[0]) 
            )
    
    #begin of domain 2##################################################
    #points equal
    sol_branch_sb2 = lambda S01,S_11,S_21,S_31, S02,S12,S22,S32, S03,S13,S23,S33: S02[0] - S01[0]
    sol_branch_sn2 = lambda S01,S_11,S_21,S_31, S02,S12,S22,S32, S03,S13,S23,S33, z: S02[z+1] - S01[z+1]

    #begin of domain 3 ##################################################
    #popints equal
    sol_branch_sb3 = lambda S01,S_11,S_21,S_31, S02,S12,S22,S32, S03,S13,S23,S33: S03[0] - S01[0]
    sol_branch_sn3 = lambda S01,S_11,S_21,S_31, S02,S12,S22,S32, S03,S13,S23,S33, z: S03[z+1] - S01[z+1]
    
    
    # =============================================================================
    #     #build indices for jacobian and solution vector
    # =============================================================================
    #domain 1
    di_l1 = np.zeros((len(di1)-2)*2)
    for i in range(1,len(di1)-1):
        di_l1[i*2-2] = di1[i]-1
        di_l1[i*2-1] = di1[i]
    di_l1 = np.array(di_l1, dtype=int)
    
    #build the lists of indices for the different places in the Jacobian
    #to not destroy the multiplication, we should add the length of the lists afterwards
    x1 = np.delete(np.arange(di1[-1]),di_l1)[1:-1] # x coordinates for the points which are not on a aboundary
    xr1 = x1.repeat(N) # x for N values, mostly i in old code
    xr_m1 = xr1*M #M*i in old coe
    xrm_m1 = (xr1-1)*M
    xrp_m1 = (xr1+1)*M
    xr_mj1 = xr_m1+np.tile(np.arange(1,M),di1[-1]-2-(len(di1[1:-1])*2))
    xrp_mj1 = xrp_m1+np.tile(np.arange(1,M),di1[-1]-2-(len(di1[1:-1])*2))
    xrm_mj1 = xrm_m1+np.tile(np.arange(1,M),di1[-1]-2-(len(di1[1:-1])*2))
    jl1 = np.tile(np.arange(N),di1[-1]-2-(len(di1[1:-1])*2))
    
    #for the k things we need to repeat some arrays
    xr_mjr1=np.repeat(xr_m1,N)+np.tile(np.arange(1,M),(di1[-1]-2-(len(di1[1:-1])*2))).repeat(N)
    xrr1 = xr1.repeat(N)
    jlr1 = jl1.repeat(N)
    xr_mr1 = xr_m1.repeat(N)
    xrp_mr1 = xrp_m1.repeat(N)
    xrm_mr1 = xrm_m1.repeat(N)
    
    kl1 = np.tile(jl1,N)
    xr_mk1=np.repeat(xr_m1,N)+np.tile(np.tile(np.arange(1,M),(di1[-1]-2-(len(di1[1:-1])*2))),N)
    xrp_mk1=np.repeat(xrp_m1,N)+np.tile(np.tile(np.arange(1,M),(di1[-1]-2-(len(di1[1:-1])*2))),N)
    xrm_mk1=np.repeat(xrm_m1,N)+np.tile(np.tile(np.arange(1,M),(di1[-1]-2-(len(di1[1:-1])*2))),N)

    x_m1 = x1*M
    xp_m1 = (x1+1)*M
    xm_m1 = (x1-1)*M
    
    #for the boundaries 
    bnd1 = (di1[np.arange(1,len(nxn1))]*M+np.arange(M)[:,np.newaxis]).flatten()
    #bnd_dl1 = np.repeat(np.arange(1,len(nxn1)),M) #not used yet
    
    #domain 2
    di_l2 = np.zeros((len(di2)-2)*2)
    for i in range(1,len(di2)-1):
        di_l2[i*2-2] = di2[i]-1
        di_l2[i*2-1] = di2[i]
    di_l2 = np.array(di_l2, dtype=int)
    
    #build the lists of indices for the different places in the Jacobian
    x2 = np.delete(np.arange(di2[-1]),di_l2)[1:-1] # x coordinates for the points which are not on a aboundary
    xr2 = x2.repeat(N) # x for N values, mostly i in old code
    xr_m2 = xr2*M #M*i in old coe
    xrm_m2 = (xr2-1)*M
    xrp_m2 = (xr2+1)*M
    xr_mj2 = xr_m2+np.tile(np.arange(1,M),di2[-1]-2-(len(di2[1:-1])*2))
    xrp_mj2 = xrp_m2+np.tile(np.arange(1,M),di2[-1]-2-(len(di2[1:-1])*2))
    xrm_mj2 = xrm_m2+np.tile(np.arange(1,M),di2[-1]-2-(len(di2[1:-1])*2))
    jl2 = np.tile(np.arange(N),di2[-1]-2-(len(di2[1:-1])*2))
    
    #for the k things we need to repeat some arrays
    xr_mjr2=np.repeat(xr_m2,N)+np.tile(np.arange(1,M),(di2[-1]-2-(len(di2[1:-1])*2))).repeat(N)
    xrr2 = xr2.repeat(N)
    jlr2 = jl2.repeat(N)
    xr_mr2 = xr_m2.repeat(N)
    xrp_mr2 = xrp_m2.repeat(N)
    xrm_mr2 = xrm_m2.repeat(N)
    
    kl2 = np.tile(jl2,N)
    xr_mk2=np.repeat(xr_m2,N)+np.tile(np.tile(np.arange(1,M),(di2[-1]-2-(len(di2[1:-1])*2))),N)
    xrp_mk2=np.repeat(xrp_m2,N)+np.tile(np.tile(np.arange(1,M),(di2[-1]-2-(len(di2[1:-1])*2))),N)
    xrm_mk2=np.repeat(xrm_m2,N)+np.tile(np.tile(np.arange(1,M),(di2[-1]-2-(len(di2[1:-1])*2))),N)

    x_m2 = x2*M
    xp_m2 = (x2+1)*M
    xm_m2 = (x2-1)*M
    
    #for the boundaries 
    bnd2 = di1[-1]*M+(di2[np.arange(1,len(nxn2))]*M+np.arange(M)[:,np.newaxis]).flatten()
    #bnd_dl2 = np.repeat(np.arange(1,len(nxn2)),M) #not used yet
    
    #domain 3
    di_l3 = np.zeros((len(di3)-2)*2)
    for i in range(1,len(di3)-1):
        di_l3[i*2-2] = di3[i]-1
        di_l3[i*2-1] = di3[i]
    di_l3 = np.array(di_l3, dtype=int)
    
    #build the lists of indices for the different places in the Jacobian
    x3 = np.delete(np.arange(di3[-1]),di_l3)[1:-1] # x coordinates for the points which are not on a aboundary
    xr3 = x3.repeat(N) # x for N values, mostly i in old code
    xr_m3 = xr3*M #M*i in old coe
    xrm_m3 = (xr3-1)*M
    xrp_m3 = (xr3+1)*M
    xr_mj3 = xr_m3+np.tile(np.arange(1,M),di3[-1]-2-(len(di3[1:-1])*2))
    xrp_mj3 = xrp_m3+np.tile(np.arange(1,M),di3[-1]-2-(len(di3[1:-1])*2))
    xrm_mj3 = xrm_m3+np.tile(np.arange(1,M),di3[-1]-2-(len(di3[1:-1])*2))
    jl3 = np.tile(np.arange(N),di3[-1]-2-(len(di3[1:-1])*2))
    
    #for the k things we need to repeat some arrays
    xr_mjr3=np.repeat(xr_m3,N)+np.tile(np.arange(1,M),(di3[-1]-2-(len(di3[1:-1])*2))).repeat(N)
    xrr3 = xr3.repeat(N)
    jlr3 = jl3.repeat(N)
    xr_mr3 = xr_m3.repeat(N)
    xrp_mr3 = xrp_m3.repeat(N)
    xrm_mr3 = xrm_m3.repeat(N)
    
    kl3 = np.tile(jl3,N)
    xr_mk3=np.repeat(xr_m3,N)+np.tile(np.tile(np.arange(1,M),(di3[-1]-2-(len(di3[1:-1])*2))),N)
    xrp_mk3=np.repeat(xrp_m3,N)+np.tile(np.tile(np.arange(1,M),(di3[-1]-2-(len(di3[1:-1])*2))),N)
    xrm_mk3=np.repeat(xrm_m3,N)+np.tile(np.tile(np.arange(1,M),(di3[-1]-2-(len(di3[1:-1])*2))),N)

    x_m3 = x3*M
    xp_m3 = (x3+1)*M
    xm_m3 = (x3-1)*M

    #for the boundaries 
    bnd3 = di1[-1]*M+di2[-1]*M+(di3[np.arange(1,len(nxn3))]*M+np.arange(M)[:,np.newaxis]).flatten()
    #bnd_dl3 = np.repeat(np.arange(1,len(nxn3)),M) #not used yet
    
    #often repeated stuff
    dom2_co = di1[-1]*M
    dom3_co = (di1[-1]+di2[-1])*M
    
    
    def jac_build(ans,terms):
        jac = np.zeros(((di1[-1]+di2[-1]+di3[-1])*M+3,(di1[-1]+di2[-1]+di3[-1])*M+3))
        
        if 1 in terms : #contribution to jacobian due to term T1
            jac[xr_mj1,xrm_mj1] = jac[xr_mj1,xrm_mj1] - C1a1[xr1]* ans[-3]/(2*dl1[xr1]) 
            jac[xr_mj1,xrp_mj1] = jac[xr_mj1,xrp_mj1] + C1a1[xr1]* ans[-3]/(2*dl1[xr1]) 
            jac[xr_mj1,-3] = jac[xr_mj1,-3] + C1a1[xr1] * ((ans[xrp_mj1]-ans[xrm_mj1])/(2*dl1[xr1]))
            
            jac[dom2_co+xr_mj2,dom2_co+xrm_mj2] = jac[dom2_co+xr_mj2,dom2_co+xrm_mj2] - C1a2[xr2]* ans[-2]/(2*dl2[xr2]) 
            jac[dom2_co+xr_mj2,dom2_co+xrp_mj2] = jac[dom2_co+xr_mj2,dom2_co+xrp_mj2] + C1a2[xr2]* ans[-2]/(2*dl2[xr2]) 
            jac[dom2_co+xr_mj2,-2] = jac[dom2_co+xr_mj2,-2] + C1a2[xr2] * (ans[dom2_co+xrp_mj2]-ans[dom2_co+xrm_mj2])/(2*dl2[xr2])
            
            jac[dom3_co+xr_mj3,dom3_co+xrm_mj3] = jac[dom3_co+xr_mj3,dom3_co+xrm_mj3] - C1a3[xr3]* ans[-1]/(2*dl3[xr3]) 
            jac[dom3_co+xr_mj3,dom3_co+xrp_mj3] = jac[dom3_co+xr_mj3,dom3_co+xrp_mj3] + C1a3[xr3]* ans[-1]/(2*dl3[xr3]) 
            jac[dom3_co+xr_mj3,-1] = jac[dom3_co+xr_mj3,-1] + C1a3[xr3] * ((ans[dom3_co+xrp_mj3]-ans[dom3_co+xrm_mj3])/(2*dl3[xr3]))
        
        if 2 in terms: #contribution to jacobian due to term T2
            #domain 1
            jac[xr_mj1,xrm_mj1] = jac[xr_mj1,xrm_mj1] - C2a1[xr1,jl1]*ans[-3]/(2*dl1[xr1]) - C2b1[xr1,jl1]/(2*dl1[xr1]) * (ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1])
            jac[xr_mj1,xrp_mj1] = jac[xr_mj1,xrp_mj1] + C2a1[xr1,jl1]*ans[-3]/(2*dl1[xr1]) + C2b1[xr1,jl1]/(2*dl1[xr1]) * (ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1])
            jac[xr_mj1,xrm_m1] = (jac[xr_mj1,xrm_m1] - C2b1[xr1,jl1]/(2*dl1[xr1]) * (ans[xrp_mj1]-ans[xrm_mj1])/(2*dl1[xr1])
                                  - np.sum([C2d1[xr1,jl1,n-1] * (ans[xrp_m1+n]-ans[xrm_m1+n]) for n in range(1,M)],0)/(4*dl1[xr1]**2))
            jac[xr_mj1,xrp_m1] = (jac[xr_mj1,xrp_m1]  + C2b1[xr1,jl1]/(2*dl1[xr1]) * (ans[xrp_mj1]-ans[xrm_mj1])/(2*dl1[xr1]) 
                                  + np.sum([C2d1[xr1,jl1,n-1] * (ans[xrp_m1+n]-ans[xrm_m1+n]) for n in range(1,M)],0)/(4*dl1[xr1]**2))
            jac[xr_mjr1,xrm_mk1] = jac[xr_mjr1,xrm_mk1] - C2c1[xrr1,jlr1,kl1]*ans[-3]/(2*dl1[xrr1]) - C2d1[xrr1,jlr1,kl1]*(ans[xrp_mr1]-ans[xrm_mr1])/(4*dl1[xrr1]**2)
            jac[xr_mjr1,xrp_mk1] = jac[xr_mjr1,xrp_mk1] + C2c1[xrr1,jlr1,kl1]*ans[-3]/(2*dl1[xrr1]) + C2d1[xrr1,jlr1,kl1]*(ans[xrp_mr1]-ans[xrm_mr1])/(4*dl1[xrr1]**2)
            jac[xr_mj1,-3] = jac[xr_mj1,-3] + C2a1[xr1,jl1]*(ans[xrp_mj1]-ans[xrm_mj1])/(2*dl1[xr1]) + np.sum([C2c1[xr1,jl1,n-1] * (ans[xrp_m1+n]-ans[xrm_m1+n]) for n in range(1,M)],0)/(2*dl1[xr1])
            
            #domain 2
            jac[dom2_co+xr_mj2,dom2_co+xrm_mj2] = jac[dom2_co+xr_mj2,dom2_co+xrm_mj2] - C2a2[xr2,jl2]*ans[-2]/(2*dl2[xr2]) - C2b2[xr2,jl2]/(2*dl2[xr2]) * (ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2])
            jac[dom2_co+xr_mj2,dom2_co+xrp_mj2] = jac[dom2_co+xr_mj2,dom2_co+xrp_mj2] + C2a2[xr2,jl2]*ans[-2]/(2*dl2[xr2]) + C2b2[xr2,jl2]/(2*dl2[xr2]) * (ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2])
            jac[dom2_co+xr_mj2,dom2_co+xrm_m2] = (jac[dom2_co+xr_mj2,dom2_co+xrm_m2] - C2b2[xr2,jl2]/(2*dl2[xr2]) * (ans[dom2_co+xrp_mj2]-ans[dom2_co+xrm_mj2])/(2*dl2[xr2])  
                                                      - np.sum([C2d2[xr2,jl2,n-1] * (ans[dom2_co+xrp_m2+n]-ans[dom2_co+xrm_m2+n]) for n in range(1,M)],0)/(4*dl2[xr2]**2) )
            jac[dom2_co+xr_mj2,dom2_co+xrp_m2] = (jac[dom2_co+xr_mj2,dom2_co+xrp_m2]  + C2b2[xr2,jl2]/(2*dl2[xr2]) * (ans[dom2_co+xrp_mj2]-ans[dom2_co+xrm_mj2])/(2*dl2[xr2]) 
                                                      + np.sum([C2d2[xr2,jl2,n-1] * (ans[dom2_co+xrp_m2+n]-ans[dom2_co+xrm_m2+n]) for n in range(1,M)],0)/(4*dl2[xr2]**2) )
            jac[dom2_co+xr_mjr2,dom2_co+xrm_mk2] = (jac[dom2_co+xr_mjr2,dom2_co+xrm_mk2] - C2c2[xrr2,jlr2,kl2]*ans[-2]/(2*dl2[xrr2]) - C2d2[xrr2,jlr2,kl2]*(ans[dom2_co+xrp_mr2]-ans[dom2_co+xrm_mr2])/(4*dl2[xrr2]**2) )
            jac[dom2_co+xr_mjr2,dom2_co+xrp_mk2] = (jac[dom2_co+xr_mjr2,dom2_co+xrp_mk2] + C2c2[xrr2,jlr2,kl2]*ans[-2]/(2*dl2[xrr2]) + C2d2[xrr2,jlr2,kl2]*(ans[dom2_co+xrp_mr2]-ans[dom2_co+xrm_mr2])/(4*dl2[xrr2]**2) )
            jac[dom2_co+xr_mj2,-2] = jac[dom2_co+xr_mj2,-2] + C2a2[xr2,jl2]*(ans[dom2_co+xrp_mj2]-ans[dom2_co+xrm_mj2])/(2*dl2[xr2]) \
                + np.sum([C2c2[xr2,jl2,n-1] * (ans[dom2_co+xrp_m2+n]-ans[dom2_co+xrm_m2+n]) for n in range(1,M)],0)/(2*dl2[xr2])
                
            #domain 3
            jac[dom3_co+xr_mj3,dom3_co+xrm_mj3] = (jac[dom3_co+xr_mj3,dom3_co+xrm_mj3] - C2a3[xr3,jl3]*ans[-1]/(2*dl3[xr3])
                                                                           - C2b3[xr3,jl3]/(2*dl3[xr3]) * (ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3]) )
            jac[dom3_co+xr_mj3,dom3_co+xrp_mj3] = (jac[dom3_co+xr_mj3,dom3_co+xrp_mj3] + C2a3[xr3,jl3]*ans[-1]/(2*dl3[xr3]) 
                                                                           + C2b3[xr3,jl3]/(2*dl3[xr3]) * (ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3]) )
            jac[dom3_co+xr_mj3,dom3_co+xrm_m3] = (jac[dom3_co+xr_mj3,dom3_co+xrm_m3] - C2b3[xr3,jl3]/(2*dl3[xr3]) * (ans[dom3_co+xrp_mj3]-ans[dom3_co+xrm_mj3])/(2*dl3[xr3]) 
                                                                          - np.sum([C2d3[xr3,jl3,n-1] * (ans[dom3_co+xrp_m3+n]-ans[dom3_co+xrm_m3+n]) for n in range(1,M)],0)/(4*dl3[xr3]**2) )
            jac[dom3_co+xr_mj3,dom3_co+xrp_m3] = (jac[dom3_co+xr_mj3,dom3_co+xrp_m3] + C2b3[xr3,jl3]/(2*dl3[xr3]) * (ans[dom3_co+xrp_mj3]-ans[dom3_co+xrm_mj3])/(2*dl3[xr3])
                                                                          + np.sum([C2d3[xr3,jl3,n-1] * (ans[dom3_co+xrp_m3+n]-ans[dom3_co+xrm_m3+n]) for n in range(1,M)],0)/(4*dl3[xr3]**2) )
            jac[dom3_co+xr_mjr3,dom3_co+xrm_mk3] = (jac[dom3_co+xr_mjr3,dom3_co+xrm_mk3] - C2c3[xrr3,jlr3,kl3]*ans[-1]/(2*dl3[xrr3]) 
                                                                            - C2d3[xrr3,jlr3,kl3]*(ans[dom3_co+xrp_mr3]-ans[dom3_co+xrm_mr3])/(4*dl3[xrr3]**2) )
            jac[dom3_co+xr_mjr3,dom3_co+xrp_mk3] = (jac[dom3_co+xr_mjr3,dom3_co+xrp_mk3] + C2c3[xrr3,jlr3,kl3]*ans[-1]/(2*dl3[xrr3])
                                                                            + C2d3[xrr3,jlr3,kl3]*(ans[dom3_co+xrp_mr3]-ans[dom3_co+xrm_mr3])/(4*dl3[xrr3]**2))
            jac[dom3_co+xr_mj3,-1] = jac[dom3_co+xr_mj3,-1] + C2a3[xr3,jl3]* (ans[dom3_co+xrp_mj3]-ans[dom3_co+xrm_mj3])/(2*dl3[xr3]) \
                                     +np.sum([C2c3[xr3,jl3,n-1]*  (ans[dom3_co+xrp_m3+n]-ans[dom3_co+xrm_m3+n]) for n in range(1,M)],0)/(2*dl3[xr3])
               
            
        if 3 in terms: #contribution to jacobian due to term T3
            jac[xr_mj1,xrm_m1] = jac[xr_mj1,xrm_m1] - C3a1[xr1,jl1]*ans[-3]/(2*dl1[xr1]) - C3b1[xr1,jl1]/dl1[xr1] * (ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1])
            jac[xr_mj1,xrp_m1] = jac[xr_mj1,xrp_m1] + C3a1[xr1,jl1]*ans[-3]/(2*dl1[xr1]) + C3b1[xr1,jl1]/dl1[xr1] * (ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1])
            jac[xr_mj1,-3] = jac[xr_mj1,-3] + C3a1[xr1,jl1] * (ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1])
            
            jac[dom2_co+xr_mj2,dom2_co+xrm_m2] = jac[dom2_co+xr_mj2,dom2_co+xrm_m2] - C3a2[xr2,jl2]*ans[-2]/(2*dl2[xr2]) - C3b2[xr2,jl2]/dl2[xr2] * (ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2])
            jac[dom2_co+xr_mj2,dom2_co+xrp_m2] = jac[dom2_co+xr_mj2,dom2_co+xrp_m2] + C3a2[xr2,jl2]*ans[-2]/(2*dl2[xr2]) + C3b2[xr2,jl2]/dl2[xr2] * (ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2])
            jac[dom2_co+xr_mj2,-2] = jac[dom2_co+xr_mj2,-2] + C3a2[xr2,jl2] * (ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2])
            
            jac[dom3_co+xr_mj3,dom3_co+xrm_m3] = (jac[dom3_co+xr_mj3,dom3_co+xrm_m3] - C3a3[xr3,jl3]*ans[-1]/(2*dl3[xr3]) 
                                                                          - C3b3[xr3,jl3]/dl3[xr3] * (ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3]) )
            jac[dom3_co+xr_mj3,dom3_co+xrp_m3] = (jac[dom3_co+xr_mj3,dom3_co+xrp_m3] + C3a3[xr3,jl3]*ans[-1]/(2*dl3[xr3]) 
                                                                          + C3b3[xr3,jl3]/dl3[xr3] * (ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3]) )
            jac[dom3_co+xr_mj3,-1] = jac[dom3_co+xr_mj3,-1] + C3a3[xr3,jl3] * (ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3]) 

        if 5 in terms: #contribution to jacobian due to term T5
            jac[xr_mj1, xrm_m1] = (jac[xr_mj1, xrm_m1] + C5a1[xr1,jl1]*ans[xr_mj1]/(dl1[xr1]**2) - C5b1[xr1,jl1]/(2*dl1[xr1])*ans[xr_mj1]
                                   + np.sum([ans[xr_m1+n]*C5c1[xr1,jl1,n-1]/(dl1[xr1]**2) for n in range(1,M)],0) 
                                   - np.sum([ans[xr_m1+n]*C5d1[xr1,jl1,n-1]/(2*dl1[xr1]) for n in range(1,M)],0) )
            jac[xr_mj1, xr_m1] = jac[xr_mj1, xr_m1] - 2*C5a1[xr1,jl1]*ans[xr_mj1]/(dl1[xr1]**2) -2* np.sum([ans[xr_m1+n]*C5c1[xr1,jl1,n-1]/(dl1[xr1]**2) for n in range(1,M)],0)
            jac[xr_mj1, xrp_m1] = (jac[xr_mj1, xrp_m1] + C5a1[xr1,jl1]*ans[xr_mj1]/(dl1[xr1]**2) + C5b1[xr1,jl1]/(2*dl1[xr1])*ans[xr_mj1]
                                           + np.sum([ans[xr_m1+n]*C5c1[xr1,jl1,n-1]/(dl1[xr1]**2) for n in range(1,M)],0)
                                           + np.sum([ans[xr_m1+n]*C5d1[xr1,jl1,n-1]/(2*dl1[xr1]) for n in range(1,M)],0) )
            jac[xr_mj1,xr_mj1] = (jac[xr_mj1, xr_mj1] + C5a1[xr1,jl1]*(ans[xrp_m1]-2*ans[xr_m1]+ans[xrm_m1])/(dl1[xr1]**2) 
                                             + C5b1[xr1,jl1]*(ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1]) + C5e1[xr1,jl1] )
            jac[xr_mjr1, xr_mk1] = (jac[xr_mjr1, xr_mk1] + C5c1[xrr1,jlr1,kl1]*(ans[xrp_mr1]-2*ans[xr_mr1]+ans[xrm_mr1])/(dl1[xrr1]**2)
                                                 + C5d1[xrr1,jlr1,kl1]*(ans[xrp_mr1]-ans[xrm_mr1])/(2*dl1[xrr1]) +C5f1[xrr1,jlr1,kl1] )
            #domain 2
            jac[dom2_co+xr_mj2, dom2_co+xrm_m2] = (jac[dom2_co+xr_mj2, dom2_co+xrm_m2] + C5a2[xr2,jl2]*ans[dom2_co+xr_mj2]/(dl2[xr2]**2) - C5b2[xr2,jl2]/(2*dl2[xr2])*ans[dom2_co+xr_mj2]
                                   + np.sum([ans[dom2_co+xr_m2+n]*C5c2[xr2,jl2,n-1]/(dl2[xr2]**2) for n in range(1,M)],0) 
                                   - np.sum([ans[dom2_co+xr_m2+n]*C5d2[xr2,jl2,n-1]/(2*dl2[xr2]) for n in range(1,M)],0) )
            jac[dom2_co+xr_mj2, dom2_co+xr_m2] = (jac[dom2_co+xr_mj2, dom2_co+xr_m2] - 2*C5a2[xr2,jl2]*ans[dom2_co+xr_mj2]/(dl2[xr2]**2) 
                                                      -2* np.sum([ans[dom2_co+xr_m2+n]*C5c2[xr2,jl2,n-1]/(dl2[xr2]**2) for n in range(1,M)],0) )
            jac[dom2_co+xr_mj2, dom2_co+xrp_m2] = (jac[dom2_co+xr_mj2, dom2_co+xrp_m2] + C5a2[xr2,jl2]*ans[dom2_co+xr_mj2]/(dl2[xr2]**2) + C5b2[xr2,jl2]/(2*dl2[xr2])*ans[dom2_co+xr_mj2]
                                           + np.sum([ans[dom2_co+xr_m2+n]*C5c2[xr2,jl2,n-1]/(dl2[xr2]**2) for n in range(1,M)],0)
                                           + np.sum([ans[dom2_co+xr_m2+n]*C5d2[xr2,jl2,n-1]/(2*dl2[xr2]) for n in range(1,M)],0) )
            jac[dom2_co+xr_mj2,dom2_co+xr_mj2] = (jac[dom2_co+xr_mj2, dom2_co+xr_mj2] + C5a2[xr2,jl2]*(ans[dom2_co+xrp_m2]-2*ans[dom2_co+xr_m2]+ans[dom2_co+xrm_m2])/(dl2[xr2]**2) 
                                             + C5b2[xr2,jl2]*(ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2]) + C5e2[xr2,jl2] )
            jac[dom2_co+xr_mjr2, dom2_co+xr_mk2] = (jac[dom2_co+xr_mjr2, dom2_co+xr_mk2] + C5c2[xrr2,jlr2,kl2]*(ans[dom2_co+xrp_mr2]-2*ans[dom2_co+xr_mr2]+ans[dom2_co+xrm_mr2])/(dl2[xrr2]**2)
                                                 + C5d2[xrr2,jlr2,kl2]*(ans[dom2_co+xrp_mr2]-ans[dom2_co+xrm_mr2])/(2*dl2[xrr2]) +C5f2[xrr2,jlr2,kl2] )
            #domain 3
            jac[dom3_co+xr_mj3, dom3_co+xrm_m3] = (jac[dom3_co+xr_mj3, dom3_co+xrm_m3] + C5a3[xr3,jl3]*ans[dom3_co+xr_mj3]/(dl3[xr3]**2)
                                                                           - C5b3[xr3,jl3]/(2*dl3[xr3])*ans[dom3_co+xr_mj3]
                                                                           + np.sum([ans[dom3_co+xr_m3+n]*C5c3[xr3,jl3,n-1]/(dl3[xr3]**2) for n in range(1,M)],0) 
                                                                           - np.sum([ans[dom3_co+xr_m3+n]*C5d3[xr3,jl3,n-1]/(2*dl3[xr3]) for n in range(1,M)],0) )
            jac[dom3_co+xr_mj3, dom3_co+xr_m3] = (jac[dom3_co+xr_mj3, dom3_co+xr_m3] - 2*C5a3[xr3,jl3]*ans[dom3_co+xr_mj3]/(dl3[xr3]**2)
                                                                          -2* np.sum([ans[dom3_co+xr_m3+n]*C5c3[xr3,jl3,n-1]/(dl3[xr3]**2) for n in range(1,M)],0) )
            jac[dom3_co+xr_mj3, dom3_co+xrp_m3] = (jac[dom3_co+xr_mj3, dom3_co+xrp_m3] + C5a3[xr3,jl3]*ans[dom3_co+xr_mj3]/(dl3[xr3]**2) 
                                                                           + C5b3[xr3,jl3]/(2*dl3[xr3])*ans[dom3_co+xr_mj3]
                                                                           + np.sum([ans[dom3_co+xr_m3+n]*C5c3[xr3,jl3,n-1]/(dl3[xr3]**2) for n in range(1,M)],0)
                                                                           + np.sum([ans[dom3_co+xr_m3+n]*C5d3[xr3,jl3,n-1]/(2*dl3[xr3]) for n in range(1,M)],0) )
            jac[dom3_co+xr_mj3, dom3_co+xr_mj3] = (jac[dom3_co+xr_mj3, dom3_co+xr_mj3] 
                                                                           + C5a3[xr3,jl3]*(ans[dom3_co+xrp_m3]-2*ans[dom3_co+xr_m3]+ans[dom3_co+xrm_m3])/(dl3[xr3]**2) 
                                                                           + C5b3[xr3,jl3]*(ans[xrp_m3]-ans[xrm_m3])/(2*dl3[xr3]) + C5e3[xr3,jl3] )
            jac[dom3_co+xr_mjr3, dom3_co+xr_mk3] = (jac[dom3_co+xr_mjr3, dom3_co+xr_mk3] 
                                                                            + C5c3[xrr3,jlr3,kl3]*(ans[dom3_co+xrp_mr3]-2*ans[dom3_co+xr_mr3]+ans[dom3_co+xrm_mr3])/(dl3[xrr3]**2)
                                                                            + C5d3[xrr3,jlr3,kl3]*(ans[dom3_co+xrp_mr3]-ans[dom3_co+xrm_mr3])/(2*dl3[xrr3]) +C5f3[xrr3,jlr3,kl3] )

        if 6 in terms: #contribution to jacobian due to term T6
            jac[xr_mj1,xr_mj1] = jac[xr_mj1,xr_mj1] + C6a1[xr1,jl1]
            jac[dom2_co+xr_mj2,dom2_co+xr_mj2] = jac[dom2_co+xr_mj2,dom2_co+xr_mj2] + C6a2[xr2,jl2]
            jac[dom3_co+xr_mj3,dom3_co+xr_mj3] = jac[dom3_co+xr_mj3,dom3_co+xr_mj3] + C6a3[xr3,jl3]

        if 7 in terms: 
            jac[xr_mj1,xrm_mj1] = jac[xr_mj1,xrm_mj1] - C7a1[xr1]/(2*dl1[xr1]) + C7b1[xr1] / (dl1[xr1]**2)
            jac[xr_mj1,xr_mj1] = jac[xr_mj1,xr_mj1] - 2*C7b1[xr1]/(dl1[xr1]**2)
            jac[xr_mj1,xrp_mj1] = jac[xr_mj1,xrp_mj1] + C7a1[xr1]/(2*dl1[xr1]) + C7b1[xr1] / (dl1[xr1]**2)
            
            jac[dom2_co+xr_mj2,dom2_co+xrm_mj2] = jac[dom2_co+xr_mj2,dom2_co+xrm_mj2] - C7a2[xr2]/(2*dl2[xr2]) + C7b2[xr2] / (dl2[xr2]**2)
            jac[dom2_co+xr_mj2,dom2_co+xr_mj2] = jac[dom2_co+xr_mj2,dom2_co+xr_mj2] - 2*C7b2[xr2]/(dl2[xr2]**2)
            jac[dom2_co+xr_mj2,dom2_co+xrp_mj2] = jac[dom2_co+xr_mj2,dom2_co+xrp_mj2] + C7a2[xr2]/(2*dl2[xr2]) + C7b2[xr2] / (dl2[xr2]**2)
            
            jac[dom3_co+xr_mj3,dom3_co+xrm_mj3] = jac[dom3_co+xr_mj3,dom3_co+xrm_mj3] - C7a3[xr3]/(2*dl3[xr3]) + C7b3[xr3] / (dl3[xr3]**2)
            jac[dom3_co+xr_mj3,dom3_co+xr_mj3] = jac[dom3_co+xr_mj3,dom3_co+xr_mj3] - 2*C7b3[xr3]/(dl3[xr3]**2)
            jac[dom3_co+xr_mj3,dom3_co+xrp_mj3] = jac[dom3_co+xr_mj3,dom3_co+xrp_mj3] + C7a3[xr3]/(2*dl3[xr3]) + C7b3[xr3] / (dl3[xr3]**2)
        
        #contribution to jacobian due to term the average salt equation - always present 
        #domain 1
        #left
        jac[x_m1, xm_m1] = (jac[x_m1, xm_m1] - C10a1[x1]/(2*dl1[x1]) + C10b1[x1]/(dl1[x1]**2) - 1/(2*dl1[x1])*np.sum([C10c1[x1,n-1]*ans[x_m1+n] for n in range(1,M)],0) 
                                       + 1/(dl1[x1]**2)*np.sum([C10d1[x1,n-1]*ans[x_m1+n] for n in range(1,M)],0) 
                                       - 1/(2*dl1[x1])*np.sum([C10f1[x1,n-1]*(ans[xp_m1+n]-ans[xm_m1+n])/(2*dl1[x1]) for n in range(1,M)],0) - C10h1[x1]*ans[-3]/(2*dl1[x1]))
        jac[xr_m1, xrm_mj1] = jac[xr_m1, xrm_mj1] - C10e1[xr1,jl1]*ans[-3]/(2*dl1[xr1]) - C10f1[xr1,jl1]/(2*dl1[xr1])*(ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1])
        #center
        jac[x_m1,x_m1] = jac[x_m1,x_m1] - 2/(dl1[x1]**2)*C10b1[x1]  -2/(dl1[x1]**2)*np.sum([C10d1[x1,n-1]*ans[x_m1+n] for n in range(1,M)],0)
        jac[xr_m1, xr_mj1] = jac[xr_m1,xr_mj1] + (ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1]) * C10c1[xr1,jl1] + (ans[xrp_m1]-2*ans[xr_m1]+ans[xrm_m1])/(dl1[xr1]**2) * C10d1[xr1,jl1] + C10g1[xr1,jl1]
        #right
        jac[x_m1, xp_m1] = (jac[x_m1,xp_m1] + C10a1[x1]/(2*dl1[x1]) + C10b1[x1]/(dl1[x1]**2) + 1/(2*dl1[x1])*np.sum([C10c1[x1,n-1]*ans[x_m1+n] for n in range(1,M)],0) 
                                       + 1/(dl1[x1]**2)*np.sum([C10d1[x1,n-1]*ans[x_m1+n] for n in range(1,M)],0) 
                                       + 1/(2*dl1[x1])*np.sum([C10f1[x1,n-1]*(ans[xp_m1+n]-ans[xm_m1+n])/(2*dl1[x1]) for n in range(1,M)],0) + C10h1[x1]*ans[-3]/(2*dl1[x1]))
        jac[xr_m1, xrp_mj1] = jac[xr_m1,xrp_mj1] + C10e1[xr1,jl1]*ans[-3]/(2*dl1[xr1]) + C10f1[xr1,jl1]/(2*dl1[xr1])*(ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1])
        #discharge
        jac[x_m1,-3] = jac[x_m1,-3] + np.sum([C10e1[x1,n-1]*(ans[xp_m1+n]-ans[xm_m1+n])/(2*dl1[x1]) for n in range(1,M)],0) + C10h1[x1]*(ans[xp_m1]-ans[xm_m1])/(2*dl1[x1])
        
        #domain 2
        #left
        jac[dom2_co+x_m2, dom2_co+xm_m2] = (jac[dom2_co+x_m2, dom2_co+xm_m2] - C10a2[x2]/(2*dl2[x2]) + C10b2[x2]/(dl2[x2]**2)
                                                - 1/(2*dl2[x2])*np.sum([C10c2[x2,n-1]*ans[dom2_co+x_m2+n] for n in range(1,M)],0) 
                                       + 1/(dl2[x2]**2)*np.sum([C10d2[x2,n-1]*ans[dom2_co+x_m2+n] for n in range(1,M)],0) 
                                       - 1/(2*dl2[x2])*np.sum([C10f2[x2,n-1]*(ans[dom2_co+xp_m2+n]-ans[dom2_co+xm_m2+n])/(2*dl2[x2]) for n in range(1,M)],0) - C10h2[x2]*ans[-2]/(2*dl2[x2]))
        jac[dom2_co+xr_m2, dom2_co+xrm_mj2] = jac[dom2_co+xr_m2, dom2_co+xrm_mj2] - C10e2[xr2,jl2]*ans[-2]/(2*dl2[xr2]) - C10f2[xr2,jl2]/(2*dl2[xr2])*(ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2])
        #center
        jac[dom2_co+x_m2, dom2_co+x_m2] = jac[dom2_co+x_m2,dom2_co+x_m2] - 2/(dl2[x2]**2)*C10b2[x2]  -2/(dl2[x2]**2)*np.sum([C10d2[x2,n-1]*ans[dom2_co+x_m2+n] for n in range(1,M)],0)
        jac[dom2_co+xr_m2, dom2_co+xr_mj2] = (jac[dom2_co+xr_m2,dom2_co+xr_mj2] + (ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2]) * C10c2[xr2,jl2] 
                                                  + (ans[dom2_co+xrp_m2]-2*ans[dom2_co+xr_m2]+ans[dom2_co+xrm_m2])/(dl2[xr2]**2) * C10d2[xr2,jl2] + C10g2[xr2,jl2] )
        #right
        jac[dom2_co+x_m2, dom2_co+xp_m2] = (jac[dom2_co+x_m2,dom2_co+xp_m2] + C10a2[x2]/(2*dl2[x2]) + C10b2[x2]/(dl2[x2]**2) + 1/(2*dl2[x2])*np.sum([C10c2[x2,n-1]*ans[dom2_co+x_m2+n] for n in range(1,M)],0) 
                                       + 1/(dl2[x2]**2)*np.sum([C10d2[x2,n-1]*ans[dom2_co+x_m2+n] for n in range(1,M)],0) 
                                       + 1/(2*dl2[x2])*np.sum([C10f2[x2,n-1]*(ans[dom2_co+xp_m2+n]-ans[dom2_co+xm_m2+n])/(2*dl2[x2]) for n in range(1,M)],0) + C10h2[x2]*ans[-2]/(2*dl2[x2]) )
        jac[dom2_co+xr_m2, dom2_co+xrp_mj2] = jac[dom2_co+xr_m2,dom2_co+xrp_mj2] + C10e2[xr2,jl2]*ans[-2]/(2*dl2[xr2]) + C10f2[xr2,jl2]/(2*dl2[xr2])*(ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2])
        #discahrge
        jac[dom2_co+x_m2, -2] = jac[dom2_co+x_m2, -2] + np.sum([C10e2[x2,n-1]*(ans[dom2_co+xp_m2+n]-ans[dom2_co+xm_m2+n])/(2*dl2[x2]) for n in range(1,M)],0) + C10h2[x2]*(ans[dom2_co+xp_m2]-ans[dom2_co+xm_m2])/(2*dl2[x2])
        
        #domain 3
        #left
        jac[dom3_co+x_m3, dom3_co+xm_m3] = (jac[dom3_co+x_m3, dom3_co+xm_m3] - C10a3[x3]/(2*dl3[x3]) + C10b3[x3]/(dl3[x3]**2) 
                                        - 1/(2*dl3[x3])*np.sum([C10c3[x3,n-1]*ans[dom3_co+x_m3+n] for n in range(1,M)],0) 
                                       + 1/(dl3[x3]**2)*np.sum([C10d3[x3,n-1]*ans[dom3_co+x_m3+n] for n in range(1,M)],0) 
                                       - 1/(2*dl3[x3])*np.sum([C10f3[x3,n-1]*(ans[dom3_co+xp_m3+n]-ans[dom3_co+xm_m3+n])/(2*dl3[x3]) for n in range(1,M)],0) - C10h3[x3]*ans[-1]/(2*dl3[x3]))
        jac[dom3_co+xr_m3, dom3_co+xrm_mj3] = (jac[dom3_co+xr_m3, dom3_co+xrm_mj3] - C10e3[xr3,jl3]*ans[-1]/(2*dl3[xr3]) 
                                                                       - C10f3[xr3,jl3]/(2*dl3[xr3])*(ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3]) )
        #center
        jac[dom3_co+x_m3,dom3_co+x_m3] = (jac[dom3_co+x_m3,dom3_co+x_m3] - 2/(dl3[x3]**2)*C10b3[x3] 
                                                                  -2/(dl3[x3]**2)*np.sum([C10d3[x3,n-1]*ans[dom3_co+x_m3+n] for n in range(1,M)],0) )
        jac[dom3_co+xr_m3, dom3_co+xr_mj3] = (jac[dom3_co+xr_m3,dom3_co+xr_mj3] + (ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3]) * C10c3[xr3,jl3] 
                                                                      + (ans[dom3_co+xrp_m3]-2*ans[dom3_co+xr_m3]+ans[dom3_co+xrm_m3])/(dl3[xr3]**2) * C10d3[xr3,jl3] + C10g3[xr3,jl3] )
        #right
        jac[dom3_co+x_m3, dom3_co+xp_m3] = (jac[dom3_co+x_m3,dom3_co+xp_m3] + C10a3[x3]/(2*dl3[x3]) 
                                       + C10b3[x3]/(dl3[x3]**2) + 1/(2*dl3[x3])*np.sum([C10c3[x3,n-1]*ans[dom3_co+x_m3+n] for n in range(1,M)],0) 
                                       + 1/(dl3[x3]**2)*np.sum([C10d3[x3,n-1]*ans[dom3_co+x_m3+n] for n in range(1,M)],0) 
                                       + 1/(2*dl3[x3])*np.sum([C10f3[x3,n-1]*(ans[dom3_co+xp_m3+n]-ans[dom3_co+xm_m3+n])/(2*dl3[x3]) for n in range(1,M)],0) + C10h3[x3]*ans[-1]/(2*dl3[x3]))
        jac[dom3_co+xr_m3, dom3_co+xrp_mj3] = (jac[dom3_co+xr_m3,dom3_co+xrp_mj3] + C10e3[xr3,jl3]*ans[-1]/(2*dl3[xr3]) 
                                                                       + C10f3[xr3,jl3]/(2*dl3[xr3])*(ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3]) )
        #discharge
        jac[dom3_co+x_m3,-1] = jac[dom3_co+x_m3,-1] + np.sum([C10e3[x3,n-1]*(ans[dom3_co+xp_m3+n]-ans[dom3_co+xm_m3+n])/(2*dl3[x3]) for n in range(1,M)],0) + C10h3[x3]*(ans[dom3_co+xp_m3]-ans[dom3_co+xm_m3])/(2*dl3[x3])
        
        #river boundary - salt is prescribed
        jac[np.arange(M),np.arange(M)] = jac[np.arange(M),np.arange(M)] + 1

        
        # =============================================================================
        #         #branching point
        # =============================================================================
        #I kept the loops here, in terms of speed it should not be an issue

        #boundary conditions imposed at the beginning of domain 1
        #depth - averaged       
        jac[dom2_co-M, (di1[-1]-3)*M] = 1/(2*dl1[-1]) * np.sum(C13c1*ans[M*(di1[-1]-1)+1:M*di1[-1]]) + 1/(2*dl1[-1]) * C13d1
        jac[(dom2_co-M), (di1[-1]-2)*M] = -4/(2*dl1[-1]) * np.sum(C13c1*ans[M*(di1[-1]-1)+1:M*di1[-1]]) - 4/(2*dl1[-1]) * C13d1
        jac[(dom2_co-M), (dom2_co-M)] = 3/(2*dl1[-1]) * np.sum(C13c1*ans[M*(di1[-1]-1)+1:M*di1[-1]]) + 3/(2*dl1[-1]) * C13d1 + C13a1*ans[-3]
        for k in range(1,M): jac[(dom2_co-M), (dom2_co-M)+k] = C13b1[k-1]*ans[-3]+C13c1[k-1]*(ans[M*(di1[-1]-3)]-4*ans[M*(di1[-1]-2)]+3*ans[M*(di1[-1]-1)])/(2*dl1[-1]) 
        jac[(dom2_co-M), -3] = C13a1 + np.sum(ans[M*(di1[-1]-1)+1:M*di1[-1]]*C13b1)
        
        jac[(dom2_co-M), (di1[-1]+0)*M] = 3/(2*dl2[0]) * np.sum(C13c2*ans[M*(di1[-1])+1:M*(di1[-1]+1)]) + C13d2* 3/(2*dl2[0]) - C13a2*ans[-2]
        jac[(dom2_co-M), (di1[-1]+1)*M] =-4/(2*dl2[0]) * np.sum(C13c2*ans[M*(di1[-1])+1:M*(di1[-1]+1)]) + C13d2*-4/(2*dl2[0])
        jac[(dom2_co-M), (di1[-1]+2)*M] = 1/(2*dl2[0]) * np.sum(C13c2*ans[M*(di1[-1])+1:M*(di1[-1]+1)]) + C13d2* 1/(2*dl2[0])
        for k in range(1,M): jac[(dom2_co-M), (di1[-1]+0)*M+k] = - C13b2[k-1]*ans[-2] - C13c2[k-1]* (-3*ans[M*di1[-1]]+4*ans[M*(di1[-1]+1)]-1*ans[M*(di1[-1]+2)]) / (2*dl2[0])
        jac[(dom2_co-M), -2] = C13a2 + np.sum(ans[M*(di1[-1])+1:M*(di1[-1]+1)]*C13b2)

        jac[(dom2_co-M), (di1[-1]+di2[-1]+0)*M] = 3/(2*dl3[0]) * np.sum(C13c3*ans[M*(di1[-1]+di2[-1])+1:M*(di1[-1]+di2[-1]+1)]) + C13d3* 3/(2*dl3[0]) - C13a3*ans[-1]
        jac[(dom2_co-M), (di1[-1]+di2[-1]+1)*M] =-4/(2*dl3[0]) * np.sum(C13c3*ans[M*(di1[-1]+di2[-1])+1:M*(di1[-1]+di2[-1]+1)]) + C13d3*-4/(2*dl3[0])
        jac[(dom2_co-M), (di1[-1]+di2[-1]+2)*M] = 1/(2*dl3[0]) * np.sum(C13c3*ans[M*(di1[-1]+di2[-1])+1:M*(di1[-1]+di2[-1]+1)]) + C13d3* 1/(2*dl3[0])
        for k in range(1,M): jac[(dom2_co-M), (di1[-1]+di2[-1]+0)*M+k] = - C13b3[k-1]*ans[-1] - C13c3[k-1]* (-3*ans[M*(di1[-1]+di2[-1])]+4*ans[M*(di1[-1]+di2[-1]+1)]-1*ans[M*(di1[-1]+di2[-1]+2)]) / (2*dl3[0])
        jac[(dom2_co-M), -1] = C13a3 + np.sum(ans[M*(di1[-1]+di2[-1])+1:M*(di1[-1]+di2[-1]+1)]*C13b3)

        #depth-perturbed values 
        for j in range(1,M):
            #dom 1
            jac[(dom2_co-M)+j, (di1[-1]-3)*M] = C12b1[j-1] * ans[M*(di1[-1]-1)+j] * 1/ (2*dl1[-1]) + np.sum(C12c1[j-1]*ans[M*(di1[-1]-1)+1:M*di1[-1]]) * 1/ (2*dl1[-1]) + C12g1[j-1]*1/(2*dl1[-1]) * ans[M*(di1[-1]-1)]
            jac[(dom2_co-M)+j, (di1[-1]-2)*M] = C12b1[j-1] * ans[M*(di1[-1]-1)+j] *-4/ (2*dl1[-1]) + np.sum(C12c1[j-1]*ans[M*(di1[-1]-1)+1:M*di1[-1]]) *-4/ (2*dl1[-1]) - C12g1[j-1]*4/(2*dl1[-1]) * ans[M*(di1[-1]-1)]
            jac[(dom2_co-M)+j, (dom2_co-M)] = C12b1[j-1] * ans[M*(di1[-1]-1)+j] * 3/ (2*dl1[-1]) + np.sum(C12c1[j-1]*ans[M*(di1[-1]-1)+1:M*di1[-1]]) * 3/ (2*dl1[-1]) \
                                            + C12f1[j-1]*ans[-3] + C12g1[j-1]*(ans[M*(di1[-1]-3)]-4*ans[M*(di1[-1]-2)]+3*ans[M*(di1[-1]-1)])/(2*dl1[-1]) + C12g1[j-1]*3/(2*dl1[-1]) * ans[M*(di1[-1]-1)]
            
            jac[(dom2_co-M)+j, (di1[-1]-3)*M+j] = C12a1 * 1/(2*dl1[-1]) 
            jac[(dom2_co-M)+j, (di1[-1]-2)*M+j] = C12a1 *-4/(2*dl1[-1]) 
            jac[(dom2_co-M)+j, (dom2_co-M)+j] = C12a1 * 3/(2*dl1[-1])  + C12b1[j-1] * (ans[M*(di1[-1]-3)]-4*ans[M*(di1[-1]-2)]+3*ans[M*(di1[-1]-1)])/(2*dl1[-1]) + C12d1[j-1]*ans[-3]
            for k in range(1,M):  jac[(dom2_co-M)+j, (dom2_co-M)+k]= jac[(dom2_co-M)+j, (dom2_co-M)+k] +  C12c1[j-1,k-1] * (ans[M*(di1[-1]-3)]-4*ans[M*(di1[-1]-2)]+3*ans[M*(di1[-1]-1)])/(2*dl1[-1]) + C12e1[j-1,k-1]*ans[-3]
            
            #dom2
            jac[(dom2_co-M)+j, (di1[-1]+2)*M] = C12b2[j-1] * ans[M*(di1[-1])+j] * 1/ (2*dl2[0]) + np.sum(C12c2[j-1]*ans[M*(di1[-1])+1:M*(di1[-1]+1)]) * 1/ (2*dl2[0]) + C12g2[j-1]*1/(2*dl2[0])*ans[M*(di1[-1])]
            jac[(dom2_co-M)+j, (di1[-1]+1)*M] = C12b2[j-1] * ans[M*(di1[-1])+j] *-4/ (2*dl2[0]) + np.sum(C12c2[j-1]*ans[M*(di1[-1])+1:M*(di1[-1]+1)]) *-4/ (2*dl2[0]) - C12g2[j-1]*4/(2*dl2[0])*ans[M*(di1[-1])]
            jac[(dom2_co-M)+j, (di1[-1]+0)*M] = C12b2[j-1] * ans[M*(di1[-1])+j] * 3/ (2*dl2[0]) + np.sum(C12c2[j-1]*ans[M*(di1[-1])+1:M*(di1[-1]+1)]) * 3/ (2*dl2[0]) \
                                            - C12f2[j-1]*ans[-2] - C12g2[j-1]*(-ans[M*(di1[-1]+2)]+4*ans[M*(di1[-1]+1)]-3*ans[M*(di1[-1])])/(2*dl2[0]) + C12g2[j-1]*3/(2*dl2[0])*ans[M*(di1[-1])]
            
            jac[(dom2_co-M)+j, (di1[-1]+2)*M+j] = C12a2 * 1/(2*dl2[0]) 
            jac[(dom2_co-M)+j, (di1[-1]+1)*M+j] = C12a2 *-4/(2*dl2[0]) 
            jac[(dom2_co-M)+j, (di1[-1]+0)*M+j] = C12a2 * 3/(2*dl2[0])  + C12b2[j-1] * (ans[M*(di1[-1]+2)]-4*ans[M*(di1[-1]+1)]+3*ans[M*(di1[-1])])/(2*dl2[0]) - C12d2[j-1]*ans[-2]
            for k in range(1,M):  jac[(dom2_co-M)+j, (di1[-1]+0)*M+k]= jac[(dom2_co-M)+j, (di1[-1]+0)*M+k] +  C12c2[j-1,k-1] * (ans[M*(di1[-1]+2)]-4*ans[M*(di1[-1]+1)]+3*ans[M*(di1[-1])])/(2*dl2[0]) - C12e2[j-1,k-1]*ans[-2]

            #dom 3
            jac[(dom2_co-M)+j, (di1[-1]+di2[-1]+2)*M] = C12b3[j-1] * ans[M*(di1[-1]+di2[-1])+j] * 1/ (2*dl3[0]) + np.sum(C12c3[j-1]*ans[M*(di1[-1]+di2[-1])+1:M*(di1[-1]+di2[-1]+1)]) * 1/ (2*dl3[0]) + C12g3[j-1]*1/(2*dl3[0])*ans[M*(di1[-1]+di2[-1])]
            jac[(dom2_co-M)+j, (di1[-1]+di2[-1]+1)*M] = C12b3[j-1] * ans[M*(di1[-1]+di2[-1])+j] *-4/ (2*dl3[0]) + np.sum(C12c3[j-1]*ans[M*(di1[-1]+di2[-1])+1:M*(di1[-1]+di2[-1]+1)]) *-4/ (2*dl3[0]) - C12g3[j-1]*4/(2*dl3[0])*ans[M*(di1[-1]+di2[-1])]
            jac[(dom2_co-M)+j, (di1[-1]+di2[-1]+0)*M] = C12b3[j-1] * ans[M*(di1[-1]+di2[-1])+j] * 3/ (2*dl3[0]) + np.sum(C12c3[j-1]*ans[M*(di1[-1]+di2[-1])+1:M*(di1[-1]+di2[-1]+1)]) * 3/ (2*dl3[0]) \
                                            - C12f3[j-1]*ans[-1] - C12g3[j-1]*(-ans[M*(di1[-1]+di2[-1]+2)]+4*ans[M*(di1[-1]+di2[-1]+1)]-3*ans[M*(di1[-1]+di2[-1])])/(2*dl3[0]) + C12g3[j-1]*3/(2*dl3[0])*ans[M*(di1[-1]+di2[-1])]
            
            jac[(dom2_co-M)+j, (di1[-1]+di2[-1]+2)*M+j] = C12a3 * 1/(2*dl3[0]) 
            jac[(dom2_co-M)+j, (di1[-1]+di2[-1]+1)*M+j] = C12a3 *-4/(2*dl3[0]) 
            jac[(dom2_co-M)+j, (di1[-1]+di2[-1]+0)*M+j] = C12a3 * 3/(2*dl3[0])  + C12b3[j-1] * (ans[M*(di1[-1]+di2[-1]+2)]-4*ans[M*(di1[-1]+di2[-1]+1)]+3*ans[M*(di1[-1]+di2[-1])])/(2*dl3[0]) - C12d3[j-1]*ans[-1]
            for k in range(1,M):  jac[(dom2_co-M)+j, (di1[-1]+di2[-1]+0)*M+k]= jac[(dom2_co-M)+j, (di1[-1]+di2[-1]+0)*M+k] +  C12c3[j-1,k-1] * (ans[M*(di1[-1]+di2[-1]+2)]-4*ans[M*(di1[-1]+di2[-1]+1)]+3*ans[M*(di1[-1]+di2[-1])])/(2*dl3[0]) - C12e3[j-1,k-1]*ans[-1]

        #boundary conditions imposed at the beginning of domain 2
        #points equal
        for k in range(M):
            jac[dom2_co+k, (dom2_co-M)+k] = -1
            jac[dom2_co+k, dom2_co+k] = 1 
            
        #boundary conditions imposed at the beginning of domain 3
        #points equal
        for k in range(M):
            jac[dom3_co+k, (dom2_co-M)+k] = -1
            jac[dom3_co+k, dom3_co+k] = 1 
            
        
        # =============================================================================
        #         Inner boundaries in domains
        # =============================================================================
        #domain 1
        for d in range(1,len(nxn1)):   #this for loop has to be replaced in a later stage 
            #derivatives equal
            jac[(di1[d]-1)*M+np.arange(M), (di1[d]-3)*M+np.arange(M)] =  1/(2*dl1[di1[d]-1])
            jac[(di1[d]-1)*M+np.arange(M), (di1[d]-2)*M+np.arange(M)] = - 4/(2*dl1[di1[d]-1])
            jac[(di1[d]-1)*M+np.arange(M), (di1[d]-1)*M+np.arange(M)] =  3/(2*dl1[di1[d]-1]) 
            
            jac[(di1[d]-1)*M+np.arange(M), di1[d]*M+np.arange(M)] =  3/(2*dl1[di1[d]]) 
            jac[(di1[d]-1)*M+np.arange(M), (di1[d]+1)*M+np.arange(M)] =  - 4/(2*dl1[di1[d]])
            jac[(di1[d]-1)*M+np.arange(M), (di1[d]+2)*M+np.arange(M)] =  1/(2*dl1[di1[d]])
      
        #points equal
        jac[bnd1, bnd1-M] = 1                   
        jac[bnd1, bnd1] = -1            
        
        #domain 2
        for d in range(1,len(nxn2)):   #this for loop has to be replaced in a later stage 
            #derivatives equal
            jac[(di1[-1]+di2[d]-1)*M+np.arange(M), (di1[-1]+di2[d]-3)*M+np.arange(M)] =  1/(2*dl2[di2[d]-1])
            jac[(di1[-1]+di2[d]-1)*M+np.arange(M), (di1[-1]+di2[d]-2)*M+np.arange(M)] = - 4/(2*dl2[di2[d]-1])
            jac[(di1[-1]+di2[d]-1)*M+np.arange(M), (di1[-1]+di2[d]-1)*M+np.arange(M)] =  3/(2*dl2[di2[d]-1]) 
            
            jac[(di1[-1]+di2[d]-1)*M+np.arange(M), (di1[-1]+di2[d])*M+np.arange(M)] =  3/(2*dl2[di2[d]]) 
            jac[(di1[-1]+di2[d]-1)*M+np.arange(M), (di1[-1]+di2[d]+1)*M+np.arange(M)] =  - 4/(2*dl2[di2[d]])
            jac[(di1[-1]+di2[d]-1)*M+np.arange(M), (di1[-1]+di2[d]+2)*M+np.arange(M)] =  1/(2*dl2[di2[d]])
      
        #points equal
        jac[bnd2, bnd2-M] = 1                   
        jac[bnd2, bnd2] = -1           
        
        #domain 3
        for d in range(1,len(nxn3)):   #this for loop has to be replaced in a later stage 
            #derivatives equal
            jac[(di1[-1]+di2[-1]+di3[d]-1)*M+np.arange(M), (di1[-1]+di2[-1]+di3[d]-3)*M+np.arange(M)] =  1/(2*dl3[di3[d]-1])
            jac[(di1[-1]+di2[-1]+di3[d]-1)*M+np.arange(M), (di1[-1]+di2[-1]+di3[d]-2)*M+np.arange(M)] = - 4/(2*dl3[di3[d]-1])
            jac[(di1[-1]+di2[-1]+di3[d]-1)*M+np.arange(M), (di1[-1]+di2[-1]+di3[d]-1)*M+np.arange(M)] =  3/(2*dl3[di3[d]-1]) 
            
            jac[(di1[-1]+di2[-1]+di3[d]-1)*M+np.arange(M), (di1[-1]+di2[-1]+di3[d])*M+np.arange(M)] =  3/(2*dl3[di3[d]]) 
            jac[(di1[-1]+di2[-1]+di3[d]-1)*M+np.arange(M), (di1[-1]+di2[-1]+di3[d]+1)*M+np.arange(M)] =  - 4/(2*dl3[di3[d]])
            jac[(di1[-1]+di2[-1]+di3[d]-1)*M+np.arange(M), (di1[-1]+di2[-1]+di3[d]+2)*M+np.arange(M)] =  1/(2*dl3[di3[d]])
      
        #points equal
        jac[bnd3, bnd3-M] = 1                   
        jac[bnd3, bnd3] = -1           
           
        #sea boundary - 2 times
        #domain 2
        jac[M*(di1[-1]+di2[-1]-1),M*(di1[-1]+di2[-1]-1)] = jac[M*(di1[-1]+di2[-1]-1),M*(di1[-1]+di2[-1]-1)] - 1
        jac[np.arange(M*(di1[-1]+di2[-1]-1)+1,M*(di1[-1]+di2[-1])),np.arange(M*(di1[-1]+di2[-1]-1)+1,M*(di1[-1]+di2[-1]))] =  1
        #domain 3
        jac[M*(di1[-1]+di2[-1]+di3[-1]-1),M*(di1[-1]+di2[-1]+di3[-1]-1)] = jac[M*(di1[-1]+di2[-1]+di3[-1]-1),M*(di1[-1]+di2[-1]+di3[-1]-1)] - 1
        jac[np.arange(M*(di1[-1]+di2[-1]+di3[-1]-1)+1,M*(di1[-1]+di2[-1]+di3[-1])),np.arange(M*(di1[-1]+di2[-1]+di3[-1]-1)+1,M*(di1[-1]+di2[-1]+di3[-1]))] =  1
        
        # =============================================================================
        #         river discharge distribution
        # =============================================================================
        #main river is main river
        jac[-3,-3] = 1
        #mass conservation
        jac[-2,-3] = 1
        jac[-2,-2] = -1
        jac[-2,-1] = -1
        #water level equal
        jac[-1,M*di1[-1]] = C14b2
        jac[-1,-2] = C14c2
        jac[-1,M*(di1[-1]+di2[-1])] = - C14b3
        jac[-1,-1] = - C14c3
        
        return jac

       
    def sol_build(ans, terms): 
    
        so = np.zeros((di1[-1]+di2[-1]+di3[-1])*M+3)
        
        #river boundary - everything is zero
        so[0] = ans[0]-sri/soc
        so[1:M] = ans[1:M]
        
        #contribution to solution vector due to average salt balance
        so[x_m1] = so[x_m1] + (C10a1[x1]*(ans[xp_m1]-ans[xm_m1])/(2*dl1[x1]) + C10b1[x1]*(ans[xp_m1] - 2*ans[x_m1] + ans[xm_m1])/(dl1[x1]**2) 
                            + (ans[xp_m1]-ans[xm_m1])/(2*dl1[x1]) * np.sum([C10c1[x1,n-1]*ans[x_m1+n] for n in range(1,M)],0) 
                            + (ans[xp_m1] - 2*ans[x_m1] + ans[xm_m1])/(dl1[x1]**2) * np.sum([C10d1[x1,n-1]*ans[x_m1+n] for n in range(1,M)],0) 
                            + np.sum([C10e1[x1,n-1]*(ans[xp_m1+n]-ans[xm_m1+n])/(2*dl1[x1]) for n in range(1,M)],0) *ans[-3] 
                            + (ans[xp_m1]-ans[xm_m1])/(2*dl1[x1]) * np.sum([C10f1[x1,n-1]*(ans[xp_m1+n]-ans[xm_m1+n])/(2*dl1[x1]) for n in range(1,M)],0)
                            + np.sum([C10g1[x1,n-1]*ans[x_m1+n] for n in range(1,M)],0) + C10h1[x1]*ans[-3]*(ans[xp_m1]-ans[xm_m1])/(2*dl1[x1]) )
        so[dom2_co+x_m2] = so[dom2_co+x_m2] + (C10a2[x2]*(ans[dom2_co+xp_m2]-ans[dom2_co+xm_m2])/(2*dl2[x2]) + C10b2[x2]*(ans[dom2_co+xp_m2] - 2*ans[dom2_co+x_m2] + ans[dom2_co+xm_m2])/(dl2[x2]**2) 
                            + (ans[dom2_co+xp_m2]-ans[dom2_co+xm_m2])/(2*dl2[x2]) * np.sum([C10c2[x2,n-1]*ans[dom2_co+x_m2+n] for n in range(1,M)],0) 
                            + (ans[dom2_co+xp_m2] - 2*ans[dom2_co+x_m2] + ans[dom2_co+xm_m2])/(dl2[x2]**2) * np.sum([C10d2[x2,n-1]*ans[dom2_co+x_m2+n] for n in range(1,M)],0) 
                            + np.sum([C10e2[x2,n-1]*(ans[dom2_co+xp_m2+n]-ans[dom2_co+xm_m2+n])/(2*dl2[x2]) for n in range(1,M)],0) *ans[-2]
                            + (ans[dom2_co+xp_m2]-ans[dom2_co+xm_m2])/(2*dl2[x2]) * np.sum([C10f2[x2,n-1]*(ans[dom2_co+xp_m2+n]-ans[dom2_co+xm_m2+n])/(2*dl2[x2]) for n in range(1,M)],0)
                            + np.sum([C10g2[x2,n-1]*ans[dom2_co+x_m2+n] for n in range(1,M)],0) + C10h2[x2]*ans[-2]*(ans[dom2_co+xp_m2]-ans[dom2_co+xm_m2])/(2*dl2[x2]))
        so[dom3_co+x_m3] = so[dom3_co+x_m3] + (C10a3[x3]*(ans[dom3_co+xp_m3]-ans[dom3_co+xm_m3])/(2*dl3[x3]) 
                            + C10b3[x3]*(ans[dom3_co+xp_m3] - 2*ans[dom3_co+x_m3] + ans[dom3_co+xm_m3])/(dl3[x3]**2) 
                            + (ans[dom3_co+xp_m3]-ans[dom3_co+xm_m3])/(2*dl3[x3]) * np.sum([C10c3[x3,n-1]*ans[dom3_co+x_m3+n] for n in range(1,M)],0) 
                            + (ans[dom3_co+xp_m3] - 2*ans[dom3_co+x_m3] + ans[dom3_co+xm_m3])/(dl3[x3]**2) * np.sum([C10d3[x3,n-1]*ans[dom3_co+x_m3+n] for n in range(1,M)],0) 
                            + np.sum([C10e3[x3,n-1]*(ans[dom3_co+xp_m3+n]-ans[dom3_co+xm_m3+n])/(2*dl3[x3]) for n in range(1,M)],0) *ans[-1]
                            + (ans[dom3_co+xp_m3]-ans[dom3_co+xm_m3])/(2*dl3[x3]) * np.sum([C10f3[x3,n-1]*(ans[dom3_co+xp_m3+n]-ans[dom3_co+xm_m3+n])/(2*dl3[x3]) for n in range(1,M)],0)
                            + np.sum([C10g3[x3,n-1]*ans[dom3_co+x_m3+n] for n in range(1,M)],0) + C10h3[x3]*ans[-1]*(ans[dom3_co+xp_m3]-ans[dom3_co+xm_m3])/(2*dl3[x3])  )
        
        if 1 in terms : #contribution to solution vector due to term T1
            so[xr_mj1] = so[xr_mj1] + C1a1[xr1] * ans[-3] * ((ans[xrp_mj1]-ans[xrm_mj1])/(2*dl1[xr1]))
            so[dom2_co+xr_mj2] = so[dom2_co+xr_mj2] + C1a2[xr2] * ans[-2] * ((ans[dom2_co+xrp_mj2]-ans[dom2_co+xrm_mj2])/(2*dl2[xr2]))
            so[dom3_co+xr_mj3] = so[dom3_co+xr_mj3] + C1a3[xr3] * ans[-1] * ((ans[dom3_co+xrp_mj3]-ans[dom3_co+xrm_mj3])/(2*dl3[xr3]))

        if 2 in terms: #contribution to solution vector due to term T2
            so[xr_mj1] = so[xr_mj1] + (C2a1[xr1,jl1]*ans[-3]*(ans[xrp_mj1]-ans[xrm_mj1])/(2*dl1[xr1])
                                    + C2b1[xr1,jl1]*((ans[xrp_mj1]-ans[xrm_mj1])/(2*dl1[xr1]))*((ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1]))
                                    +np.sum([C2c1[xr1,jl1,n-1]* ans[-3] * (ans[xrp_m1+n]-ans[xrm_m1+n]) for n in range(1,M)],0)/(2*dl1[xr1])
                                    +np.sum([C2d1[xr1,jl1,n-1] * (ans[xrp_m1+n]-ans[xrm_m1+n]) for n in range(1,M)],0)/(2*dl1[xr1])
                                    *(ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1])
                                     )
            so[dom2_co+xr_mj2] = so[dom2_co+xr_mj2] + (C2a2[xr2,jl2]* ans[-2]*(ans[dom2_co+xrp_mj2]-ans[dom2_co+xrm_mj2])/(2*dl2[xr2])
                                    + C2b2[xr2,jl2]*((ans[dom2_co+xrp_mj2]-ans[dom2_co+xrm_mj2])/(2*dl2[xr2]))*((ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2]))
                                    +np.sum([C2c2[xr2,jl2,n-1]* ans[-2] * (ans[dom2_co+xrp_m2+n]-ans[dom2_co+xrm_m2+n]) for n in range(1,M)],0)/(2*dl2[xr2])
                                    +np.sum([C2d2[xr2,jl2,n-1] * (ans[dom2_co+xrp_m2+n]-ans[dom2_co+xrm_m2+n]) for n in range(1,M)],0)/(2*dl2[xr2])
                                    *(ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2])
                                     )
            so[dom3_co+xr_mj3] = so[dom3_co+xr_mj3] + (C2a3[xr3,jl3]* ans[-1]*(ans[dom3_co+xrp_mj3]-ans[dom3_co+xrm_mj3])/(2*dl3[xr3])
                                    + C2b3[xr3,jl3]*((ans[dom3_co+xrp_mj3]-ans[dom3_co+xrm_mj3])/(2*dl3[xr3]))*((ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3]))
                                    +np.sum([C2c3[xr3,jl3,n-1]* ans[-1] * (ans[dom3_co+xrp_m3+n]-ans[dom3_co+xrm_m3+n]) for n in range(1,M)],0)/(2*dl3[xr3])
                                    +np.sum([C2d3[xr3,jl3,n-1] * (ans[dom3_co+xrp_m3+n]-ans[dom3_co+xrm_m3+n]) for n in range(1,M)],0)/(2*dl3[xr3])
                                    *(ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3])
                                     )

        if 3 in terms: #contribution to solution vector due to term T3
            so[xr_mj1] = so[xr_mj1] + C3a1[xr1,jl1]* ans[-3] * (ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1]) + C3b1[xr1,jl1] * ((ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1]))**2
            so[dom2_co+xr_mj2] = so[dom2_co+xr_mj2] + C3a2[xr2,jl2]* ans[-2] * (ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2]) + C3b2[xr2,jl2] * ((ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2]))**2
            so[dom3_co+xr_mj3] = (so[dom3_co+xr_mj3] + C3a3[xr3,jl3]* ans[-1] * (ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3]) 
                                              + C3b3[xr3,jl3] * ((ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3]))**2 )

        if 5 in terms: #contribution to jacobian due to term T5
            so[xr_mj1] = so[xr_mj1] + (C5a1[xr1,jl1]*(ans[xrp_m1] - 2*ans[xr_m1] + ans[xrm_m1])/(dl1[xr1]**2)*ans[xr_mj1] 
                                     + C5b1[xr1,jl1]*(ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1]) * ans[xr_mj1]  
                                       +np.sum([C5c1[xr1,jl1,n-1]*ans[xr_m1+n] for n in range(1,M)],0) * (ans[xrp_m1] - 2*ans[xr_m1] + ans[xrm_m1])/(dl1[xr1]**2) 
                                       +np.sum([C5d1[xr1,jl1,n-1]*ans[xr_m1+n] for n in range(1,M)],0) * (ans[xrp_m1]-ans[xrm_m1])/(2*dl1[xr1])
                                       + C5e1[xr1,jl1]*ans[xr_mj1] + np.sum([C5f1[xr1,jl1,n-1]*ans[xr_m1+n] for n in range(1,M)],0))
            so[dom2_co+xr_mj2] = so[dom2_co+xr_mj2] + (C5a2[xr2,jl2]*(ans[dom2_co+xrp_m2] - 2*ans[dom2_co+xr_m2] + ans[dom2_co+xrm_m2])/(dl2[xr2]**2)*ans[dom2_co+xr_mj2] 
                                     + C5b2[xr2,jl2]*(ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2]) * ans[dom2_co+xr_mj2]  
                                       +np.sum([C5c2[xr2,jl2,n-1]*ans[dom2_co+xr_m2+n] for n in range(1,M)],0) * (ans[dom2_co+xrp_m2] - 2*ans[dom2_co+xr_m2] + ans[dom2_co+xrm_m2])/(dl2[xr2]**2) 
                                       +np.sum([C5d2[xr2,jl2,n-1]*ans[dom2_co+xr_m2+n] for n in range(1,M)],0) * (ans[dom2_co+xrp_m2]-ans[dom2_co+xrm_m2])/(2*dl2[xr2])
                                       + C5e2[xr2,jl2]*ans[dom2_co+xr_mj2] + np.sum([C5f2[xr2,jl2,n-1]*ans[dom2_co+xr_m2+n] for n in range(1,M)],0))
            so[dom3_co+xr_mj3] = so[dom3_co+xr_mj3] + (C5a3[xr3,jl3]*(ans[dom3_co+xrp_m3] - 2*ans[dom3_co+xr_m3] + ans[dom3_co+xrm_m3])/(dl3[xr3]**2)*ans[dom3_co+xr_mj3] 
                                     + C5b3[xr3,jl3]*(ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3]) * ans[dom3_co+xr_mj3]  
                                       +np.sum([C5c3[xr3,jl3,n-1]*ans[dom3_co+xr_m3+n] for n in range(1,M)],0) * (ans[dom3_co+xrp_m3] - 2*ans[dom3_co+xr_m3] + ans[dom3_co+xrm_m3])/(dl3[xr3]**2) 
                                       +np.sum([C5d3[xr3,jl3,n-1]*ans[dom3_co+xr_m3+n] for n in range(1,M)],0) * (ans[dom3_co+xrp_m3]-ans[dom3_co+xrm_m3])/(2*dl3[xr3])
                                       + C5e3[xr3,jl3]*ans[dom3_co+xr_mj3] + np.sum([C5f3[xr3,jl3,n-1]*ans[dom3_co+xr_m3+n] for n in range(1,M)],0))

        if 6 in terms: #contribution to solution vector due to term T6
            so[xr_mj1] = so[xr_mj1] + C6a1[xr1,jl1]*ans[xr_mj1]
            so[dom2_co+xr_mj2] = so[dom2_co+xr_mj2] + C6a2[xr2,jl2]*ans[dom2_co+xr_mj2]
            so[dom3_co+xr_mj3] = so[dom3_co+xr_mj3] + C6a3[xr3,jl3]*ans[dom3_co+xr_mj3]

        if 7 in terms: 
            so[xr_mj1] = so[xr_mj1] + C7a1[xr1]*(ans[xrp_mj1]-ans[xrm_mj1])/(2*dl1[xr1]) + C7b1[xr1]*(ans[xrp_mj1] - 2*ans[xr_mj1] + ans[xrm_mj1])/(dl1[xr1]**2) 
            so[dom2_co+xr_mj2] = so[dom2_co+xr_mj2] + C7a2[xr2]*(ans[dom2_co+xrp_mj2]-ans[dom2_co+xrm_mj2])/(2*dl2[xr2]) + C7b2[xr2]*(ans[dom2_co+xrp_mj2] - 2*ans[dom2_co+xr_mj2] + ans[dom2_co+xrm_mj2])/(dl2[xr2]**2) 
            so[dom3_co+xr_mj3] = (so[dom3_co+xr_mj3] + C7a3[xr3]*(ans[dom3_co+xrp_mj3]-ans[dom3_co+xrm_mj3])/(2*dl3[xr3])
                                              + C7b3[xr3]*(ans[dom3_co+xrp_mj3] - 2*ans[dom3_co+xr_mj3] + ans[dom3_co+xrm_mj3])/(dl3[xr3]**2) )
          
            
        # =============================================================================
        #         #branching point
        # =============================================================================
        
        #variables 
        s_31,s_21,s_11,s01 = ans[M*(di1[-1]-4):M*(di1[-1]-3)], ans[M*(di1[-1]-3):M*(di1[-1]-2)] , ans[M*(di1[-1]-2):M*(di1[-1]-1)] , ans[M*(di1[-1]-1):M*di1[-1]] 
        s02, s12, s22, s32 = ans[M*(di1[-1]):M*(di1[-1]+1)] , ans[M*(di1[-1]+1):M*(di1[-1]+2)] , ans[M*(di1[-1]+2):M*(di1[-1]+3)], ans[M*(di1[-1]+3):M*(di1[-1]+4)]
        s03, s13, s23, s33 = ans[M*(di1[-1]+di2[-1]):M*(di1[-1]+di2[-1]+1)] , ans[M*(di1[-1]+di2[-1]+1):M*(di1[-1]+di2[-1]+2)] , ans[M*(di1[-1]+di2[-1]+2):M*(di1[-1]+di2[-1]+3)], ans[M*(di1[-1]+di2[-1]+3):M*(di1[-1]+di2[-1]+4)]
        
        #domain 1: flux - or something else
        so[M*(di1[-1]-1)] = sol_branch_sb1(s01,s_11,s_21,s_31, s02,s12,s22,s32, s03,s13,s23,s33, ans[-3],ans[-2],ans[-1])
        for j in range(1,M): so[M*(di1[-1]-1)+j] = sol_branch_sn1(s01,s_11,s_21,s_31, s02,s12,s22,s32, s03,s13,s23,s33, ans[-3],ans[-2],ans[-1], j-1)
        #domain 2 - points equal with domain 1
        so[M*di1[-1]] = sol_branch_sb2(s01,s_11,s_21,s_31, s02,s12,s22,s32, s03,s13,s23,s33)
        for j in range(1,M): so[M*di1[-1]+j] = sol_branch_sn2(s01,s_11,s_21,s_31, s02,s12,s22,s32, s03,s13,s23,s33, j-1)
        #domain 3 - points equal with domain 1
        so[M*(di1[-1]+di2[-1])] = sol_branch_sb3(s01,s_11,s_21,s_31, s02,s12,s22,s32, s03,s13,s23,s33)
        for j in range(1,M): so[M*(di1[-1]+di2[-1])+j] = sol_branch_sn3(s01,s_11,s_21,s_31, s02,s12,s22,s32, s03,s13,s23,s33, j-1)   
            

        # =============================================================================
        #         inner boundary
        # =============================================================================
        #domain 1
        #inner boundary - this for loop may live on as a living fossil
        for d in range(1,len(nxn1)):
            for j in range(M):
                s_3, s_2, s_1 = ans[(di1[d]-3)*M:(di1[d]-2)*M], ans[(di1[d]-2)*M:(di1[d]-1)*M], ans[(di1[d]-1)*M:di1[d]*M]
                s0, s1, s2 = ans[di1[d]*M:(di1[d]+1)*M], ans[(di1[d]+1)*M:(di1[d]+2)*M], ans[(di1[d]+2)*M:(di1[d]+3)*M]
                so[(di1[d]-1)*M+j] =  sol_in2['dse'](s_3[j],s_2[j],s_1[j],s0[j],s1[j],s2[j],dl1[di1[d]-1],dl1[di1[d]])
                so[di1[d]*M+j] =  sol_in2['se'](s_1[j],s0[j])
                
        #domain 2
        #inner boundary - this for loop may live on as a living fossil
        for d in range(1,len(nxn2)):
            for j in range(M):
                s_3, s_2, s_1 = ans[(di1[-1]+di2[d]-3)*M:(di1[-1]+di2[d]-2)*M], ans[(di1[-1]+di2[d]-2)*M:(di1[-1]+di2[d]-1)*M], ans[(di1[-1]+di2[d]-1)*M:(di1[-1]+di2[d])*M]
                s0, s1, s2 = ans[(di1[-1]+di2[d])*M:(di1[-1]+di2[d]+1)*M], ans[(di1[-1]+di2[d]+1)*M:(di1[-1]+di2[d]+2)*M], ans[(di1[-1]+di2[d]+2)*M:(di1[-1]+di2[d]+3)*M]
                so[(di1[-1]+di2[d]-1)*M+j] =  sol_in2['dse'](s_3[j],s_2[j],s_1[j],s0[j],s1[j],s2[j],dl2[di2[d]-1],dl2[di2[d]])
                so[(di1[-1]+di2[d])*M+j] =  sol_in2['se'](s_1[j],s0[j])
                
        #domain 3
        #inner boundary - this for loop may live on as a living fossil
        for d in range(1,len(nxn3)):
            for j in range(M):
                s_3, s_2, s_1 = ans[(di1[-1]+di2[-1]+di3[d]-3)*M:(di1[-1]+di2[-1]+di3[d]-2)*M], ans[(di1[-1]+di2[-1]+di3[d]-2)*M:(di1[-1]+di2[-1]+di3[d]-1)*M], ans[(di1[-1]+di2[-1]+di3[d]-1)*M:(di1[-1]+di2[-1]+di3[d])*M]
                s0, s1, s2 = ans[(di1[-1]+di2[-1]+di3[d])*M:(di1[-1]+di2[-1]+di3[d]+1)*M], ans[(di1[-1]+di2[-1]+di3[d]+1)*M:(di1[-1]+di2[-1]+di3[d]+2)*M], ans[(di1[-1]+di2[-1]+di3[d]+2)*M:(di1[-1]+di2[-1]+di3[d]+3)*M]
                so[(di1[-1]+di2[-1]+di3[d]-1)*M+j] =  sol_in2['dse'](s_3[j],s_2[j],s_1[j],s0[j],s1[j],s2[j],dl3[di3[d]-1],dl3[di3[d]])
                so[(di1[-1]+di2[-1]+di3[d])*M+j] =  sol_in2['se'](s_1[j],s0[j])

                
        #sea boundary
        so[M*(di1[-1]+di2[-1]-1)] = 1-ans[M*(di1[-1]+di2[-1]-1)]
        so[M*(di1[-1]+di2[-1]-1)+1:M*(di1[-1]+di2[-1])] = ans[M*(di1[-1]+di2[-1]-1)+1:M*(di1[-1]+di2[-1])]
        
        so[M*(di1[-1]+di2[-1]+di3[-1]-1)] = 1-ans[M*(di1[-1]+di2[-1]+di3[-1]-1)]
        so[M*(di1[-1]+di2[-1]+di3[-1]-1)+1:M*(di1[-1]+di2[-1]+di3[-1])] = ans[M*(di1[-1]+di2[-1]+di3[-1]-1)+1:M*(di1[-1]+di2[-1]+di3[-1])]

        # =============================================================================
        # river discharge distribution
        # =============================================================================
        #main river is main river
        so[-3] = ans[-3]-Q1
        #mass conservation
        so[-2] = ans[-3]-ans[-2]-ans[-1]
        #water level equal
        so[-1] = C14a2 + C14b2*ans[M*di1[-1]] + C14c2*ans[-2] - C14a3 - C14b3*ans[M*(di1[-1]+di2[-1])] - C14c3*ans[-1]
        
        return so
    
    '''
    #initialise the first run - done from outside
    init = np.zeros((di1[-1]+di2[-1]+di3[-1])*M+3)
    init[-3:] =[Q1,Q1/2,Q1/2]
    #init=init.flatten()

    #first iteration step of the initialisation
    jaco, solu = jac_build(init,[3,6,7]) , sol_build(init,[3,6,7])    
    sss_n =init - sp.sparse.linalg.spsolve(sp.sparse.csc_matrix(jaco),solu)  # this is faster then np.linalg.solve (at least for a sufficiently large matrix)

    sss, ti=init,1
    
    # do the rest of the iterations
    while np.max(np.abs(sss-sss_n))>10e-3: #check whether the algoritm has converged
        sss = sss_n.copy()#update
        jaco, solu = jac_build(sss,[3,6,7]) , sol_build(sss,[3,6,7])
        sss_n =sss - sp.sparse.linalg.spsolve(sp.sparse.csc_matrix(jaco),solu)  # this is faster then np.linalg.solve (at least for a sufficiently large matrix)

        ti = 1+ti
        if ti>20:
            print('ERROR: The initialisation failed')
            break
        
    init = sss_n #now we have a initialisation that is already almost correct for most river discharges
    '''
    #do the first time step
    jaco, solu = jac_build(init,term) , sol_build(init,term)
    sss_n =init - sp.sparse.linalg.spsolve(sp.sparse.csc_matrix(jaco),solu)  # this is faster then np.linalg.solve (at least for a sufficiently large matrix)

    t=1
    print('That was iteration step ', t)
    sss=init
    # do the rest of the iterations
    while np.max(np.abs(sss-sss_n))>10e-6: #check whether the algoritm has converged
        sss = sss_n.copy() #update

        jaco = jac_build(sss,term) 
        solu = sol_build(sss,term)
        sss_n =sss - sp.sparse.linalg.spsolve(sp.sparse.csc_matrix(jaco),solu)  # this is faster then np.linalg.solve (at least for a sufficiently large matrix)
        '''
        #plot the new sigma - gaat niet werken lijkt me
        fig,ax = plt.subplots(1,3,figsize=(10,5))
        ax[0].plot(px1[:,0],sss_n[:-3].reshape(int(len(sss_n[:-3])/M),M)[:,0][:len(px1)])
        ax[0].plot(px1[:,0],sss_n[:-3].reshape(int(len(sss_n[:-3])/M),M)[:,1][:len(px1)])
        ax[0].plot(px1[:,0],sss_n[:-3].reshape(int(len(sss_n[:-3])/M),M)[:,2][:len(px1)])
        
        ax[1].plot(np.concatenate([px1[-50:,0],px2[:,0]]),sss_n[:-3].reshape(int(len(sss_n[:-3])/M),M)[:,0][len(px1)-50:len(px1)+len(px2)])
        ax[1].plot(np.concatenate([px1[-50:,0],px2[:,0]]),sss_n[:-3].reshape(int(len(sss_n[:-3])/M),M)[:,1][len(px1)-50:len(px1)+len(px2)])
        ax[1].plot(np.concatenate([px1[-50:,0],px2[:,0]]),sss_n[:-3].reshape(int(len(sss_n[:-3])/M),M)[:,2][len(px1)-50:len(px1)+len(px2)])
        ax[2].plot(np.concatenate([px1[-50:,0],px3[:,0]]),np.concatenate([sss_n[:-3].reshape(int(len(sss_n[:-3])/M),M)[:,0][len(px1)-50:len(px1)],sss_n[:-3].reshape(int(len(sss_n[:-3])/M),M)[:,0][len(px1)+len(px2):]]))
        ax[2].plot(np.concatenate([px1[-50:,0],px3[:,0]]),np.concatenate([sss_n[:-3].reshape(int(len(sss_n[:-3])/M),M)[:,1][len(px1)-50:len(px1)],sss_n[:-3].reshape(int(len(sss_n[:-3])/M),M)[:,1][len(px1)+len(px2):]]))
        ax[2].plot(np.concatenate([px1[-50:,0],px3[:,0]]),np.concatenate([sss_n[:-3].reshape(int(len(sss_n[:-3])/M),M)[:,2][len(px1)-50:len(px1)],sss_n[:-3].reshape(int(len(sss_n[:-3])/M),M)[:,2][len(px1)+len(px2):]]))
        
        ax[0].vlines(np.sum(Ln1)/1000,-1,1,'grey')
        ax[1].vlines(np.sum(Ln1)/1000,-1,1,'grey')
        ax[2].vlines(np.sum(Ln1)/1000,-1,1,'grey')
        ax[0].set_ylim(-0.5,1) , ax[1].set_ylim(-0.5,1) , ax[2].set_ylim(-0.5,1)
        
        #ax[2].set_xlim(195,205) , ax[1].set_xlim(195,205)
        plt.tight_layout()
        plt.show()
        #'''
        t=1+t
        print('That was iteration step ', t)

        if t>=20: break
    
    if t<20:
        print('The algoritm has converged \n')  
    else:
        print('ERROR: no convergence')
        return ['NOCON']
    
    return sss

def split_opeen(list_ori):
    list_split=[]
    temp=[]
    for i in range(len(list_ori)-1):
        if list_ori[i+1]- list_ori[i] <2:
            temp.append(list_ori[i])
        else:
            temp.append(list_ori[i])
            list_split.append(temp)
            temp=[]
    temp.append(list_ori[-1])
    list_split.append(temp)
    return list_split

def inp_nans(lst): #works only for 1D list
    ind = np.where(np.isnan(lst)==True)[0] #find the indices with nans
    ind2 = split_opeen(ind) #split in seperate list with opeenvolgende nans
    lst_new = np.array(lst.copy()) #output
    #fix endpoints if required
    if ind2[0][0]==0:
        lst_new[ind2[0]] = lst[ind2[0][-1]+1]
        del ind2[0]
    if ind2[-1][-1]==len(lst)-1:
        lst_new[ind2[-1]] = lst[ind2[-1][0]-1]
        del ind2[-1]
    #fix rest
    for i in range(len(ind2)):
        lst_new[ind2[i]] = np.linspace(lst[ind2[i][0]-1],lst[ind2[i][-1]+1],len(ind2[i])+2)[1:-1]
    
    return lst_new



def plot_results(inp_p, inp_chn1, inp_chn2, inp_chn3, term, sss):
           
    (Ut1,Ut2,Ut3), soc, sri, Q1, u_w, d_w, (sf1,sf3,sf3), H, N, Lsc = inp_p #1. , 35. , 150. , 100000. , [2000.,1e9] , 10.
    Ln1, b01, bs1, dxn1 = inp_chn1
    Ln2, b02, bs2, dxn2 = inp_chn2
    Ln3, b03, bs3, dxn3 = inp_chn3
    if np.any(Ln1%dxn1!=0): print( 'WARNING: L/dx is not an integer')#check numerical parameters
    if np.any(Ln2%dxn2!=0): print( 'WARNING: L/dx is not an integer')#check numerical parameters
    if np.any(Ln3%dxn3!=0): print( 'WARNING: L/dx is not an integer')#check numerical parameters
    if len(Ln1) != len(bs1) or len(Ln1)!=len(dxn1) : print('ERROR: number of domains is not correct')
    if len(Ln2) != len(bs2) or len(Ln2)!=len(dxn2) : print('ERROR: number of domains is not correct')    
    if len(Ln3) != len(bs3) or len(Ln3)!=len(dxn3) : print('ERROR: number of domains is not correct')
    if u_w!=0: print('WARNING: this code is not suited yet to model wind, first check all terms')

    dln1 = dxn1/Lsc
    dln2 = dxn2/Lsc
    dln3 = dxn3/Lsc
    nxn1 = np.array(Ln1/dxn1+1,dtype=int)
    nxn2 = np.array(Ln2/dxn2+1,dtype=int)
    nxn3 = np.array(Ln3/dxn3+1,dtype=int)
    di1 = np.zeros(len(Ln1)+1,dtype=int) #starting indices of the domains
    di2 = np.zeros(len(Ln2)+1,dtype=int) #starting indices of the domains
    di3 = np.zeros(len(Ln3)+1,dtype=int) #starting indices of the domains
    for i in range(1,len(Ln1)):   di1[i] = np.sum(nxn1[:i])
    di1[-1] = np.sum(nxn1)
    for i in range(1,len(Ln2)):   di2[i] = np.sum(nxn2[:i])
    di2[-1] = np.sum(nxn2)
    for i in range(1,len(Ln3)):   di3[i] = np.sum(nxn3[:i])
    di3[-1] = np.sum(nxn3)
    
    dl1,dl2,dl3 = np.zeros(np.sum(nxn1)),np.zeros(np.sum(nxn2)),np.zeros(np.sum(nxn3))
    dl1[0:nxn1[0]],dl2[0:nxn2[0]],dl3[0:nxn3[0]] = dln1[0],dln2[0],dln3[0]
    for i in range(1,len(nxn1)): dl1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = dln1[i]
    for i in range(1,len(nxn2)): dl2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = dln2[i]
    for i in range(1,len(nxn3)): dl3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = dln3[i]
    
    M=N+1 
    
    # make H a list from here. H can not really vary in the channel network so making it lists 
    # is a bit overkill but lets do it anyway
    H1= H[0]+np.zeros(di1[-1])
    H2= H[1]+np.zeros(di2[-1])
    H3= H[2]+np.zeros(di3[-1])
    #del H
    
    #plotting parameters
    #build parameters: convergence length
    bn1, bn2, bn3 = np.zeros(len(Ln1)), np.zeros(len(Ln2)), np.zeros(len(Ln3))
    bn1[0] = 9e99 if bs1[0] == b01 else Ln1[0]/np.log(bs1[0]/b01)
    bn2[0] = 9e99 if bs2[0] == b02 else Ln2[0]/np.log(bs2[0]/b02)
    bn3[0] = 9e99 if bs3[0] == b03 else Ln3[0]/np.log(bs3[0]/b03)
    for i in range(1,len(Ln1)): bn1[i] = 9e99 if bs1[i] == bs1[i-1] else Ln1[i]/np.log(bs1[i]/bs1[i-1])
    for i in range(1,len(Ln2)): bn2[i] = 9e99 if bs2[i] == bs2[i-1] else Ln2[i]/np.log(bs2[i]/bs2[i-1])
    for i in range(1,len(Ln3)): bn3[i] = 9e99 if bs3[i] == bs3[i-1] else Ln3[i]/np.log(bs3[i]/bs3[i-1])
    #build width
    b1, bex1 = np.zeros(np.sum(nxn1)),np.zeros(np.sum(nxn1))
    b2, bex2 = np.zeros(np.sum(nxn2)),np.zeros(np.sum(nxn2))
    b3, bex3 = np.zeros(np.sum(nxn3)),np.zeros(np.sum(nxn3))
    b1[0:nxn1[0]] = b01 * np.exp(bn1[0]**(-1)*(np.linspace(-Ln1[0],0,nxn1[0])+Ln1[0]))
    b2[0:nxn2[0]] = b02 * np.exp(bn2[0]**(-1)*(np.linspace(-Ln2[0],0,nxn2[0])+Ln2[0]))
    b3[0:nxn3[0]] = b03 * np.exp(bn3[0]**(-1)*(np.linspace(-Ln3[0],0,nxn3[0])+Ln3[0]))
    bex1[0:nxn1[0]] = [bn1[0]]*nxn1[0]
    bex2[0:nxn2[0]] = [bn2[0]]*nxn2[0]
    bex3[0:nxn3[0]] = [bn3[0]]*nxn3[0]
    
    for i in range(1,len(nxn1)): bex1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = [bn1[i]]*nxn1[i]
    for i in range(1,len(nxn2)): bex2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = [bn2[i]]*nxn2[i]
    for i in range(1,len(nxn3)): bex3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = [bn3[i]]*nxn3[i]
    for i in range(1,len(nxn1)): b1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = b1[np.sum(nxn1[:i])-1] * np.exp(bn1[i]**(-1)*(np.linspace(-Ln1[i],0,nxn1[i])+Ln1[i]))
    for i in range(1,len(nxn2)): b2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = b2[np.sum(nxn2[:i])-1] * np.exp(bn2[i]**(-1)*(np.linspace(-Ln2[i],0,nxn2[i])+Ln2[i]))
    for i in range(1,len(nxn3)): b3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = b3[np.sum(nxn3[:i])-1] * np.exp(bn3[i]**(-1)*(np.linspace(-Ln3[i],0,nxn3[i])+Ln3[i]))
    
    #plotting parameters
    #vertical
    nz=51 #vertical step - only for plot
    pz1 =np.zeros((di1[-1],nz))
    for i in range(di1[-1]): pz1[i] = np.linspace(-H1[i],0,nz)
    pz2 =np.zeros((di2[-1],nz))
    for i in range(di2[-1]): pz2[i] = np.linspace(-H2[i],0,nz)
    pz3 =np.zeros((di3[-1],nz))
    for i in range(di3[-1]): pz3[i] = np.linspace(-H3[i],0,nz)

    #horizontal
    px1 = np.zeros((np.sum(nxn1),nz))
    px1[0:nxn1[0]] = -np.linspace(np.sum(Ln1[0:]), np.sum(Ln1[0+1:]), nxn1[0]).repeat(nz).reshape((di1[1],nz)) #here i can use the di list
    for i in range(1,len(nxn1)): px1[np.sum(nxn1[:i]):np.sum(nxn1[:i+1])] = -np.linspace(np.sum(Ln1[i:]), np.sum(Ln1[i+1:]), nxn1[i]).repeat(nz).reshape((nxn1[i],nz))
    px2 = np.zeros((np.sum(nxn2),nz))
    px2[0:nxn2[0]] = -np.linspace(np.sum(Ln2[0:]), np.sum(Ln2[0+1:]), nxn2[0]).repeat(nz).reshape((di2[1],nz)) #here i can use the di list
    for i in range(1,len(nxn2)): px2[np.sum(nxn2[:i]):np.sum(nxn2[:i+1])] = -np.linspace(np.sum(Ln2[i:]), np.sum(Ln2[i+1:]), nxn2[i]).repeat(nz).reshape((nxn2[i],nz))
    px3 = np.zeros((np.sum(nxn3),nz))
    px3[0:nxn3[0]] = -np.linspace(np.sum(Ln3[0:]), np.sum(Ln3[0+1:]), nxn3[0]).repeat(nz).reshape((di3[1],nz)) #here i can use the di list
    for i in range(1,len(nxn3)): px3[np.sum(nxn3[:i]):np.sum(nxn3[:i+1])] = -np.linspace(np.sum(Ln3[i:]), np.sum(Ln3[i+1:]), nxn3[i]).repeat(nz).reshape((nxn3[i],nz))

    px1, px2, px3 = px1+np.sum(Ln1) , px2+np.sum(Ln1)+np.sum(Ln2) , px3+np.sum(Ln1)+np.sum(Ln3)
    px1,px2,px3=px1/1000 , px2/1000, px3/1000


    #build parameters - adjusted
    #sf1,sf2,sf3 = 2*cv*1,2*cv*1,2*cv*1
    #Av1, Av2, Av3 = cv*7.5+np.zeros(di1[-1]), cv*7.5+np.zeros(di2[-1]), cv*7.5+np.zeros(di3[-1])  
    Kh1, Kh2, Kh3 = ch2*Ut1**2+np.zeros(di1[-1]), ch2*Ut2**2+np.zeros(di2[-1]), ch2*Ut3**2+np.zeros(di3[-1])
    Kh2[di2[-2]:], Kh3[di3[-2]:]= ch2*Ut2**2*b2[di2[-2]:]/b2[di2[-2]], ch2*Ut3**2*b3[di3[-2]:]/b3[di3[-2]]
    
    #old
    Av1, Av2, Av3 = cv*Ut1*H1, cv*Ut2*H2, cv*Ut3*H3
    sf1, sf2, sf3 = 2*cv*Ut1, 2*cv*Ut2, 2*cv*Ut3
    #Kh1, Kh2, Kh3 = ch*Ut1*b1, ch*Ut2*b2, ch*Ut3*b3
    

    bH_11, bH_12, bH_13 = 1/(H1*b1), 1/(H2*b2), 1/(H3*b3)
    Kv1, Kv2, Kv3 = Av1/Sc, Av2/Sc, Av3/Sc
    alf1, alf2, alf3 = g*Be*H1**3/(48*Av1), g*Be*H2**3/(48*Av2), g*Be*H3**3/(48*Av3)

    #lists of ks and ns and pis
    kkp = np.linspace(1,N,N)*np.pi #k*pi
    nnp = np.linspace(1,N,N)*np.pi #n*pi
    pkn = np.array([kkp]*N) + np.transpose([nnp]*N) #pi*(n+k)
    pnk = np.array([nnp]*N) - np.transpose([kkp]*N) #pi*(n-k)
    np.fill_diagonal(pkn,None),np.fill_diagonal(pnk,None)

    #coefficients
    #new partial slip coefficients
    #'''
    rr1, rr2, rr3 = Av1/(sf1*H1) , Av2/(sf2*H2) ,Av3/(sf3*H3)
    g11, g12, g13 = -1 + (1.5+3*rr1) / (1+ 3 *rr1), -1 + (1.5+3*rr2) / (1+ 3 *rr2), -1 + (1.5+3*rr3) / (1+ 3 *rr3)
    g21, g22, g23 =  -3 / (2+6*rr1) , -3 / (2+6*rr2), -3 / (2+6*rr3)
    g31, g32, g33 = (1+4*rr1) / (1+3*rr1) * (9+18*rr1) - 8 - 24*rr1, (1+4*rr2) / (1+3*rr2) * (9+18*rr2) - 8 - 24*rr2, (1+4*rr3) / (1+3*rr3) * (9+18*rr3) - 8 - 24*rr3
    g41, g42, g43 = -9 * (1+4*rr1) / (1+3*rr1), -9 * (1+4*rr2) / (1+3*rr2), -9 * (1+4*rr3) / (1+3*rr3)
    g51, g52, g53 = - 8 , -8, -8
    g61, g62, g63 = 4+4*rr1 -12*(0.5+rr1)**2/(1+3*rr1), 4+4*rr2 -12*(0.5+rr2)**2/(1+3*rr2), 4+4*rr3 -12*(0.5+rr3)**2/(1+3*rr3)
    g71, g72, g73 = 4 , 4, 4
    g81, g82, g83 = (3+6*rr1) / (1+3*rr1) , (3+6*rr2) / (1+3*rr2), (3+6*rr3) / (1+3*rr3)
    

    Q1,Q2,Q3 = sss[-3:]
    print('Q1 = ',np.round(Q1),', Q2=',np.round(Q2),', Q3= ', np.round(Q3) )
    
    #salt calculation
    sb1 = np.reshape(sss[:di1[-1]*M],(di1[-1],M))[:,0] *soc
    sn1 = np.reshape(sss[:di1[-1]*M],(di1[-1],M))[:,1:] *soc
    sb2 = np.reshape(sss[di1[-1]*M:(di1[-1]+di2[-1])*M],(di2[-1],M))[:,0] *soc
    sn2 = np.reshape(sss[di1[-1]*M:(di1[-1]+di2[-1])*M],(di2[-1],M))[:,1:] *soc
    sb3 = np.reshape(sss[(di1[-1]+di2[-1])*M:-3],(di3[-1],M))[:,0] *soc
    sn3 = np.reshape(sss[(di1[-1]+di2[-1])*M:-3],(di3[-1],M))[:,1:] *soc
    
    nz=51
    sigma=np.linspace(-1, 0,nz)
    sp1 = np.array([np.sum([sn1[i,n-1]*np.cos(np.pi*n*sigma) for n in range(1,M)],0) for i in range(di1[-1])])
    sp2 = np.array([np.sum([sn2[i,n-1]*np.cos(np.pi*n*sigma) for n in range(1,M)],0) for i in range(di2[-1])])
    sp3 = np.array([np.sum([sn3[i,n-1]*np.cos(np.pi*n*sigma) for n in range(1,M)],0) for i in range(di3[-1])])
    #stratification
    strat1 = (sp1[:,0]-sp1[:,-1])/soc#/sb1
    strat2 = (sp2[:,0]-sp2[:,-1])/soc#/sb2
    strat3 = (sp3[:,0]-sp3[:,-1])/soc#/sb3
    
    s1 = sp1+sb1[:,np.newaxis]
    s2 = sp2+sb2[:,np.newaxis]
    s3 = sp3+sb3[:,np.newaxis]

    #print('The salt intruison length is ',-px[np.where(s[:,0]>2)[0][0]]-Ln[-1]/1000,' km')
    print('Salinity at the junction is ', np.mean(s1[-1]))
    
    # =============================================================================
    # calculate flux    
    # =============================================================================
    #alf = g*Be*H**3/(48*cv*Ut*H)
    
    #channel 1
    sb1_x = np.concatenate([[0],(sb1[2:]-sb1[:-2])/(2*dl1[1:-1]*Lsc),[0]])     #derivative of sb
    #method 2: calculate flux directly from Fourier modes. Works better. 
    F1_1 = H1*b1*Q1*bH_11*sb1
    F2_1 = H1*b1*np.sum(sn1*(2*Q1*bH_11[:,np.newaxis]*g21[:,np.newaxis]*np.cos(nnp)/nnp**2 \
                          + alf1[:,np.newaxis]*sb1_x[:,np.newaxis]*(2*g41[:,np.newaxis]*np.cos(nnp)/nnp**2 + g51*((6*np.cos(nnp)-6)/nnp**4 - 3*np.cos(nnp)/nnp**2))),1)
    F3_1 = H1*b1*- Kh1*sb1_x
    #remove edge points
    F1_1[[0,-1]],F2_1[[0,-1]],F3_1[[0,-1]] = None , None , None
    F1_1[di1[1:-1]],F2_1[di1[1:-1]],F3_1[di1[1:-1]] = None, None, None
    F1_1[di1[1:-1]-1],F2_1[di1[1:-1]-1],F3_1[di1[1:-1]-1] = None, None, None
    

    #channel 2
    sb2_x = np.concatenate([[0],(sb2[2:]-sb2[:-2])/(2*dl2[1:-1]*Lsc),[0]])     #derivative of sb
    
    #method 2: calculate flux directly from Fourier modes. Works better. 
    F1_2 = H2*b2*Q2*bH_12*sb2
    F2_2 = H2*b2*np.sum(sn2*(2*Q2*bH_12[:,np.newaxis]*g22[:,np.newaxis]*np.cos(nnp)/nnp**2 \
                          + alf2[:,np.newaxis]*sb2_x[:,np.newaxis]*(2*g42[:,np.newaxis]*np.cos(nnp)/nnp**2 + g52*((6*np.cos(nnp)-6)/nnp**4 - 3*np.cos(nnp)/nnp**2))),1)
    F3_2 = H2*b2*- Kh2*sb2_x
    #remove boundary points
    F1_2[[0,-1]],F2_2[[0,-1]],F3_2[[0,-1]] = None , None , None
    F1_2[di2[1:-1]],F2_2[di2[1:-1]],F3_2[di2[1:-1]] = None, None, None
    F1_2[di2[1:-1]-1],F2_2[di2[1:-1]-1],F3_2[di2[1:-1]-1] = None, None, None

    #channel 3
    sb3_x = np.concatenate([[0],(sb3[2:]-sb3[:-2])/(2*dl3[1:-1]*Lsc),[0]])     #derivative of sb
    #method 2: calculate flux directly from Fourier modes. Works better. 
    F1_3 = H3*b3*Q3*bH_13*sb3
    F2_3 = H3*b3*np.sum(sn3*(2*Q3*bH_13[:,np.newaxis]*g23[:,np.newaxis]*np.cos(nnp)/nnp**2 \
                       + alf3[:,np.newaxis]*sb3_x[:,np.newaxis]*(2*g43[:,np.newaxis]*np.cos(nnp)/nnp**2 + g53*((6*np.cos(nnp)-6)/nnp**4 - 3*np.cos(nnp)/nnp**2))),1)
    F3_3 = H3*b3*- Kh3*sb3_x

    F1_3[[0,-1]],F2_3[[0,-1]],F3_3[[0,-1]] = None , None , None
    F1_3[di3[1:-1]],F2_3[di3[1:-1]],F3_3[di3[1:-1]] = None, None, None
    F1_3[di3[1:-1]-1],F2_3[di3[1:-1]-1],F3_3[di3[1:-1]-1] = None, None, None
    
    #interpolate nans
    F1_1,F2_1,F3_1 = inp_nans(F1_1),inp_nans(F2_1),inp_nans(F3_1)
    F1_2,F2_2,F3_2 = inp_nans(F1_2),inp_nans(F2_2),inp_nans(F3_2)
    F1_3,F2_3,F3_3 = inp_nans(F1_3),inp_nans(F2_3),inp_nans(F3_3)
    
    #some checks if the code works
    flux1,flux2,flux3 = (F1_1+F2_1+F3_1) , (F1_2+F2_2+F3_2) , (F1_3+F2_3+F3_3) 
    #check 1: flux in the main channel is (close to) zero, (i.e. smaller than 1% of the river flux)
    if sri !=0: print('WARNING: flux calculations might be nonsense becasue there is a net salt transport through the river')
    print('transport main channel:  ',np.nanmax(np.abs(flux1)).round(3), '. At this location is the river transport ',F1_1[np.nanargmax(np.abs(flux1))].round(3) ,
          '. This means that there is an error of ', (100*np.nanmax(np.abs(flux1))/F1_1[np.nanargmax(np.abs(flux1))]).round(3), '%')
    
    print('transport channel 2:  ',flux2[10].round(3), '. transport channel 3: ',flux3[10].round(3),
          '. There is thus an error of ',(100*(flux2[10]+flux3[10])/(0.5*(np.abs(F1_2[10])+np.abs(F1_3[10])))).round(3), '%' )
    

    
    # =============================================================================
    #     combine former plots
    #=============================================================================
    import matplotlib.gridspec as gridspec

    #normalize
    norm1 = plt.Normalize(0,soc)
    if np.max(np.concatenate([strat1,strat2,strat3]))>0.5: print('WARNING: colorbar of stratification plot not well chosen')
    
    #channel shapes - averaged salt
    x1 = px1[:,0]-np.sum(Ln1)/1000
    y1 = np.zeros(len(x1))
    points1 = np.array([x1, y1]).T.reshape(-1, 1, 2)
    segments1 = np.concatenate([points1[:-1], points1[1:]], axis=1)

    x2 = px2[:di2[-2],0]-np.sum(Ln1)/1000
    y2 = x2/np.max(x2)
    points2 = np.array([x2, y2]).T.reshape(-1, 1, 2)
    segments2 = np.concatenate([points2[:-1], points2[1:]], axis=1)

    x3 = px3[:di3[-2],0]-np.sum(Ln1)/1000
    y3 = -x3/np.max(x3)
    points3 = np.array([x3, y3]).T.reshape(-1, 1, 2)
    segments3 = np.concatenate([points3[:-1], points3[1:]], axis=1)
    
    #make figure
    fig = plt.figure(tight_layout=True,figsize=(11,8))
    gs = gridspec.GridSpec(3,4)
    
    a0 = fig.add_subplot(gs[:2, :3])
    
    #depth-averaged salinity
    lc1 = LineCollection(segments1, cmap='RdBu_r', norm=norm1)
    lc1.set_array(sb1)
    lc1.set_linewidth(10)
    line1 = a0.add_collection(lc1)
    
    lc2 = LineCollection(segments2, cmap='RdBu_r', norm=norm1)
    lc2.set_array(sb2[:di2[-2]])
    lc2.set_linewidth(10)
    a0.add_collection(lc2)

    lc3 = LineCollection(segments3, cmap='RdBu_r', norm=norm1)
    lc3.set_array(sb3[:di3[-2]])
    lc3.set_linewidth(10)
    a0.add_collection(lc3)
    
    a0.axis('off')
    #fig.set_facecolor('lightgrey')
    #a0.axhspan(-1,1,-10,90,color='lightgrey')
    #ax[1].xaxis.set_tick_params(labelsize=15)
    
    #
    cb1=fig.colorbar(line1, ax=a0)
    cb1.set_label(label='Depth-averaged salinity  [g kg$^{-1}$]',fontsize=15)    
    cb1.ax.tick_params(labelsize=13)

    a0.set_xlim(-np.sum(Ln1)/1000-5,np.max([np.sum(Ln2),np.sum(Ln3)])/1000+5)
    a0.set_xlim(-20,20)
    a0.set_ylim(-1.1,1.1)
    
    a0.text(-10,0.1,'MW',fontsize=20)
    a0.text(6,0.82,'MC',fontsize=20)
    a0.text(8,-0.85,'HC',fontsize=20)
    
    rc_MC = 1/12.5
    rc_HC = 1/17
    l_ar = 2
    a0.arrow(0.5,0.22,l_ar,l_ar*rc_MC,width=0.05,color='b',head_length=0.9)#head_width=5)
    a0.arrow(1,-0.22,l_ar,l_ar*-rc_HC,width=0.05,color='b',head_length=0.9)#head_width=5)
    a0.arrow(9,0.55,l_ar,l_ar*rc_MC,width=0.05,color='r',head_length=0.9)#head_width=5)
    a0.arrow(12,-0.55,-l_ar,l_ar*rc_HC,width=0.05,color='r',head_length=0.9)#head_width=5)
    
    #a0.text(-4,0.5,'$Q=$'+str(np.round(Q2/Q1,2))+'$~Q_{riv}$',c='b',fontsize=15)
    #a0.text(-4,-0.52,'$Q=$'+str(np.round(Q3/Q1,2))+'$~Q_{riv}$',c='b',fontsize=15)
    #a0.text(10,-0.01,'$T=$'+str(int(flux2[3]))+' kg s$^{-1}$',c='r',fontsize=15)
    a0.text(-5.5,0.5,'$Q=$'+str(int(np.round(Q2,-1)))+' m$^3$s$^{-1}$',c='b',fontsize=15)
    a0.text(-5.5,-0.52,'$Q=$'+str(int(np.round(Q3,-1)))+' m$^3$s$^{-1}$',c='b',fontsize=15)
    a0.text(10,-0.01,'$T_o=$'+str(int(np.round(flux2[3],-1)))+' kg s$^{-1}$',c='r',fontsize=15)
     
    a0.text(-20,0.9,'(a)',fontsize=15)
    
    #fluxes
    a1 = fig.add_subplot(gs[2, 0])
    a2 = fig.add_subplot(gs[2, 1])
    a3 = fig.add_subplot(gs[2, 2])
    a4 = fig.add_subplot(gs[2, 3])

    a1.plot(px1[:,0]-px1[-1,0],F1_1,label = '$T_Q$',linewidth=2)
    a1.plot(px1[:,0]-px1[-1,0],F2_1,label = '$T_E$',linewidth=2)
    a1.plot(px1[:,0]-px1[-1,0],F3_1,label = '$T_D$',linewidth=2)
    a1.plot(px1[:,0]-px1[-1,0],flux1,label = '$T$',linewidth=2)
    
    a2.plot(px2[:,0]-px1[-1,0],F1_2,label = 'riv',linewidth=2)
    a2.plot(px2[:,0]-px1[-1,0],F2_2,label = 'ddc',linewidth=2)
    a2.plot(px2[:,0]-px1[-1,0],F3_2,label = 'dif',linewidth=2)
    a2.plot(px2[:,0]-px1[-1,0],flux2,label = 'tot',linewidth=2)

    a3.plot(px3[:,0]-px1[-1,0],F1_3,label = 'riv',linewidth=2)
    a3.plot(px3[:,0]-px1[-1,0],F2_3,label = 'ddc',linewidth=2)
    a3.plot(px3[:,0]-px1[-1,0],F3_3,label = 'dif',linewidth=2)
    a3.plot(px3[:,0]-px1[-1,0],flux3,label = 'tot',linewidth=2)
    
    a4.plot(px3[:,0]-px1[-1,0],F1_3,label = 'riv',linewidth=2)
    a4.plot(px3[:,0]-px1[-1,0],F2_3,label = 'ddc',linewidth=2)
    a4.plot(px3[:,0]-px1[-1,0],F3_3,label = 'dif',linewidth=2)
    a4.plot(px3[:,0]-px1[-1,0],flux3,label = 'tot',linewidth=2)
    
    lim = np.nanmax(np.concatenate([F1_1,F1_2,F1_3]))
    #ax[0].legend(loc=3,fontsize = 12)
    for i in (a1,a2,a3,a4):
        i.grid()
        i.set_xlabel('$x$ [km]',fontsize=15)
        i.tick_params(axis='both', which='major', labelsize=13)
        if i!=a4: i.set_ylim(-lim,lim)

    a1.set_xlim(-40,0) ,a2.set_xlim(0,px2[-1,0]-px1[-1,0]-25) ,a3.set_xlim(0,px3[-1,0]-px1[-1,0]-25) ,a4.set_xlim(0,px3[-1,0]-px1[-1,0]-25)
    a1.text(-39,lim-13000,'(b) MW',fontsize=15) , a2.text(0.3,lim-13000,'(c) MC',fontsize=15) , a3.text(0.3,lim-13000,'(d) HC',fontsize=15), a4.text(0.3,2900,'(e) HC',fontsize=15)
    a2.yaxis.set_ticklabels([]) ,  a3.yaxis.set_ticklabels([]),
    a1.set_ylabel('Transport [kg s$^{-1}$]',fontsize=15)
    a1.ticklabel_format(style='sci', axis='y',scilimits=(0,0))
    a1.yaxis.major.formatter._useMathText = True

    a4.ticklabel_format(style='sci', axis='y',scilimits=(0,0))
    a4.yaxis.major.formatter._useMathText = True
    
    a1.legend(loc=3,fontsize=12)
    #fig.text(0.4,1.0,'Q='+str(Q_old[cos[ii]])+' , Ut ='+str(Ut_old[cos[ii]]),fontsize=15)
    
    #calculate contriubtion to total overspill
    print(flux2[3]/F1_1[-1])
    
    #plt.tight_layout()
    #plt.savefig('/Users/biemo004/Documents/UU phd Saltisolutions/Verslagen/Papers/Paper fork channel/figs_def/fig5.jpg',bbox_inches='tight', dpi=300)
    plt.show()

    #print(Q3/Q1)

#'''
#numerical parameters
N=10
Lsc=1000

#physical parameters
soc = 35
sri=0
Q=2000

#let op: ik heb hier per kanaal een andere Ut gekozen!
Ut1,Ut2,Ut3 = 0.84,1.05,0.75 #standard values
#Ut1,Ut2,Ut3 = 0.52, 0.65,0.36 #neap tide
#Ut1,Ut2,Ut3 = 1.07 , 1.34 , 1.05 #spring tide

sf1,sf2,sf3 =2*cv*Ut1, 2*cv*Ut2, 2*cv*Ut3

u_w = 0
d_w = 1

#water level elevation
n_sea2 = 0.
n_sea3 = 0.002

#depth, constant in all dimensions
H=(8,7,5)


#geometry 
#main channel
Ln1 = np.array([110000,39000,800,200])
b01 = 2000
bs1 = np.array([b01,b01,b01,b01])
dxn1 = np.array([500,250,100,10]) 
#upper branch
Ln2 = np.array([200,800,11500,25000])
b02 = 2000
bs2 = np.array([b02,b02,b02,b02*np.exp(10)])
#dxn2 = np.array([10,50,250,250]) 
dxn2 = np.array([10,50,100,50]) 
#lower branch
Ln3 = np.array([200,800,10000,6000,25000])
b03,b13 = 500,3500
bs3 = np.array([b03,b03,b03, b13, b13*np.exp(10)])
#dxn3 = np.array([10,50,250,250,250]) 
dxn3 = np.array([10,50,100,100,50]) 

#lower branc - HC - adjusted
#Ln3 = np.array([200,800,16000,25000])
#b03,b13 = 500,3500
##bs3 = np.array([b03*np.exp(0.2/(17/np.log(7))),b03*np.exp(0.8/(17/np.log(7))), b13, b13*np.exp(10)])
#dxn3 = np.array([10,50,250,250]) 


def runmod():
    ini = np.zeros(int(np.sum(Ln1/dxn1+1)+np.sum(Ln2/dxn2+1)+np.sum(Ln3/dxn3+1))*(N+1)+3)
    ini[-3:] =[Q,Q/2,Q/2]
    out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [3,6,7],ini)
    print('initialisation completed')
    out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
    #check if there was convergence
    if out[0] == 'NOCON':
        #if there was no convergence, try to go in steps of Ut to this values
        Ut1_temp,Ut2_temp,Ut3_temp = np.linspace(1.5,Ut1,5) , np.linspace(1.5,Ut2,5) , np.linspace(1.5,Ut3,5)
        out = MC04_Gal_split(((Ut1_temp[0],Ut2_temp[0],Ut3_temp[0]), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [3,6,7],ini)
        print('initialisation completed')
        out = MC04_Gal_split(((Ut1_temp[0],Ut2_temp[0],Ut3_temp[0]), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
        for i in range(1,len(Ut1_temp)):
            out = MC04_Gal_split(((Ut1_temp[i],Ut2_temp[i],Ut3_temp[i]), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
    #check if there is convergence now
    if out[0] == 'NOCON':
        #try to go in steps of Q to this value
        Q_temp = np.linspace(100,Q,5) #this value of 1000 depends of course a lot on the channel geometry
        out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q_temp[0], u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [3,6,7],ini)
        print('initialisation completed')
        out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q_temp[0], u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
        for i in range(1,len(Q_temp)):
            out = MC04_Gal_split(((Ut1,Ut2,Ut3), soc, sri, Q_temp[i], u_w, d_w, (sf1,sf2,sf3), H, N, Lsc, (n_sea2,n_sea3)), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)
    #check if there is convergence now
    if out[0] == 'NOCON' : print('Still no convergence. better check grid spacing and stuff ')
    plot_results(((Ut1,Ut2,Ut3), soc, sri, Q, u_w, d_w, (sf1,sf2,sf3), H, N, Lsc), (Ln1, b01, bs1, dxn1), (Ln2, b02, bs2, dxn2), (Ln3, b03, bs3, dxn3), [1,2,5,3,6,7],out)


tijd = time.time()
runmod()
print('Doing the simulation takes  '+ str(time.time()-tijd)+' seconds')
#'''
